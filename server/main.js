import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import '../imports/api/forum-services.js';
import '../imports/api/subforum-service.js';
import '../imports/api/user-services.js';
import '../imports/api/messaging-service.js';
import '../imports/api/threads-service.js';
import '../imports/api/notification-services.js';
import '../imports/api/policy-service.js';
import '../imports/api/two-way-auth-service.js';
import { AUTH } from '../imports/api/domainlayer/BL/Authentication/two-way-auth-handler.js';

const policyServices = require('../imports/api/policy-service.js');

const user_service = require('../imports/api/user-services.js');

process.env.MAIL_URL = "smtp://callmeshesababa%40gmail.com:wearesababa@smtp.gmail.com";

var serverURL = process.env.SERVER_URL;

Meteor.startup(() => {
	//Initialized system policy
	policyServices.getPolicy();

	//Init auth handler
	AUTH.initialize();

	Accounts.urls.enrollAccount = wrapFunctionWithURLFix(Accounts.urls.enrollAccount);
	Accounts.urls.verifyEmail = wrapFunctionWithURLFix(Accounts.urls.verifyEmail);
	Accounts.urls.resetPassword = wrapFunctionWithURLFix(Accounts.urls.resetPassword);
	
	ConsoleMe.enabled = true;
});

Accounts.onCreateUser(function(options, user) {
    //pass the surname in the options
    user.password_date = new Date();
    return user;
})


Accounts.onLogin(function() {
	userId = Meteor.userId();
});

function wrapFunctionWithURLFix (urlFunction) {
	if (!serverURL) return urlFunction;

	return function(token) {
		var oldURL = urlFunction(token);
		oldURL = oldURL.replace("http://localhost:3000", "http://" + serverURL);
		return oldURL.replace("http://localhost:80", "http://" + serverURL);
	}
}
 

Meteor.methods({
  'checkServerHealth'() {
  	return true;
  }
});