import { Meteor } from 'meteor/meteor';
const messageHandler = require("./domainlayer/BL/Messaging/messagingBL");

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('getUserMessages', function() {
  	var result =  messageHandler.readMessages(this.userId);
  	console.log(result.fetch());
  	return result;
  });
}


Meteor.methods({
  'messages.send'(sender, receiver, text) {
	 return messageHandler.sendMessage(sender, receiver, text);
  },
});


//Private
export function getAllMessages () {
	return messageHandler.getAllMessages();
}

export function read(userId) {
  var result =  messageHandler.readMessages(userId);
  console.log(result.fetch());
  return result;
}