import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const messagingService = require('./../messaging-service.js');

if (Meteor.isServer) {
  describe('Messaging Integration Testing', () => {
      it('Send message : Check that user got 1 message', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var numOfMessages = messagingService.getAllMessages().fetch().length;
          assert.equal(numOfMessages, 1);
          resetDatabase();
      });
      it('Send message : Check that user got 2 messages', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var numOfMessages = messagingService.getAllMessages().fetch().length;
          assert.equal(numOfMessages, 2);
          resetDatabase();
      });

      it('Send message : Check that message content is correct', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var message = messagingService.getAllMessages().fetch()[0];
          assert.equal(message.messageBody, 'text');
          resetDatabase();
      });
      
      it('Read message : Check that message content is correct', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var messageSent = messagingService.getAllMessages().fetch()[0];
          var messageRead = messagingService.read('receiverid').fetch()[0];
          assert.equal(messageSent.messageBody, messageRead.messageBody);
          resetDatabase();
      });
      
      it('Read message : Check that message sender is correct', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var messageSent = messagingService.getAllMessages().fetch()[0];
          var messageRead = messagingService.read('receiverid').fetch()[0];
          assert.equal(messageSent.sender, messageRead.sender);
          resetDatabase();
      });
      
      it('Read message : Check that message reciever is correct', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var messageSent = messagingService.getAllMessages().fetch()[0];
          var messageRead = messagingService.read('receiverid').fetch()[0];
          assert.equal(messageSent.reciever, messageRead.reciever);
          resetDatabase();
      });


    });
}      
