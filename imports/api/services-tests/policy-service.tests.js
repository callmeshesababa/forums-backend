import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const policyServices = require('./../policy-service.js');

if (Meteor.isServer) {
  	describe('Policy Integration Test', () => {
      it('Policy creation : New Policy created', () => {
          resetDatabase();
          var policy = policyServices.getPolicy().fetch()[0]
          assert.isNotNull(policy);
          resetDatabase();
      });

	  it('Policy creation : Only one Policy exists', () => {
	      resetDatabase();
	      var policy = policyServices.getPolicy().fetch().length;
	      assert.equal(1, policy);
	      resetDatabase();
      });
      
      it('Policy creation : Policy exists on starup created', () => {
          resetDatabase();
          var policy = policyServices.getPolicy().fetch()[0]
          assert.isNotNull(policy);
          resetDatabase();
      });

      it('Policy properties : Test policy default expiration time', () => {
          resetDatabase();
          var policy = policyServices.getPolicy().fetch()[0]
          assert.equal(14, policy.passExpirationPeriod);
          resetDatabase();
      });

      it('Policy properties : Test policy default security questions', () => {
          resetDatabase();
          var policy = policyServices.getPolicy().fetch()[0]
          assert.equal(false, policy.allowSecurityQuestion);
          resetDatabase();
      });

    });
}     
