import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const forumServices = require('./../forum-services.js');
const subforumService = require('./../subforum-service.js');

if (Meteor.isServer) {
  describe('Subforums Integration Testing', () => {
      it('Subforum creation : Check if created and exists 1 subforums', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          var numOfSubforums = subforumService.getSubforumsForForumId(forum._id).fetch().length;
          assert.equal(numOfSubforums, 1);
          resetDatabase();
      });
      
      it('Subforum creation : Check if subforum name is correct', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          assert.equal(subforum.subForumName, 'Gals Subforum');
          resetDatabase();
      });
      
      it('Subforum creation : sCheck if subforum description is correct', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          assert.equal(subforum.subForumDescription, 'this is gals subforum');
          resetDatabase();
      });
      
      it('Subforum creation : Create with empty paramas should fail', () => {
          resetDatabase();
          var forum = createForum();
          var name = '';
          var desc = '';
          var userId = '';
          Meteor.call('subforums.insert', forum._id, name, desc, userId);
          var numOfSubforums = subforumService.getSubforumsForForumId(forum._id).fetch().length;
          assert.equal(numOfSubforums, 0);
          resetDatabase();
      });
      
      it('Subforum deletion : Check if subforum was deleted', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          Meteor.call('subforums.delete', subforum._id, 'userid1');
          var numOfSubforums = subforumService.getSubforumsForForumId(forum._id).fetch().length;
          assert.equal(numOfSubforums, 0);
          resetDatabase();
      });
      
      it('Subforum deletion : Deletion not by an admin should fail', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          Meteor.call('subforums.delete', subforum._id, 'notadminid');
          var numOfSubforums = subforumService.getSubforumsForForumId(forum._id).fetch().length;
          assert.notEqual(numOfSubforums, 0);
          resetDatabase();
      });

      it('Subforum editing : Try to edit subforum name', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          Meteor.call('subforums.edit', subforum._id, 'new_name', 'new_desc', 'userid1');
          var subforums = subforumService.getSubforumsForForumId(forum._id).fetch()[0];
          assert.equal(subforums.subForumName, 'new_name');
          resetDatabase();
      });
     
       it('Subforum editing : Try to edit subfourm description', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          Meteor.call('subforums.edit', subforum._id, 'new_name', 'new_desc', 'userid1');
          var subforums = subforumService.getSubforumsForForumId(forum._id).fetch()[0];
          assert.equal(subforums.subForumDescription, 'new_desc');
          resetDatabase();
      });     

      it('Subforum editing : Try to edit forum description with wrong id should fail', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          Meteor.call('subforums.edit', subforum._id, 'new_name', 'new_desc', 'gal');
          var subforums = subforumService.getSubforumsForForumId(forum._id).fetch()[0];
          assert.notEqual(subforums.subForumDescription, 'new_desc');
          resetDatabase();
        }); 
      
    });
}

function createForum(){
    var name = 'Gals Forum';
    var desc = 'this is gals forum';
    var userId = 'userid1';
    Meteor.call('forums.insert', name, desc, userId);
    return forumServices.getAllForums().fetch()[0];
}

function createSubforum(forumId){
    var name = 'Gals Subforum';
    var desc = 'this is gals subforum';
    var userId = 'userid1';
    Meteor.call('subforums.insert',forumId, name, desc, userId);
    return subforumService.getSubforumsForForumId(forumId).fetch()[0];
}
  