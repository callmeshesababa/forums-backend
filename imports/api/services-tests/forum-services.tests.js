import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const forumServices = require('./../forum-services.js');

if (Meteor.isServer) {
  describe('Forums Integration Testing', () => {
      it('Forum creation : Check num of forums', () => {
          resetDatabase();
          var forum = createForum();
          var numOfForums = forumServices.getAllForums().fetch().length;
          assert.equal(numOfForums, 1);
          resetDatabase();
      });
      
      it('Forum creation : Check correct forum name', () => {
          resetDatabase();
          var forum = createForum();
          assert.equal(forum.forumName, 'Gals Forum');
          resetDatabase();
      });
      
      it('Forum creation : Check forum description', () => {
          resetDatabase();
          var forum = createForum();
          assert.equal(forum.forumDescription, 'this is gals forum');
          resetDatabase();
      });
      
      it('Forum creation : Check correct admin id', () => {
          resetDatabase();
          var forum = createForum();
          assert.equal(forum.forumAdminId, 'userid1');
          resetDatabase();
      });
      
      it('Forum creation : New forum with empty paramas should fail', () => {
          resetDatabase();
          var name = '';
          var desc = '';
          var userId = '';
          Meteor.call('forums.insert', name, desc, userId);
          var forum = forumServices.getAllForums().fetch()[0];
          var numOfForums = forumServices.getAllForums().fetch().length;
          assert.equal(numOfForums, 0);
          resetDatabase();
      });
      
      it('Forum deletion : Check proper deletion', () => {
          resetDatabase();
          var forum = createForum();
          Meteor.call('forums.delete', forum._id, 'userid1');
          var numOfForums = forumServices.getAllForums().fetch().length;
          assert.equal(numOfForums, 0);
          resetDatabase();
      });
      
      it('Forum deletion : Created by not an admin should fail', () => {
          resetDatabase();
          var forum = createForum();
          Meteor.call('forums.delete', forum._id, 'notadminid');
          var numOfForums = forumServices.getAllForums().fetch().length;
          assert.notEqual(numOfForums, 0);
          resetDatabase();
      });
      
      it('Forum deletion : Try to delete with wrong forum id should fail', () => {
          resetDatabase();
          var forum = createForum();
          Meteor.call('forums.delete', 'nosuchforumid', 'userid1');
          var numOfForums = forumServices.getAllForums().fetch().length;
          assert.notEqual(numOfForums, 0);
          resetDatabase();
      });
      
      it('Forum editing : Try to edit forum name', () => {
          resetDatabase();
          var forum = createForum();
          Meteor.call('forums.edit', forum._id, 'new_name', 'new_desc', 'userid1');
          var forums = forumServices.getAllForums().fetch()[0];
          assert.equal(forums.forumName, 'new_name');
          resetDatabase();
      });
     
       it('Forum editing : Try to edit forum description', () => {
          resetDatabase();
          var forum = createForum();
          Meteor.call('forums.edit', forum._id, 'new_name', 'new_desc', 'userid1');
          var forums = forumServices.getAllForums().fetch()[0];
          assert.equal(forums.forumDescription, 'new_desc');
          resetDatabase();
      });     

      it('Forum editing : Try to edit forum description with wrong id should fail', () => {
          resetDatabase();
          var forum = createForum();
          Meteor.call('forums.edit', forum._id, 'new_name', 'new_desc', 'userid1');
          var forums = forumServices.getAllForums().fetch()[0];
          assert.notEqual(forums.forumDescription, 'this is gals forum');
          resetDatabase();
      });  
    });
}

function createForum(){
    var name = 'Gals Forum';
    var desc = 'this is gals forum';
    var userId = 'userid1';
    Meteor.call('forums.insert', name, desc, userId);
    return forumServices.getAllForums().fetch()[0];
}
          
