import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const forumService = require("./../forum-services.js");
const subforumService = require("./../subforum-service.js");
const threadService = require("./../threads-service.js");

if (Meteor.isServer) {
  describe('Threads Integration Testing', () => {
      it('Add new thread test', () => {
        resetDatabase();	
    	  var forumId = createForum();
    	  var subForumId = createSubForum(forumId);
    	  Meteor.call('threads.postThread', subForumId, '123', 'test1', 'this is my test1');
        assert.equal(1, threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch().length);
      });

      it('Validate added thread title test', () => {
        resetDatabase();	
    	  var forumId = createForum();
  	    var subForumId = createSubForum(forumId);
    	  Meteor.call('threads.postThread', subForumId, '123', 'test1', 'this is my test1');
        assert.equal('test1', threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch()[0].subject);
      });
      
      it('Validate added thread bodytest ', () => {
        resetDatabase();	
    	  var forumId = createForum();
  	    var subForumId = createSubForum(forumId);
    	  Meteor.call('threads.postThread', subForumId, '123', 'test1', 'this is my test1');
        assert.equal('this is my test1', threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch()[0].text);
      });
      
      it('Edit thread subject test', () => {
        resetDatabase();  
        var forumId = createForum();
        var subForumId = createSubForum(forumId);
        Meteor.call('threads.postThread', subForumId, '123', 'test1', 'this is my test1');
        var thread = threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch()[0]
        Meteor.call('threads.editThread', thread._id, '123', 'new_test1', 'this is my test1');
        assert.equal('new_test1', threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch()[0].subject);
      });
      
      it('Edit thread text test', () => {
        resetDatabase();  
        var forumId = createForum();
        var subForumId = createSubForum(forumId);
        Meteor.call('threads.postThread', subForumId, '123', 'test1', 'this is my test1');
        var thread = threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch()[0]
        Meteor.call('threads.editThread', thread._id, '123', 'new_test1', 'this is my new test1');
        assert.equal('this is my new test1', threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch()[0].text);
      });
    
      it('Add new thread test', () => {
        resetDatabase();  
        var forumId = createForum();
        var subForumId = createSubForum(forumId);
        Meteor.call('threads.postThread', subForumId, '123', 'test1', 'this is my test1');
        assert.equal(1, threadService.threadsForSubforumIdWithUserId(subForumId, '123').fetch().length);
      });
    });
}

function createForum(){
      	  Meteor.call('forums.insert', 'forum1', 'desc1', '123');
      	  var forum = forumService.getAllForums().fetch()[0];
      	  return forum._id;
}
function createSubForum(forumId){
    	  Meteor.call('subforums.insert', forumId, 'subforum1', 'desc1', '123');
    	  var subForum = subforumService.getSubforumsForForumId(forumId).fetch()[0];
    	  return subForum._id;
}