import { Meteor } from 'meteor/meteor';
const subforumHandler = require("./domainlayer/BL/SubForum/subforumBL");

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('subforumsForForumId', getSubforumsForForumId);
  Meteor.publish('subForumArrayForSubforumId', getSubforumsArrayForSubforumId);
  Meteor.publish('subforumCursorForSubforumId', getSubforumCursorForSubforumId);
}

Meteor.methods({
  'subforums.delete'(subforumId, userId) {
    subforumHandler.deleteSubForum(subforumId, userId);
  },
  'subforums.edit'(subforumId, newName, description, userId) {
    subforumHandler.editSubForum(subforumId, newName, description, userId);
  },
  'subforums.addManager'(subforumId, newManagerId, userId, experationDate) {
    return subforumHandler.addManager(subforumId, newManagerId, userId, experationDate);
  },
  'subforums.removeManager'(subforumId, userId, manager) {
    subforumHandler.removeManager(subforumId, userId, manager);
  },
  'subforums.editManagerExpidationDate'(subforumId, newManagerId, userId, experationDate) {
    subforumHandler.editSubForumManagerExperationDate(subforumId, newManagerId, userId, experationDate);
  },
  'subforums.insert'(forumId, subForumName, description, userId) {
    return subforumHandler.createSubForum(forumId, subForumName, description, userId);
  },
});


//Public

export function getSubforumsArrayForSubforumId(subforumId) {
  return subforumHandler.subForumArrayForSubforumId(subforumId);
}

export function getSubforumsForForumId(forumId) {
	return subforumHandler.getSubforumsForForumId(forumId);
}

export function getSubforumCursorForSubforumId(subforumId) {
  return subforumHandler.getSubforumCursorForSubforumId(subforumId);
}

export function getNumOfMessagesInForum(forumId) {
    return subforumHandler.getNumOfMessagesInForum(forumId, Meteor.user());
}
    
export function getNumOfMessagesInForumByUser(forumId, userId) {
    return subforumHandler.getNumOfMessagesInForumForUser(forumId, Meteor.user(), userId);
}
    
export function getManagers() {
    return subforumHandler.getManagers();
}

export function userIsModeratorInSubforum(userId, subforumId) {
  return subforumHandler.userIsModeratorInSubforum(userId, subforumId);
}