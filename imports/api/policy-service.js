import { Meteor } from 'meteor/meteor';
const policyHandler = require("./domainlayer/BL/Policy/PolicyBL");
const usersBL = require("./domainlayer/BL/Users/usersBL");


if (Meteor.isServer){
	Meteor.publish('getPolicy', getPolicy);
}

Meteor.methods({
	'policy.setAllowSecurityQuestionAndPassExporationDate'(isAllowed, period) {
		return setAllowSecurityQuestionAndPassExporationDate(isAllowed, period);
	}
});

export function getPolicy () {
	return policyHandler.getPolicy();
}

export function setAllowSecurityQuestionAndPassExporationDate (isAllowed, period) {
	return policyHandler.setAllowSecurityQuestionAndPassExporationDate(isAllowed, period);
}