import { check } from 'meteor/check';

const notificationCenterDA = require("../../DAL/NotificationCenterDA");
const logger = require("../logger");
const priveleges = require("../Authentication/privilegesBL");
const forumBL = require("../Forum/forumBL");
const usersBL = require("../Users/usersBL");
const threadsBL = require("../Threads/threadsBL");

import { userNotificationSettingsInForum, forumNotiificationsSettings } from '../../../systemConsts';

export function getNotificationsForUser(userId, count) {
	return notificationCenterDA.getNotificationsForUser(userId, count);
}

export function getUnreadNotificationsForUser(userId, count) {
	return notificationCenterDA.getUnreadNotificationsForUser(userId, count);
}

export function markNotificationIsRead(notificationId) {
	return notificationCenterDA.markNotificationIsRead(notificationId);
}

export function registerUserForNotificationsOnObject (userId, objectId) {
	check(userId, String);
	check(objectId, String);

	logger.logSuccess("notification center registered userId " + userId + " on object " + objectId);

	return notificationCenterDA.registerUserForNotificationsOnObject(userId, objectId);
}

export function unregisterUserForNotificationsOnObject (userId, objectId) {
	check(userId, String);
	check(objectId, String);

	logger.logSuccess("notification center unregistered userId " + userId + " on object " + objectId);

	return notificationCenterDA.unregisterUserForNotificationsOnObject(userId, objectId);
}


//Return boolean - success
//To get this notification, register on forumId
export function newThreadWasCreatedInForum (forumId, subforumId, threadId, actorId) {
	check(forumId, String);
	check(subforumId, String);
	check(threadId, String);
	check(actorId, String);

	var forumName = forumBL.getForumNameForForumId(forumId);
	var actorName = usersBL.getUserNameForUserId(actorId);
	var threadSubject = threadsBL.getThreadSubjectForThreadId(threadId);
	var notificationMessage = actorName + " created a new thread " + threadSubject + " in " + forumName;

	return createNotifications(forumId, subforumId, threadId, notificationMessage, forumId, actorId);
}
 
//Return boolean - success
//To get this notification, register on threadId
export function threadWasEdited (forumId, subforumId, threadId, actorId) {
	check(forumId, String);
	check(subforumId, String);
	check(threadId, String);
	check(actorId, String);

	var forumName = forumBL.getForumNameForForumId(forumId);
	var actorName = usersBL.getUserNameForUserId(actorId);
	var threadSubject = threadsBL.getThreadSubjectForThreadId(threadId);
	var notificationMessage = actorName + " edited " + threadSubject + " in " + forumName;

	return createNotifications(forumId, subforumId, threadId, notificationMessage, threadId, actorId);
}

//Private - all notification creations should go through here
//notificationObjectId - is the id of the object on which we register the user for notifications
function createNotifications (forumId, subforumId, threadId, notificationMessage, notificationObjectId, actorId) {
	check(notificationObjectId, String);
	check(actorId, String);

	var usersIdsToNotify = notificationCenterDA.getUsersRegisteredForNotificationsOnObjectId(notificationObjectId);

	var success = true;

	for (var userIdIndex in usersIdsToNotify) {
		var userId = usersIdsToNotify[userIdIndex];

		if (actorId !== userId && shouldCreateNotificationForUserInForum(forumId, userId)) {

			logger.logSuccess("notification was sent to " + userId + " on object " + notificationObjectId);

			var insertSuccess = notificationCenterDA.createNotificationForUser(userId, notificationMessage, forumId, subforumId, threadId);
			success = success && insertSuccess;
		}
	}

	return success;
}

function shouldCreateNotificationForUserInForum (forumId, userId) {
	let forumNotificationSetting = forumBL.getForumNotificationSettingsForForumId(forumId);
	let userNotificationSettingInForum = forumBL.getUserNotificationSettingsInForum(forumId, userId);

	if (!forumNotificationSetting || !userNotificationSettingInForum) return true;

	let userIsConnected = usersBL.userIsConnected(userId);

	//Return true if forum notification settings is offline
	if ( forumNotificationSetting === forumNotiificationsSettings.OFFLINE ) return true;

	//Return true if forum notification settings is online and user is online
	if ( forumNotificationSetting === forumNotiificationsSettings.ONLINE && userIsConnected ) return true;

	//Return true if forum notification settings is selective and user chose offline or not set
	if ( forumNotificationSetting === forumNotiificationsSettings.SELECTIVE ) {
		if ( userNotificationSettingInForum === userNotificationSettingsInForum.NOTSET || userNotificationSettingInForum === userNotificationSettingsInForum.OFFLINE ) {
			return true;
		}
	}

	//Return true if forum notification settings is selective and user chose online and user is online
	if ( forumNotificationSetting === forumNotiificationsSettings.SELECTIVE ) {
		if ( userNotificationSettingInForum === userNotificationSettingsInForum.ONLINE && userIsConnected ) {
			return true;
		}
	}

	//Else - return false
	return false;
}