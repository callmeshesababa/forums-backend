const subforumsDA = require("../../DAL/SubforumsDA");
const logger = require("../logger");
const priveleges = require("../Authentication/privilegesBL");

import {
	privilegesErrorName,
	allowUserBeModeratorInMultiplePolicies
} from '../../../systemConsts';
import { getForum } from '../Forum/forumBL';


export function createSubForum(forumId, subForumName, description, creatorId) {

	if(subForumName == ""){
 		logger.logFail("SubForum create with empty name");
 		return "Error: Empty SubForum Name";
 	} else if (!subforumNameisFree(subForumName,forumId)){
 		logger.logFail("SubForum creation failed. SubForum name is taken, SubForum name: " + subForumName);
 		return "Error: SubForum Name Exists";
 	}else if(!priveleges.isAdminInForum(forumId,creatorId)){
 		logger.logFail("SubForum creation failed. User is not admin, forum id: " + forumId);
 		return "Error: No priveleges for operation";
 	}else{
 		logger.logSuccess("SubForum created with name: " + subForumName + " in Forum: " + forumId);
 		return subforumsDA.addSubForum(forumId, subForumName, description, creatorId);
 	}
}

export function deleteSubForum(subforumId, userId) {
	var forumId = subforumsDA.getForumIdBySubforum(subforumId);

	if (!checkSubforumExists(subforumId)) return;

	if(!priveleges.isAdminInForum(forumId, userId)){
		logger.logFail("SubForum deletion failed. User is not admin, SubForum name: " + subforumId);
		return "Error: No priveleges for operation";
	}else{
		logger.logSuccess("SubForum: " + subforumId + " deleted");
		return subforumsDA.deleteSubForum(subforumId);
	}
}

export function editSubForum(subforumId, newName, desc, userId) {
	var forumId = subforumsDA.getForumIdBySubforum(subforumId);

	if (!checkSubforumExists(subforumId)) return;

	if(!priveleges.isAdminInForum(forumId,userId) && !priveleges.isManagerInSubForum(subforumId,userId)){
		logger.logFail("Subforum deletion failed. User is not admin or subforum manager, Subforum : " + subforumId);
		return "Error: No priveleges for operation";
	}else if(!subforumNameisFree(newName,forumId)){
		logger.logFail("SubForum creation failed. SubForum name is taken, SubForum name: " + newName);
		return "Error: SubForum Name Exists";
	}else{
		logger.logSuccess("Subforum: " + subforumId + " edited");
		return subforumsDA.editSubForum(subforumId, newName, desc);
	}
}

export function addManager(subforumId, newManagerId, userId, experationDate) {
	var forumId = subforumsDA.getForumIdBySubforum(subforumId);

	if (!checkSubforumExists(subforumId)) return;

	if (!checkMultipleModerationConstraints(subforumId, newManagerId, forumId)) return;

	if(!priveleges.isAdminInForum(forumId, userId)){
		logger.logFail("Subforum manager addition failed. User is not admin in Forum: " + forumId);
		throw new Meteor.Error(privilegesErrorName, "You have no permission to add moderators to this sub forum");
	}else if(!priveleges.userInForum(forumId, newManagerId)){
		logger.logFail("Subforum manager addition failed. User is not a member of the forum: " + forumId);
		throw new Meteor.Error("not-a-member", "The user you chose is not a member of the forum");
	}else{
		logger.logSuccess("Manager: " + newManagerId + " added to subforum: " + subforumId);
		return subforumsDA.addSubForumManager(subforumId, newManagerId, experationDate);
	}
}




export function editSubForumManagerExperationDate(subforumId, newManagerId, userId, experationDate) {
	var forumId = subforumsDA.getForumIdBySubforum(subforumId);

	if (!checkSubforumExists(subforumId)) return;

	if(!priveleges.isAdminInForum(forumId, userId)){
		logger.logFail("Subforum manager edit failed. User is not admin in Forum: " + forumId);
		return "Error: No priveleges for operation";
	}else if(!priveleges.isManagerInSubForum(subforumId, newManagerId)){
		logger.logFail("Subforum manager edit failed. User is not manager in Subforum: " + subforumId);
		return "Error: No priveleges for operation";
	}else{
		logger.logSuccess("Manager: " + newManagerId + " experation date edited from subforum: " + subforumId);
		return subforumsDA.editSubForumManagerExperationDate(subforumId, newManagerId, experationDate);
	}
}

export function removeManager(subforumId, userId, managerToRemoveId) {
	var forumId = subforumsDA.getForumIdBySubforum(subforumId);

	if (!checkSubforumExists(subforumId)) return;

	if(!priveleges.isAdminInForum(forumId, userId)){
		logger.logFail("Subforum manager deletion failed. User is not admin in Forum: " + forumId);
		return "Error: No priveleges for operation";
	}else if(!priveleges.isManagerInSubForum(subforumId, managerToRemoveId)){
		logger.logFail("Subforum manager deletion failed. User is not manager in Subforum: " + subforumId);
		return "Error: No priveleges for operation";
	}else{
		logger.logSuccess("Manager: " + managerToRemoveId + " deleted from subforum: " + subforumId);
		return subforumsDA.removeSubForumManager(subforumId, managerToRemoveId);
	}
}

export function getNumOfMessagesInForum(forumId, user) {
	if(!priveleges.forumExists(forumId)){
		logger.logFail("Get num of messages in forum failed. Forum doesn't exist with id: " + forumId);
		return "Error: Subforum Doesn't Exist";
	}else if(!priveleges.isAdminInForum(forumId,user)){
		logger.logFail("Get num of messages in forum failed.User is not admin in Forum: " + forumId);
		return "Error: No priveleges for operation";
	}else{
		return subforumsDA.getNumOfMessages(forumId);
	}
}

export function getNumOfMessagesInForumForUser(forumId, admin, user) {
	if(!priveleges.forumExists(forumId)){
		logger.logFail("Get num of messages in forum per user failed. Forum doesn't exist with id: " + forumId);
		return "Error: Subforum Doesn't Exist";
	}else if(!priveleges.isAdminInForum(forumId,admin)){
		logger.logFail("Get num of messages in forum per user  failed.User is not admin in Forum: " + forumId);
		return "Error: No priveleges for operation";
	}else{
		return subforumsDA.getNumOfMessagesForUser(forumId,user);
	}
}

export function subforumNameisFree(name, forumId) {
	return subforumsDA.isNameFree(name, forumId);
}

export function getSubforumsForForumId (forumId) {
	return subforumsDA.getSubforumsForForumId(forumId);
}

export function subForumArrayForSubforumId(subforumId) {
	return subforumsDA.subForumArrayForSubforumId(subforumId);
}

export function getManagers() {
	return subforumsDA.getAllManagers();
}

export function getForumIdBySubforum (subforumId) {
	return subforumsDA.getForumIdBySubforum(subforumId);
}

//return true if exists. throws if not
function checkSubforumExists(subforumId) {
	let exists = priveleges.subForumExists(subforumId);
	if(!exists){
		logger.logFail("Subforum manager addition failed. Subforum doesn't exist with id: " + subforumId);
		throw new Meteor.Error("subforum-not-exist", "Sub forum doesn't exist");
	}
	return true;
}

//return true if multiple moderator constraints are held. throws if not
function checkMultipleModerationConstraints(subforumId, newModeratorId, forumId) {
	var forum = getForum(forumId).fetch()[0];

	if (!forum.settings || forum.settings.allowUserBeModeratorInMultiplePolicies !== allowUserBeModeratorInMultiplePolicies.NOTALLOWED) {
		//No need to check that the user is not a moderator in other sub forums in the forum
		return true;
	}

	let userIsModeratorInOtherSubforum = subforumsDA.checkIfUserIsModeratorInOneOfTheSubforumsOfAForum(newModeratorId, forumId);

	if (userIsModeratorInOtherSubforum) {
		logger.logFail("Failed adding manager " + userId + " to subforum " + subforumId + " beacuse of multiple moderation constraint violation");
		throw new Meteor.Error("multiple-moderation-violation", "User is already a moderator in another subforum in this forum");
		return false;
	}

	return true;
}

export function getSubforumCursorForSubforumId(subforumId) {
	return subforumsDA.getSubforumCursorForSubforumId(subforumId);
}

export function userIsModeratorInSubforum(userId, subforumId) {
	let subforumArray = getSubforumCursorForSubforumId(subforumId).fetch();

	if (!subforumArray || subforumArray.length != 1) return false;

	let subforum = subforumArray[0];

	var foundObj = { found: false };
	
	_.forEach(subforum.subForumManagersId, function(subforumManagerObject) {
		if (subforumManagerObject.managerId === userId) {
			foundObj.found = true;
		}
	});

	return foundObj.found;
}