const logger = require("../logger");
const subforumsBL = require("../SubForum/subforumBL");
const usersDA = require("../../DAL/UsersDA");

export function getAllUsers () {
	return usersDA.getAllUsers();
}

export function getUsersWithName (seachQuery) {
	return usersDA.getUsersWithName(seachQuery);
}

export function getUsersWithNameOutOfIdList (seachQuery, userIdList) {
	return usersDA.getUsersWithNameOutOfIdList(seachQuery, userIdList);
}

export function getUserWithId(userId) {
	return usersDA.getUserWithId(userId);
}

export function getUserNameForUserId (userId) {
	return usersDA.getUserNameForUserId(userId);
}


export function isSuperAdminSet(){
	return usersDA.isSuperAdminSet();
}

export function isSuperAdmin(userId) {
	return usersDA.isSuperAdmin(userId);
}

export function setUserAsSuperAdmin(userId) {
	return usersDA.setUserAsSuperAdmin(userId);
}

export function userIsConnected(userId) {
	return UserStatus.connections.find({userId: userId}).count() > 0;
}