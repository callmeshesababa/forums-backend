import { Meteor } from 'meteor/meteor';

const forumDA = require("../../DAL/ForumDA");
const subforumDA = require("../../DAL/SubforumsDA");
const threadsDA = require("../../DAL/ThreadsDA");

export function isAdminInForum (forumId, userId) {
	if (Meteor.isServer) {
		return forumDA.isAdminInForum(forumId, userId);
	}
	return true;
}

export function forumExists (forumId) {
	return forumDA.forumExists(forumId);
}

export function subForumExists (subForumId) {
	return subforumDA.subForumExists(subForumId);
}

export function isManagerInSubForum (subForumId, userId) {
	return subforumDA.isManagerInSubForum(subForumId, userId);
}

export function userInForum (forumId, user) {
	return forumDA.userInForum(forumId, user);
}

export function userCanDeletePost (postId, user) {
	return threadsDA.userCanManipulatePost(postId, user);
}

export function userCanEditPost (postId, user) {
	return threadsDA.userCanManipulatePost(postID, user);
}

export function userCanDeleteComment (postId, commentId, user) {
	return threadsDA.userCanManipulateComment(postID, user);
}

export function userCanEditComment (postId, commentId, user) {
	return threadsDA.userCanManipulateComment(postID, user);
}

//TODO : this is wrong
export function isSuperAdmin (userId) {
	return true;
}