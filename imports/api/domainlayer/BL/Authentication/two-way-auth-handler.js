const TwoWayAuthDA = require("../../DAL/TwoWayAuthDA");

//This module is internal to the server use only. this should not be made public using publicatiosn
export var AUTH = {
	initialize () {
		UserStatus.events.on("connectionLogin", function(fields) {
			var userId = fields.userId;
			var connectionId = fields.connectionId;

			if (userId && connectionId) {
				TwoWayAuthDA.addConnection(connectionId, userId);
			}
		});

		UserStatus.events.on("connectionLogout", function(fields) {
			var userId = fields.userId;
			var connectionId = fields.connectionId;

			if (userId && connectionId) {
				TwoWayAuthDA.removeConnection(connectionId);
			}
		});

		TwoWayAuthDA.initialize();
	}
}


export function isCurrentSessionAuthorized(connectionId) {
	return TwoWayAuthDA.isCurrentSessionAuthorized(connectionId);
}

//For publications
export function connectionsForConnectionId (connectionId) {
	return TwoWayAuthDA.connectionsForConnectionId(connectionId);
}

//Returns the key with which the user can authorize other connections
//If the current connection is unauthorized, will return undefined
export function getAuthorizationKeyForConnectionId(connectionId) {
	if (!isCurrentSessionAuthorized(connectionId)) {
		return;
	}

	return TwoWayAuthDA.getAuthorizationKeyForConnectionId(connectionId);
}

export function clientSideCurrentConnectionIsAuthorized () {
	return TwoWayAuthDA.clientSideCurrentConnectionIsAuthorized();
}

export function clientSideCurrentConnectionIsAuthorizedDefaultIsNo () {
	return TwoWayAuthDA.clientSideCurrentConnectionIsAuthorizedDefaultIsNo();
}

//authorizationKey is optional
export function authorizeConnection(connectionId, authorizationKey) {
	return TwoWayAuthDA.authorizeConnection(connectionId, authorizationKey);
}

export function clientShouldShowConnectionCode() {
	return TwoWayAuthDA.clientShouldShowConnectionCode();
}

export function requestShowConnectionCodeInAuthorizedClients(userId) {
	TwoWayAuthDA.requestShowConnectionCodeInAuthorizedClients(userId);
}

export function stopShowingConnectionCodeInAuthorizedClient(connectionId) {
	TwoWayAuthDA.stopShowingConnectionCodeInAuthorizedClient(connectionId);
}