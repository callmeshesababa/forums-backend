const forumDA = require("../../DAL/ForumDA");
const logger = require("../logger");
const priveleges = require("../Authentication/privilegesBL");
const notificationCenter = require("../NotificationCenter/NotificationCenterBL");

import { privilegesErrorName } from '../../../systemConsts';

export function create(name, description, userId) {
	if(name == ""){
		logger.logFail("Forum create with empty name");
		return "Error: Empty Forum Name";
	}else if(!forumNameisFree(name)){
		logger.logFail("Forum creation failed. forum name is taken in, forum name: " + name);
		return "Error: Forum Name Exists";
	}else{
		logger.logSuccess("Forum created with name: " + name + " and forum admin id: " + userId);
		var createdForumId = forumDA.addForum(name, description, userId);
		register(createdForumId, userId);
		return createdForumId;
	}
}

export function getForum (forumId) {
	return forumDA.getForum(forumId);
}

export function deleteForum(forumId, user) {
	if(!priveleges.forumExists(forumId)){
		logger.logFail("Forum deletion failed. forum doesn't exist with id: " + forumId);
		return "Error: Forum Doesn't Exist";
	}else if(!priveleges.isAdminInForum(forumId,user)){
		logger.logFail("Forum deletion failed. User is not admin in forum: " + forumId);
		return "Error: No priveleges for operation";
	}else{
		logger.logSuccess("Forum: " + forumId + " deleted");
		return forumDA.deleteForum(forumId);
	}
}

export function editForum(forumId, newName, desc, user) {
	if(!priveleges.forumExists(forumId)){
		logger.logFail("Forum editing failed. forum doesn't exist with name: " + name);
		return "Error: Forum Doesn't Exist";
	}else if(!priveleges.isAdminInForum(forumId,user)){
		logger.logFail("Forum deletion editing. User is not admin, forum : " + forumId);
		return "Error: No priveleges for operation";
	}else{
		logger.logSuccess("Forum: " + forumId + " edited");
		return forumDA.editForum(forumId, newName, desc);
	}
}


//BOOL
export function register(forumId, userId) {
	notificationCenter.registerUserForNotificationsOnObject(userId, forumId);

	logger.logSuccess("User " + forumId + " added to forum" + forumId);
	return forumDA.register(forumId, userId);
}

//BOOL
export function unregister(forumId, userId) {
	notificationCenter.unregisterUserForNotificationsOnObject(userId, forumId);

	logger.logSuccess("User " + userId + " left forum" + forumId);
	return forumDA.unregister(forumId, userId);
}

export function userInForum(forumName, user){
	return forumDA.userInForum(forumName, user);
}

export function getAllForums() {
	return forumDA.getAllForums();
}

export function getNumOfForums() {
	return forumDA.getNumOfForums();
}

export function forumNameisFree(name) {
	return forumDA.isNameFree(name);
}

export function userCanDeleteForum (forumId, userId) {
	return priveleges.isSuperAdmin(userId) || priveleges.isAdminInForum(forumId, userId);
}

//Array of forums
export function forumsTheUsersHasJoined (userId) {
	return forumDA.forumsTheUsersHasJoined(userId);
}

//Array of forums
export function publicForumListUserIsNotIn (userId) {
	return forumDA.publicForumListUserIsNotIn(userId);
}

export function getForumNameForForumId (forumId) {
	return forumDA.getForumNameForForumId(forumId);
}

export function setForumNotificationSettings(forumId, newSetting, userId) {
	if (getForumNotificationSettingsForForumId(forumId) === newSetting) return;

	if (priveleges.isAdminInForum(forumId, userId)) {
		let opSuccess = forumDA.setForumNotificationSettings(forumId, newSetting);
		if (opSuccess) {
			logger.logSuccess("User " + userId + " changed forum " + forumId + " settings to \"" + newSetting + "\"");
		} else {
			logger.logFail("User " + userId + " failed changing forum " + forumId + " settings to \"" + newSetting + "\"");
		}
		return opSuccess;
	} else {
		logger.logFail("User " + userId + " failed changing forum " + forumId + " settings to \"" + newSetting + "\"" + " because he is not the admin");

		return false;
	}
}

export function getUserNotificationSettingsInForum(forumId, userId) {
	return forumDA.getUserNotificationSettingsInForum(forumId, userId);
}

export function updateUserNotificationSettingsInForum(forumId, userId, newSetting, actingUserId) {
	if (actingUserId !== userId) {
		logger.logFail("User " + userId + " failed changing personal notification settings in forum " + forumId + " because of privilage violation");
		return;
	}

	let updateResult = forumDA.updateUserNotificationSettingsInForum(forumId, userId, newSetting);
	if (updateResult) {
		logger.logSuccess("User " + userId + " changed personal notification settings in forum " + forumId + " to \"" + newSetting + "\"");
	} else {
		logger.logFail("User " + userId + " failed changing personal notification settings in forum " + forumId + " because of DB error");
	}
	return updateResult;
}

export function getForumNotificationSettingsForForumId(forumId) {
	return forumDA.getForumNotificationSettingsForForumId(forumId);
}

export function setForumAllowUserBeModeratorInMultipleSubforums(forumId, newSetting, userId) {
	if (getForumAllowUserBeModeratorInMultipleSubforums(forumId) === newSetting) return;

	if (priveleges.isAdminInForum(forumId, userId)) {
		let opSuccess = forumDA.setForumAllowUserBeModeratorInMultipleSubforums(forumId, newSetting);
		if (opSuccess) {
			logger.logSuccess("User " + userId + " changed forum " + forumId + " allow user moderate multiple subforum settings to \"" + newSetting + "\"");
		} else {
			logger.logFail("User " + userId + " failed changing forum " + forumId + " allow user moderate multiple subforum settings to \"" + newSetting + "\"");
		}
		return opSuccess;
	} else {
		logger.logFail("User " + userId + " failed changing forum " + forumId + " allow user moderate multiple subforum settings to \"" + newSetting + "\"" + " because he is not the admin");

		return false;
	}
}

export function getForumAllowUserBeModeratorInMultipleSubforums(forumId) {
	return forumDA.getForumAllowUserBeModeratorInMultipleSubforums(forumId);
}

export function setForumAllowModeratorDeleteThreads(forumId, newSetting, userId) {
	if (getForumAllowModeratorDeleteThreads(forumId) === newSetting) return;

	if (priveleges.isAdminInForum(forumId, userId)) {
		let opSuccess = forumDA.setForumAllowModeratorDeleteThreads(forumId, newSetting);
		if (opSuccess) {
			logger.logSuccess("User " + userId + " changed forum " + forumId + " allow moderator delete threads to  \"" + newSetting + "\"");
		} else {
			logger.logFail("User " + userId + " failed changing forum " + forumId + " allow moderator delete threads to  \"" + newSetting + "\"");
		}
		return opSuccess;
	} else {
		logger.logFail("User " + userId + " failed changing forum " + forumId + " allow moderator delete threads to  \"" + newSetting + "\"" + " because he is not the admin");
		throw new Meteor.Error(privilegesErrorName, "You have no permission to add moderators to this sub forum");
		return false;
	}
}

export function getForumAllowModeratorDeleteThreads(forumId) {
	return forumDA.getForumAllowModeratorDeleteThreads(forumId);
}