const messagingDA = require("../../DAL/MessagesDA");
const logger = require("../logger");
const privileges = require("../Authentication/privilegesBL");

//TODO: what is the return type
export function sendMessage(sender, reciever, text) {
	logger.logSuccess("Message : " + text + " sent successefuly");
	return messagingDA.sendMessage(sender, reciever, text);
}

//TODO: what is the return type
export function readMessages(userId) {
	logger.logSuccess("Messages read for user " + userId);
	return messagingDA.readMessages(userId);
}

export function getAllMessages() {
	return messagingDA.getAllMessages();
}