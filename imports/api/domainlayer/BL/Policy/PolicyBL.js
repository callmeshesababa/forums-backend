const policyDA = require("../../DAL/PolicyDA");
const logger = require("../logger");
const priveleges = require("../Authentication/privilegesBL");
const notificationCenter = require("../NotificationCenter/NotificationCenterBL");

export function getPolicy() {
	return policyDA.getPolicy();
}

export function setPassExporationPeriod (period) {
	return policyDA.setPassExporationPeriod(period);
}

export function setAllowSecurityQuestion (isAllowed) {
	return policyDA.setAllowSecurityQuestion (isAllowed);
}

export function setAllowSecurityQuestionAndPassExporationDate (isAllowed, period) {
	return policyDA.setAllowSecurityQuestion (isAllowed) && policyDA.setPassExporationPeriod(period);
}