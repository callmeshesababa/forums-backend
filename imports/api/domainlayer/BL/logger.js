import { Meteor } from 'meteor/meteor';
const colors = require('colors');

module.exports = {
	logSuccess, 
	logFail,
	logInfo
}

var shouldLog = true;
var shouldLogServerOrClient = false;
var shouldPrintStackTrace = false;

function logSuccess (message) {
	if (shouldLogServerOrClient) {
		if (Meteor.isServer) {
			console.log("Server:".green);
		} else {
			console.log("Client:".green);
		}
	}
	if (shouldLog) {
		console.log(message.green);
		printStackTrace(arguments.callee);
	}
}

function logFail (message) {
	if (shouldLogServerOrClient) {
		if (Meteor.isServer) {
			console.log("Server:".red);
		} else {
			console.log("Client:".red);
		}
	}
	if (shouldLog) {
		console.log(message.red);
		printStackTrace(arguments.callee);
	}
}

function logInfo (message) {
	if (shouldLogServerOrClient) {
		if (Meteor.isServer) {
			console.log("Server:".grey);
		} else {
			console.log("Client:".grey);
		}
	}
	if (shouldLog) {
		console.log(message.grey);
		printStackTrace(arguments.callee);
	}
}

function printStackTrace (callee) {
	if (!shouldPrintStackTrace) return;

	var caller = callee.caller;
	i = 0;

	console.log("Stack trace:");
	while (callee && i < 5) {
		console.log(caller.name);
		caller = caller.caller;
		i++;
	}
	console.log("\n");
}