const threadsDA = require("../../DAL/ThreadsDA");
const logger = require("../logger");
const priveleges = require("../Authentication/privilegesBL");
const subforumsBL = require("../SubForum/subforumBL");
const notificationCenter = require("../NotificationCenter/NotificationCenterBL");

// export function getAllThreads() {
// 	return threadsDA.getAllThreads();
// }

export function threadsForSubforumId (subforumId, userId) {
	//TODO: check if user id can see the threads
	return threadsDA.threadsForSubforumId(subforumId);
}

//returns all comments related to the given postId
export function getCommentsByPost(subforumId, postId) {
	return threadsDA.getCommentsByPost(subforumId, postId);
}

export function postThread(subForumIdToPostTo, postUserId, subject, text) {
	var forumId = subforumsBL.getForumIdBySubforum(subForumIdToPostTo);
	
	//Create thread
	var newThreadId = threadsDA.postThread(subForumIdToPostTo, postUserId, subject, text);

	if (Meteor.isServer) {
		//Send notifications
		notificationCenter.newThreadWasCreatedInForum(forumId, subForumIdToPostTo, newThreadId, postUserId);

		//Register for notifications on thread
		notificationCenter.registerUserForNotificationsOnObject(postUserId, newThreadId);
	}
	
	logger.logSuccess("Posted new post: " + text + " to subForum" + subForumIdToPostTo);
	return newThreadId;
}

export function getThreadArrayById (threadId, userId) {
	//TODO: check user privilagares
	return threadsDA.getThreadArrayById(threadId);
}

export function editThread(threadId, editingUserId, newSubject, newText) {
	//Send notification
	var threadObject = threadsDA.getPostObject(threadId);
	if (!threadObject) {
		logger.logFail("Thread edit failed. thread was not found");
		return;
	}
	var subforumId = threadObject.subForumId;
	var forumId = subforumsBL.getForumIdBySubforum(subforumId);

	if (Meteor.isServer) {
		notificationCenter.threadWasEdited(forumId, subforumId, threadId, editingUserId);
	}

	//Edit the thread
	logger.logSuccess("Edited thread: " + threadId + ". new subject: " + newSubject);
	return threadsDA.editThread(threadId, editingUserId, newSubject, newText);
}


// //TODO: what is the return type
export function deletePost(subForumId, postId, user) {
	logger.logSuccess("Deleted post: " + postId);
	return threadsDA.deletePost(postId);
}

//add a comment to the given post.
export function comment(postId, user, text) {
	//Register for notifications on thread
	notificationCenter.registerUserForNotificationsOnObject(user._id, postId);

	logger.logSuccess("Comment on post " + postId + ": " + text);
	return threadsDA.addComment(postId, user, text);
}

export function getPost(postId) {
	return threadsDA.getPost(postId);
}

export function getThreadSubjectForThreadId (threadId) {
	return threadsDA.getThreadSubjectForThreadId(threadId);
}