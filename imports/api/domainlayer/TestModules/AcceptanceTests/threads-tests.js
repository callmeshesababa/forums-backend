const threadsDA = require('./../../DAL/threadsDA');
const threadsBL = require('./../../BL/Threads/threadsBL');
const authBL = require("./../../BL/Authentication/authenticationBL");
const forumBL = require('./../../BL/Forum/forumBL');
const subForumBL = require('./../../BL/SubForum/subforumBL');
const threadsSL = require('./../../SL/threads-service');
const authSL = require('./../../SL/auth-services');

var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testThreadsSL_posts();
	testThreadsSL_comments();
}

function testThreadsSL_posts () {
	describe('User add a post to data base tests - SL', function() {
		var userName = 'user4' //admin
		var password = 'pP12345!'
		var email = 'litalmor6@gmail.com'
		var forumId = forumBL.create('ForumName3', 'forum-description3', userName, password, email) //create forum
		var accessToken = authSL.loginUser(userName, password, forumId)
		var subForumId = subForumBL.createSubForum(accessToken, forumId, 'SubForumName3' , 'description3', userName)
		var text = 'text test'
		var pid = threadsSL.post(accessToken, subForumId, userName, text)
		var pid2 = threadsSL.post(accessToken, subForumId, userName, text)

		it(userName + ' tries to add posts p1, p2 to subForum1 when user is loggedin.  in post()', function () {
		 	expect(pid).to.be.ok
		});

		var post = threadsSL.getPost(accessToken, pid)
		
		it(userName + 'tries to get a post p1 when user is loggedin. in getPost(p1)', function () {
			expect(post).to.be.deep.eql({pid: pid, subForumId: subForumId, userName: userName, text: text});
		});	

		var isDeleted = threadsSL.deletePost(accessToken, pid)
		
		it(userName + ' tries to delete a post p1 when he is loggedin. deletePost()', function () {
		 	expect(isDeleted).to.be.ok
		});

		var checkPost = threadsSL.getPost(accessToken, pid)
		
		it(userName + 'tries to get a post p1 after p1 was deleted when user is loggedin. in getPost(p1)', function () {
			expect(checkPost).to.not.be.ok
		});	
	
		accessToken = null //TODO - change for logout
		var pid3 = threadsSL.post(accessToken, subForumId, userName, text)
		
		it(userName + ' tries to add a post p3 user is loggedout. post()', function () {
		 	expect(pid3).to.not.be.ok;
		});

		var checkPost2 = threadsSL.getPost(accessToken, pid)
		
		it(userName + 'tries to get a post p2 when user is loggedout. in getPost(p2)', function () {
			expect(checkPost2).to.not.be.ok
		});	

		var isDeleted2 = threadsSL.deletePost(accessToken, pid2)
		
		it(userName + ' tries to delete a post p2 when user is loggedout. in deletePost()', function () {
		 	expect(isDeleted2).to.not.be.ok
		});

		threadsDA.deleteAllComments()
		threadsDA.deleteAllPosts()
	});
}

function testThreadsSL_comments () {
	describe('User add a comment to data base tests - SL', function() {
		var userName = 'user5' //admin
		var password = 'pP12345!'
		var email = 'lital_morali2@walla.co.il'
		var forumId = forumBL.create('ForumName4', 'forum-description4', userName, password, email)
		var accessToken = authSL.loginUser(userName, password, forumId)
		var subForumId = subForumBL.createSubForum(accessToken, forumId, 'SubForumName4' , 'description4', userName)
		var postText = 'post test'
		var text = 'text'
		var pid = threadsSL.post(accessToken, subForumId, userName, postText)
		var pid2 = threadsSL.post(accessToken, subForumId, userName, postText)
		var cid1 = threadsSL.addComment(accessToken, pid, userName, text)
		var cid2 = threadsSL.addComment(accessToken, pid, userName, text)
		var cid3 = threadsSL.addComment(accessToken, pid, userName, text)
		var cid4 = threadsSL.addComment(accessToken, pid2, userName, text)

		it(userName + ' tries to add comments c1, c2, c3 to p1 when user is loggedin. in addComment()', function () {
		 	expect(cid1).to.be.ok
		 	expect(cid2).to.be.ok
		 	expect(cid3).to.be.ok
		});

		it(userName + ' tries to add another comment c4 to p2 when user is loggedin. in addComment()', function () {
		 	expect(cid4).to.be.ok
		});
		
		var comment = threadsBL.getComment(accessToken, cid2)
		
		it(userName + ' tries to get comment c2 from p1 when is loggedin. in getComment(c2)', function () {
			expect(comment).to.be.deep.eql( {cid: cid2, postId: pid, userName: userName, text: text });
		});	

		var isDeletedComment = threadsSL.deleteComment(accessToken, cid1)
		
		it(userName + ' tries to delete a comment c1 from p1 when user is loggedin. in deleteComment(c1)', function () {
			expect(isDeletedComment).to.be.ok;
		});	

		var checkComment = threadsSL.getComment(accessToken, cid1)
		
		it(userName + ' tries to get comment c1 from p1 afer c1 has been deleted. in getComment(c1)', function () {
			expect(checkComment).to.not.be.ok
		});	

		threadsBL.deletePost(pid)
		var checkComment1 = threadsSL.getComment(cid1)
		var checkComment2 = threadsSL.getComment(cid2)
		var checkComment3 = threadsSL.getComment(cid3)

		it(userName + ' tries to get comments c1, c2, c3 after p1 has been deleted when user is loggedin. in deletePost(p1)', function () {
			expect(checkComment1).to.not.be.ok;
			expect(checkComment2).to.not.be.ok;
			expect(checkComment3).to.not.be.ok;
		});	

		accessToken = null //TODO - change for logout
		var checkComment4 = threadsSL.addComment(accessToken, pid2, userName, text)

		it(userName + ' tries to add a comment c4 to p2 when user is loggedout', function () {
		 	expect(checkComment4).to.not.be.ok;
		});

		var isDeletedComment4 = threadsSL.deleteComment(accessToken, cid4)
		
		it(userName + ' tries to delete a comment c4 from p2 when user is loggedout. in deleteComment()', function () {
		 	expect(isDeletedComment4).to.not.be.ok
		});

		threadsDA.deleteAllComments()
		threadsDA.deleteAllPosts()
	});
}
