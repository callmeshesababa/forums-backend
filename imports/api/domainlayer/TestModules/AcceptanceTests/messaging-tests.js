
var messagingSL = require('./../../SL/messaging-service');
const authBL = require("./../../BL/Authentication/authenticationBL");
const forumBL = require('./../../BL/Forum/forumBL');


var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testSendMessage();
	testReadMessage();
}

function testSendMessage() {
	describe('Testing messaging module functions - sendMessage', function() {
		var sender = 'elad' 
		var password = '111'
		var email = 'eladmaymon1812@gmail.com'
		var forumId = forumBL.create('EladsForum', 'forum-description', sender, password, email) //create forum
		var senderToken = authBL.login(sender, password, forumId);
		var receiver = 'lital';
		var password2 = '222';
		var email2 = 'litalmor5@gmail.com'
		var receiverToken = authBL.register(receiver, password2, forumId, email2);
		var messageBody = "make me some toast woman";
		var message = messagingSL.sendMessage(forumId, senderToken, sender, receiver, messageBody);
		it('message sender should be elad', function () {
			expect(message.sender).to.equal('elad');
		});
		it('message receiver should be lital', function () {
			expect(message.receiver).to.equal('lital');
		});
		it("message sender should be 'make me some toast woman'", function () {
			expect(message.messageBody).to.equal('make me some toast woman');
		});

		});
}

function testReadMessage() {
	describe('Testing messaging module functions - readMessages', function() {

		var sender = 'michael'
		var password = '333'
		var email = 'mish@gmail.com'
		var forumId = forumBL.create('MichaelsForum', 'forum-description', sender, password, email) //create forum
		var senderToken = authBL.login(sender, password, forumId);
		var receiver = 'lital';
		var password2 = '222';
		var email2 = 'litalmor5@gmail.com'
		var receiverToken = authBL.register(receiver, password2, forumId, email2);
		var sender2 = 'gal';
		var password22 = '444';
		var email22 = 'shtits@gmail.com'
		var senderToken2 = authBL.register(sender2, password22, forumId, email22);
		var messageBody1 = "make me some toast woman"
		var message1 = messagingSL.sendMessage(forumId, senderToken, sender, receiver, messageBody1);
		var messageBody2 = "clean the dishes Bi*ch"
		var message2 = messagingSL.sendMessage(forumId, senderToken2, sender2, receiver, messageBody2);

		var unreadMessagesOfLital = messagingSL.readMessages(forumId, receiverToken, receiver); //sender : {messageId : {messageId, sender,receiver, messageBody, unread}}
		var keys = Object.keys(unreadMessagesOfLital);// keys are sender names
		for(i=0; i<keys.length; i++){
			if (keys[i] == 'gal'){
				var galsKeys = Object.keys(unreadMessagesOfLital['gal']);//keys are messageIds
				for(j=0; j<galsKeys.length; j++){
					sender = unreadMessagesOfLital[keys[i]][galsKeys[j]].sender;
					body = unreadMessagesOfLital[keys[i]][galsKeys[j]].messageBody;
					it(receiver + ' read a message from ' + sender, function () {
						expect(sender).to.equal('gal');
					});
					it('the message body that ' + sender +' sent to ' + receiver + ' was clean the dishes Bi*ch' , function () {
						expect(body).to.equal('clean the dishes Bi*ch');
					});
				}
			}
			else if (keys[i] == 'michael'){
				var michaelKeys = Object.keys(unreadMessagesOfLital['michael']);
				for(j=0; j<michaelKeys.length; j++){
					sender1 = unreadMessagesOfLital[keys[i]][michaelKeys[j]].sender;
					body1 = unreadMessagesOfLital[keys[i]][michaelKeys[j]].messageBody;
					it(receiver + ' read a message from ' + sender1, function () {
						expect(sender1).to.equal('michael');
					});
					it('the message body that ' + sender1 +' sent to ' + receiver + ' was make me some toast woman' , function () {
						expect(body1).to.equal('make me some toast woman');
					});
				}
			}
		}
	});		
}