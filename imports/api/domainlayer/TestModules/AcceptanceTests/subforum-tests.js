
var subforumSL = require('./../../SL/subforum-service');
var subforumDA = require('./../../DAL/SubForumsDA');
var subforumBL = require('./../../BL/SubForum/subforumBL');
var authSL = require('./../../SL/auth-services');
var forumSL = require('./../../SL/forum-services');


var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testSubForumSL();
}


function testSubForumSL () {
	describe('Forum acceptance tests - Services', function() {
		var name = "Testing Forum";
		var description = "This Forum is created for unit tests"
		var username = "galste";
		var password = "123";
		var email = "gal.steinberg@gmail.com";


		var addForum = forumSL.createForum("Forumdsd", "blaaaa", "gal", "123", "asdsadsad@sadsa.com");
		var admin = authSL.loginUser("gal","123",addForum);

		var manager = authSL.registerUser(username, password, addForum, "blaasd@sd.com");
		var manager = authSL.loginUser(username,password,addForum);

		var subforum = subforumSL.createSubForum(admin, addForum, "Cool Subforum1", "Some description", username);
		var subforum1 = subforumSL.createSubForum(manager, addForum, "Cool Subforum1", "Some description", username);

		var editedSubForum = subforumSL.editSubForum(manager,subforum,"edited","edited descr");

		var manager1 = authSL.registerUser("misdke2","112323",addForum,"gaasl@gal.com");
		var addedManger = subforumSL.addSubForumManager(admin, subforum, "misdke2");

		var deleted = subforumSL.deleteSubForum(admin,subforum);


		it('forum name ' + name + ' should Be created', function () {
			expect(subforum).to.be.ok;
		});

		it('forum name ' + name + ' should NOT be created', function () {
			expect(subforum1).to.be.false;
		});

		it('forum name ' + name + ' edit operation should edit forum', function () {
			expect(editedSubForum).to.be.ok;
		});

		it('forum name ' + name + ' add manager operation should add mangager to forum', function () {
			expect(addedManger).to.be.true;
		});

		it('forum name ' + name + ' delete operation should NOT delete forum', function () {
			expect(deleted).to.be.true;
		});
	});
}