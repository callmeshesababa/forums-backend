var authtests = require("./auth-tests");
var threadsTests = require("./threads-tests");
var forumtests = require("./forum-tests");
var subforumtests = require("./subforum-tests");
var messagingtests = require ("./messaging-tests.js")
var systemInitTests = require ("./systemInit-tests.js")

function runTests() {
	authtests.test();
	forumtests.test();
	subforumtests.test();
	threadsTests.test();
	messagingtests.test();
	systemInitTests.test();
}

runTests();