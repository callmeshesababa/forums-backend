
var authServices = require('./../../SL/auth-services');
var userDA = require('./../../DAL/UserDA');
var authBL = require('./../../BL/Authentication/authenticationBL');

var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testAuthServices();
}


function testAuthServices () {
	describe('User register and login logic test - SL', function() {
		var forumId = "asdfasdf3238r3";
		var username = "michaeltzach23";
		var password = "pass123";
		var userEmail = "galush@gmail.com";

		var isUsernameIsFreeInForum1 = authServices.isUserNameFreeInForum(username, forumId);
		var registerStatus1 = authServices.registerUser(username, password, forumId, userEmail);
		var isAccessTokenValid1 = authServices.isAccessTokenValid(registerStatus1.accessToken);
		var isUsernameIsFreeInForum2 = authServices.isUserNameFreeInForum(username, forumId);
		var registerStatus2 = authServices.registerUser(username, password, forumId);
		var accessToken1 = authServices.loginUser(username, password, forumId);

		it('username should be free in isUserNameFreeInForum()', function () {
			expect(isUsernameIsFreeInForum1).to.be.true;
		});

		it('check if user registeration was successful in registerUser()', function () {
			expect(registerStatus1.registerSuccess).to.be.true;
		});

		it('check if getting an access token using registerUser()', function () {
			expect(registerStatus1.accessToken).to.be.ok;
		});

		it('check if access token is valid in the system using isAccessTokenValid()', function () {
			expect(isAccessTokenValid1).to.be.true;
		});

		it('username should not be free in isUserNameFreeInForum()', function () {
			expect(isUsernameIsFreeInForum2).to.be.false;
		});

		it('check if user registeration was failure for duplicate user in registerUser()', function () {
			expect(registerStatus2.registerSuccess).to.be.false;
		});

		it('check if user registeration was failure for duplicate user in registerUser() and didnt get access token', function () {
			expect(registerStatus2.accessToken).to.be.a('null');
		});

		it('check if getting an access token using loginUser()', function () {
			expect(accessToken1).to.be.ok;
		});
	});
}
























