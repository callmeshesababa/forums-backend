
var forumSL = require('./../../SL/forum-services');
var forumDA = require('./../../DAL/ForumDA');
var forumBL = require('./../../BL/Forum/forumBL');
var authSL = require('./../../SL/auth-services');


var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testForumSL();
}


function testForumSL () {
	describe('Forum acceptance tests - Services', function() {
		var name = "Testing Forum";
		var description = "This Forum is created for unit tests"
		var username = "galste";
		var password = "123";
		var email = "gal.steinberg@gmail.com";

		var token = authSL.registerUser("mike","123",forum1,"gal@gal.com")

		var forum1 = forumSL.createForum(name, description, username,password, email);
		var forum2 = forumSL.createForum(name, description, username,password, email);
		var admin = authSL.loginUser(username, password,forum1);
		var token = authSL.registerUser("mike","123",forum1,"gal@gal.com");
		var token = authSL.loginUser("mike","123", forum1);

		console.log("something: " + admin);
		console.log("something2: " + forum1);

		var edited = forumSL.editForum(admin, forum1, 'new name', 'edited forum');

		var deleted = forumSL.deleteForum(admin,forum1);
		var deleted1 = forumSL.deleteForum(admin,forum1);


		it('forum name ' + name + ' should Be created', function () {
			expect(forum1).to.be.ok;
		});

		it('forum name ' + name + ' should NOT be created', function () {
			expect(forum2).to.be.false;
		});

		it('forum name ' + name + ' edit operation should edit forum', function () {
			expect(edited).to.be.ok;
		});

		it('forum name ' + name + ' delete operation should delete forum', function () {
			expect(deleted).to.be.true;
		});

		it('forum name ' + name + ' delete operation should NOT delete forum', function () {
			expect(deleted1).to.be.false;
		});
	});
}
