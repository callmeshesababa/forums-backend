var systemSL = require('./../../SL/systemInit-services');
var systemDA = require('./../../DAL/SystemDA');
var systemBL = require('./../../BL/System/systemBL');

var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testSystemSL();
}

function testSystemSL () {
	describe('System Init tests - Services', function() {
		var username = "michaeltzach";
		var password = "pass1";

		var adminAccessToken1 = systemDA.createAdminSession (username, password);
		var createAdmin1res = systemDA.createAdmin(username, password);


		var adminAccessToken1 = systemSL.loginAsSystemAdmin (username, password);
		var adminAccessToken2 = systemSL.loginAsSystemAdmin (username + "2", password);
		
		it('should get access token for good credentials with loginAsSystemAdmin()', function () {
			expect(adminAccessToken1).to.be.ok;
		});

		it('should not get access token for bad credentials with loginAsSystemAdmin()', function () {
			expect(adminAccessToken2).to.not.be.ok;
		});
	});
}