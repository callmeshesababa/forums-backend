
var subforumSL = require('./../../SL/subforum-service');
var subforumDA = require('./../../DAL/SubForumsDA');
var subforumBL = require('./../../BL/SubForum/subforumBL');
var authSL = require('./../../SL/auth-services');
var forumSL = require('./../../SL/forum-services');


var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testSubForumDA();
	testSubForumBL();
}

function testSubForumDA() {
	describe('Testing Subforum module functions - DAL', function() {
		var name = "Testing Sub Forum";
		var description = "This Sub Forum is created for unit tests"
		var username = "galste";
		var password = "123";
		var email = "gal.steinberg@gmail.com";


		var addForum = forumSL.createForum("Forum", "blaaaa", "gal", "123", "asdsadsad@sadsa.com");
		var admin = authSL.loginUser("gal","123",addForum);

		var manager = authSL.registerUser(username, password, addForum, "blaasd@sd.com");
		var manager = authSL.loginUser(username,password,addForum);

		var isForumNameFree = subforumDA.isNameFree(name);
		var addsubForum = subforumDA.addSubForum(addForum, name, description, username);
		var isForumNameFree1 = subforumDA.isNameFree(name);
		var addsubForum1 = subforumDA.addSubForum(addForum,name, description, username);

		var editedForumid = subforumDA.editSubForum(addsubForum,'Edited Sub Forum', 'This is edited');


		var deletedForum1 = subforumDA.deleteSubForum(addForum);

		it('forum name ' + name + ' should Be Free In isNameFree()', function () {
			expect(isForumNameFree).to.be.true;
		});

		it('forum name ' + name + ' should be added addForum()', function () {
			expect(addsubForum).to.be.ok;
		});

		it('forum name ' + name + ' should NOT Be Free In isNameFree()', function () {
			expect(isForumNameFree1).to.be.false;
		});

		it('forum name ' + name + ' should be added addForum()', function () {
			expect(addsubForum1).to.be.ok;
		});

		it('forum name ' + name + ' should be edited editForum()', function () {
			expect(editedForumid).to.be.equal(addsubForum);
		});

		it('forum name ' + name + ' should be deleted deleteForum()', function () {
			expect(deletedForum1).to.be.true;
		});

	});
}


function testSubForumBL() {
	describe('Testing Subforum module functions - BL', function() {
		var name = "Testing Sub Forum";
		var description = "This Sub Forum is created for unit tests"
		var username = "galste";
		var password = "123";
		var email = "gal.steinberg@gmail.com";

		var addForum = forumSL.createForum("Forumd", "blaaaa", "gal", "123", "asdsadsad@sadsa.com");
		var admin = authSL.loginUser("gal","123",addForum);

		var manager = authSL.registerUser(username, password, addForum, "blaasd@sd.com");
		var manager = authSL.loginUser(username,password,addForum);

		var isForumNameFree = subforumBL.subforumNameisFree(name);
		var addsubForum = subforumBL.createSubForum(admin,addForum, name, description, username);
		var isForumNameFree1 = subforumBL.subforumNameisFree(name);
		var addsubForum1 = subforumBL.createSubForum(manager,addForum,name, description, username);

		var editedForumid = subforumBL.editSubForum(admin, addsubForum,'Edited Sub Forum', 'This is edited');

		var deletedForum1 = subforumBL.deleteSubForum(admin, addsubForum);

		it('forum  ' + name + ' should Be Free In isNameFree()', function () {
			expect(isForumNameFree).to.be.true;
		});

		it('forum  ' + name + ' should be added addForum()', function () {
			expect(addsubForum).to.be.ok;
		});

		it('forum  ' + name + ' should NOT Be Free In isNameFree()', function () {
			expect(isForumNameFree1).to.be.false;
		});

		it('forum  ' + name + ' should NOT be added addForum()', function () {
			expect(addsubForum1).to.be.false;
		});

		it('forum  ' + name + ' should be edited editForum()', function () {
			expect(editedForumid).to.be.ok;
		});

		it('forum  ' + name + ' should be deleted deleteForum()', function () {
			expect(deletedForum1).to.be.true;
		});
	});
}

