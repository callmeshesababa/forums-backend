
var authServices = require('./../../SL/auth-services');
var userDA = require('./../../DAL/UserDA');
var authBL = require('./../../BL/Authentication/authenticationBL');

var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testUserDA_addUser();
	testUserDA_sessionManagement();
	testAuthBL_registerAndLogin();
}

function testUserDA_addUser () {
	describe('User add to database tests - DAL', function() {
		var forumId = "asdfvuhasd89y923h";
		var username = "michaeltzach";
		var password = "pass1";
		var email = "michaeltzach@mail.com";

		var addUserEmptyUsername = userDA.addUser("", password, forumId, email);
		var addUserEmptyPassword = userDA.addUser(username, "", forumId, email);
		var addUserEmptyForumId = userDA.addUser(username, password, "", email);
		var addUserEmptyEmail = userDA.addUser(username, password, forumId, "");


		var isUsernameIsFreeInForumRes1 = userDA.isUsernameIsFreeInForum(username, forumId);
		var addUserRes1 = userDA.addUser(username, password, forumId, email);
		var isUsernameIsFreeInForumRes2 = userDA.isUsernameIsFreeInForum(username, forumId);
		var addUserRes2 = userDA.addUser(username, password, forumId, email);

		var emailVerificationCode = userDA.createEmailVerificationCode(forumId, username, email);
		var isUserVerifiedBefore = userDA.isUserEmailVerified(forumId, username);
		userDA.verifyEmail(emailVerificationCode, 5);
		var isUserVerifiedAfter = userDA.isUserEmailVerified(forumId, username);

		it('Should generate email verification code using createEmailVerificationCode()', function () {
			expect(emailVerificationCode).to.be.ok;
		});

		it('User email should be unverified before using the verification code', function () {
			expect(isUserVerifiedBefore).to.be.false;
		});

		it('User email should be verified after using the verification code', function () {
			expect(isUserVerifiedAfter).to.be.true;
		});

		it('Should not be able to create user with empty username when using addUser()', function () {
			expect(addUserEmptyUsername).to.not.be.true;
		});

		it('Should not be able to create user with empty password when using addUser()', function () {
			expect(addUserEmptyPassword).to.not.be.true;
		});

		it('Should not be able to create user with empty forum id when using addUser()', function () {
			expect(addUserEmptyForumId).to.not.be.true;
		});

		it('Should not be able to create user with empty email when using addUser()', function () {
			expect(addUserEmptyEmail).to.not.be.true;
		});

		it('username ' + username + ' should Be Free In isUsernameIsFreeInForum()', function () {
			expect(isUsernameIsFreeInForumRes1).to.be.true;
		});


		it('username ' + username + ' should be added addUser()', function () {
			expect(addUserRes1).to.be.true;
		});

		it('username ' + username + ' should not Be Free In isUsernameIsFreeInForum()', function () {
			expect(isUsernameIsFreeInForumRes2).to.be.false;
		});

		it('username ' + username + ' should not be added addUser()', function () {
			expect(addUserRes2).to.be.false;
		});
	});
}

function testUserDA_sessionManagement () {
	describe('User session management tests - DAL', function() {
		var forumId = "asdfvuhasd89y923h";
		var username = "michaeltzach";
		var password = "pass1";

		var accessToken1 = userDA.createSession(username, password, forumId);
		var userForAccessToken1 = userDA.userForAccessToken(accessToken1);
		userDA.destroySession(accessToken1);
		var userForAccessToken2 = userDA.userForAccessToken(accessToken1);

		it('generate access token in createSession()', function () {
			expect(accessToken1).to.be.ok;
		});

		it('check if can get user for credentials using userForAccessToken()', function () {
			expect(userForAccessToken1.username).to.equal(username);
			expect(userForAccessToken1.password).to.equal(password);
			expect(userForAccessToken1.forumId).to.equal(forumId);
		});

		it('check if destroySession() destroys a session', function () {
			expect(userForAccessToken2).to.be.a('null');
		});

	});
}

function testAuthBL_registerAndLogin () {
	describe('User register and login logic test - BL', function() {
		var forumId = "asdfasdf3238r";
		var username = "michaeltzach2";
		var password = "pass12";
		var email = "michaeltzach@mail.com"

		var accessToken1 = authBL.register(username, password, forumId, email);
		var isLoggedIn1 = authBL.isLoggedIn(accessToken1);
		var accessToken2 = authBL.login(username, password, forumId);
		var isLoggedIn2 = authBL.isLoggedIn(accessToken1);
		var accessToken3 = authBL.login(username, password + "2", forumId);

		it('should get access token after register()', function () {
			expect(accessToken1).to.be.ok;
		});

		it('user should be logged in in isLoggedIn() using access token from register()', function () {
			expect(isLoggedIn1).to.be.true;
		});

		it('should get access token after login()', function () {
			expect(accessToken2).to.be.ok;
		});

		it('user should be logged in in isLoggedIn() using access token from login()', function () {
			expect(isLoggedIn2).to.be.true;
		});

		it('should not get access token after login() using wrong credentials', function () {
			expect(accessToken3).to.be.a('null');
		});
	});
}
























