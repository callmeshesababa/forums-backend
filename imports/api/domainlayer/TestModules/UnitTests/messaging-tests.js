
const messagingDAL = require("./../../DAL/MessagesDA");
const messagingBL = require("./../../BL/Messaging/messagingBL");
const authSL = require('./../../SL/auth-services');
const forumSL = require('./../../SL/forum-services');


var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testMessagingDA();
	testMessagingBL();
}

function testMessagingDA() {
	describe('Testing messaging module functions - DAL', function() {
		var forumid = 1;
		var sender = "gal";
		var reciever = "elad";
		var text = "message text";

		sent_message1 = messagingDAL.addMessage(forumid,sender,reciever,text);
		read1 = sent_message1.unread;

		it('message from gal to elad is sent', function () {
			expect(sent_message1.messageId).to.be.ok;
		});

		it('message reciever should be elad)', function () {
			expect(sent_message1.receiver).to.equal('elad');
		});

		it('message should be unread', function () {
			expect(read1).to.be.true;
		});

		read_message1 = messagingDAL.readMessages(forumid,reciever);

		it('message from gal to elad is recieved in elads inbox', function () {
			expect(read_message1).to.be.ok;
		});
		
		it('message sender should be gal)', function () {
			expect(read_message1).not.to.be.equal(null);
		});
	});
}


function testMessagingBL() {
	describe('Testing messaging module functions - BL', function() {
		var sender = "gall";
		var reciever = "eladd";
		var text = "message text";
		var password = "123";

		var addForum = forumSL.createForum("Forummm", "blaaaa", "gasl", "123", "asdsadsad@sadsa.com");
		// console.log(addForum);
		var user1 = authSL.registerUser(sender, password, addForum, "blaasd@sd.com");
		var user2 = authSL.registerUser(reciever, password, addForum, "blaasd@sd.com");

		var user1 = authSL.loginUser(sender,password,addForum);
		var user2 = authSL.loginUser(reciever,password,addForum);

		sent_message = messagingBL.sendMessage(addForum,user1,sender,reciever,text);
		read = sent_message.unread;

		it('message from gal to elad is sent', function () {
			expect(sent_message.messageId).to.be.ok;
		});

		it('message reciever should be elad)', function () {
			expect(sent_message.receiver).to.equal('eladd');
		});

		it('message should be unread', function () {
			expect(read).to.be.true;
		});

		read_message = messagingBL.readMessages(addForum,user2,reciever);

		it('message from gal to elad is recieved in elads inbox', function () {
			expect(read_message).to.be.ok;
		});
		
		it('message sender should be gal)', function () {
			expect(read_message).not.to.be.equal(null);
		});
	});
}