const threadsDA = require('./../../DAL/threadsDA');
const threadsBL = require('./../../BL/Threads/threadsBL');
const authBL = require("./../../BL/Authentication/authenticationBL");
const forumBL = require('./../../BL/Forum/forumBL');
const subForumBL = require('./../../BL/SubForum/subforumBL');
const threadsSL = require('./../../SL/threads-service');
const authSL = require('./../../SL/auth-services');

var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testThreadsDA_post()
	testThreadsDA_comments()
	testThreadsBL_posts()
	testThreadsBL_comments()
}

function testThreadsDA_post () {
	describe('User add a post to data base tests - DAL', function() {
		var userName = 'user1'
		var text = 'text test1'
		var subForumId = '123'
		var pid = threadsDA.post(subForumId, userName, text);	

		it(userName + ' has been added a post and got pid. in post()', function () {
		 	expect(pid).to.be.ok;
		});

		var post = threadsDA.getPost(pid)
		
		it(userName + ' posted a post and got this post by pid. in getPost() ', function () {
			expect(post).to.be.deep.eql({pid: pid, subForumId: subForumId, userName: userName, text: text});
		});	

		threadsDA.deletePost(pid)
		var post2 = threadsDA.getPost(pid)
		
		it(userName + ' delete a post. in deletePost()', function () {
			expect(post2).to.not.be.ok;
		});	

		threadsDA.deleteAllComments()
		threadsDA.deleteAllPosts()
	});
}

function testThreadsDA_comments () { //(accessToken, pid, userName, text)
	describe('User add a comment to the data base tests - DAL', function() {
		var userName = 'user1'
		var postText = 'post text'
		var text = 'comment text'
		var text2 = 'comment text2'
		var subForumId = '123'
		var pid = threadsDA.post(subForumId, userName, postText);
		var cid = threadsDA.addComment(pid, userName, text)	
		var pid2 = threadsDA.post(subForumId, userName, postText);
		var cid2 = threadsDA.addComment(pid, userName, text2)	

		it(userName + ' has been added a comment to a post p1 and got cid. in addComment()', function () {
		 	expect(cid).to.be.ok;
		});

		var comment = threadsDA.getComment(cid2)
		
		it(userName + ' posted a comment c2 to p1 and got this comment by cid. in getComment() ', function () {
			expect(comment).to.be.deep.eql( {cid: cid2, postId: pid, userName: userName, text: text2 });
		});	

		threadsDA.deleteComment(cid)
		var checkComment = threadsDA.getComment(cid)
		
		it(userName + ' delete a comment c1 from p1. in deleteComment(c1)', function () {
			expect(checkComment).to.not.be.ok;
		});	

		var comment2 = threadsDA.getComment(cid2)
		
		it('get comment c2 from p1. in getComment(c2)', function () {
			expect(comment2).to.be.deep.eql( {cid: cid2, postId: pid, userName: userName, text: text2 });
		});	

		threadsDA.deleteCommentsByPost(pid)
		var checkComment2 = threadsDA.getComment(cid2)

		it('tries to get comment c2 from p1. in deleteCommentsByPost(p1)', function () {
			expect(checkComment2).to.not.be.ok;
		});	
		
		threadsDA.deleteAllComments();
	});
}

function testThreadsBL_posts () {
	describe('User add a post to data base tests - BL', function() {
		var userName = 'user2' //admin
		var password = 'pP12345!'
		var email = 'litalmor5@gmail.com'
		var forumId = forumBL.create('ForumName', 'forum-description', userName, password, email) 
		var accessToken = authBL.login(userName, password, forumId)
		var subForumId = subForumBL.createSubForum(accessToken, forumId, 'SubForumName' , 'description', userName)
		var text = 'text test2' //post
		var pid = threadsBL.post(accessToken, subForumId, userName, text)
		var pid2 = threadsBL.post(accessToken, subForumId, userName, text)

		it(userName + ' tries to add a post p1 when he is login.  in post()', function () {
		 	expect(pid).to.be.ok
		});

		it(userName + ' tries to add another post p2 to the same subForum when he is login. in post()', function () {
		 	expect(pid2).to.be.ok
		});

		var isDeleted = threadsBL.deletePost(accessToken, pid)
		
		it(userName + ' tries to delete a post p1 when he is loggedin. deletePost()', function () {
		 	expect(isDeleted).to.be.ok
		});
		
		accessToken = null //TODO - change for logout
		var pid3 = threadsBL.post(accessToken, subForumId, userName, text)
		
		it(userName + ' tries to add a post p3 when he is loggedout. post()', function () {
		 	expect(pid3).to.not.be.ok;
		});

		var isDeleted2 = threadsBL.deletePost(accessToken, pid2)
		
		it(userName + ' tries to delete a post p2 when he is loggedout. in deletePost()', function () {
		 	expect(isDeleted2).to.not.be.ok
		});

		threadsDA.deleteAllComments()
		threadsDA.deleteAllPosts()

	});
}

function testThreadsBL_comments () {
	describe('User add a comment to data base tests - BL', function() {
		var userName = 'user3' //admin
		var password = 'pP12345!'
		var email = 'lital_morali@walla.co.il'
		var forumId = forumBL.create('ForumName2', 'forum-description2', userName, password, email) //create forum
		var accessToken = authBL.login(userName, password, forumId)
		var subForumId = subForumBL.createSubForum(accessToken, forumId, 'SubForumName2' , 'description2', userName)
		// console.log(subForumId)
		var postText = 'post test'
		var text = 'text'
		var pid = threadsBL.post(accessToken, subForumId, userName, postText)
		var pid2 = threadsBL.post(accessToken, subForumId, userName, postText)
		var cid1 = threadsBL.addComment(accessToken, pid, userName, text)
		var cid2 = threadsBL.addComment(accessToken, pid, userName, text)
		var cid3 = threadsBL.addComment(accessToken, pid, userName, text)
		var cid4 = threadsBL.addComment(accessToken, pid2, userName, text)

		it(userName + ' tries to add comments c1, c2, c3 to p1 when he is login. in addComment()', function () {
		 	expect(cid1).to.be.ok
		 	expect(cid2).to.be.ok
		 	expect(cid3).to.be.ok
		});

		it(userName + ' tries to add another comment c4 to p2 when he is login. in addComment()', function () {
		 	expect(cid4).to.be.ok
		});
		
		threadsBL.deleteComment(cid1)
		var checkComment = threadsDA.getComment(cid1)
		
		it(userName + ' delete comment c1 from p1. in deleteComment(c1)', function () {
			expect(checkComment).to.not.be.ok;
		});	

		var comment2 = threadsBL.getComment(accessToken, cid2)
		
		it('get comment c2 from p1. in getComment(c2)', function () {
			expect(comment2).to.be.deep.eql( {cid: cid2, postId: pid, userName: userName, text: text });
		});	

		threadsBL.deletePost(pid)
		var checkComment1 = threadsDA.getComment(cid1)
		var checkComment2 = threadsDA.getComment(cid2)
		var checkComment3 = threadsDA.getComment(cid3)

		it('tries to get comments c1, c2, c3 after p1 had been deleted. in deletePost(p1)', function () {
			expect(checkComment1).to.not.be.ok;
			expect(checkComment2).to.not.be.ok;
			expect(checkComment3).to.not.be.ok;
		});	

		accessToken = null //TODO - change for logout
		var checkcomment5 = threadsBL.addComment(accessToken, pid2, userName, text)

		it(userName + ' tries to add a comment c5 to p2 when he is loggedout', function () {
		 	expect(checkcomment5).to.not.be.ok;
		});

		var isDeletedComment4 = threadsBL.deleteComment(accessToken, cid4)
		
		it(userName + ' tries to delete a comment c4 from p2 when he is loggedout. in deleteComment()', function () {
		 	expect(isDeletedComment4).to.not.be.ok
		});

		threadsDA.deleteAllComments()
		threadsDA.deleteAllPosts()
	});
}



