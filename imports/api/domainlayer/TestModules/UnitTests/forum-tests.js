
var forumSL = require('./../../SL/forum-services');
var forumDA = require('./../../DAL/ForumDA');
var forumBL = require('./../../BL/Forum/forumBL');
var authSL = require('./../../SL/auth-services');


var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testForumDA();
	testForumBL();
}

function testForumDA() {
	describe('Testing forum module functions - DAL', function() {
		var name = "Testing Forum";
		var description = "This Forum is created for unit tests"
		var username = "galste";
		var password = "123";
		var email = "gal.steinberg@gmail.com";

		var isForumNameFree = forumDA.isNameFree(name);
		var addForum = forumDA.addForum(name, description, username, password, email);
		var isForumNameFree1 = forumDA.isNameFree(name);
		var addForum1 = forumDA.addForum(name, description, username, password, email);

		var editedForumid = forumDA.editForum(addForum,'Edited Forum', 'This is edited');

		var deletedForum1 = forumDA.deleteForum(addForum);
		var deletedForum2 = forumDA.deleteForum(addForum);

		it('forum name ' + name + ' should Be Free In isNameFree()', function () {
			expect(isForumNameFree).to.be.true;
		});

		it('forum name ' + name + ' should be added addForum()', function () {
			expect(addForum).to.be.ok;
		});

		it('forum name ' + name + ' should NOT Be Free In isNameFree()', function () {
			expect(isForumNameFree1).to.be.false;
		});

		it('forum name ' + name + ' should NOT be added addForum()', function () {
			expect(addForum1).to.be.ok;
		});

		it('forum name ' + name + ' should be deleted deleteForum()', function () {
			expect(deletedForum1).to.be.true;
		});

		it('forum name ' + name + ' should NOT be deleted deleteForum()', function () {
			expect(deletedForum2).to.be.false;
		});

		it('forum name ' + name + ' should be edited editForum()', function () {
			expect(editedForumid).to.be.equal(addForum);
		});

	});
}


function testForumBL() {
	describe('Testing forum module functions - BL', function() {
		var name = "Testing Forum";
		var description = "This Forum is created for unit tests"
		var username = "galste";
		var password = "123";
		var email = "gal.steinberg@gmail.com";

		var isForumNameFree = forumBL.forumNameisFree(name);
		var addForum = forumBL.create(name, description, username, password, email);
		var admin = authSL.loginUser(username,password,addForum);
		var isForumNameFree1 = forumBL.forumNameisFree(name);
		var addForum1 = forumBL.create(name, description, username, password, email);

		var editedForumid = forumBL.editForum(admin,addForum,'Edited Forum', 'This is edited');

		var deletedForum1 = forumBL.deleteForum(admin,addForum);
		var deletedForum2 = forumBL.deleteForum(admin,addForum);

		it('forum  ' + name + ' should Be Free In isNameFree()', function () {
			expect(isForumNameFree).to.be.true;
		});

		it('forum  ' + name + ' should be added addForum()', function () {
			expect(addForum).to.be.ok;
		});

		it('forum  ' + name + ' should NOT Be Free In isNameFree()', function () {
			expect(isForumNameFree1).to.be.false;
		});

		it('forum  ' + name + ' should NOT be added addForum()', function () {
			expect(addForum1).to.be.false;
		});

		it('forum  ' + name + ' should be deleted deleteForum()', function () {
			expect(deletedForum1).to.be.true;
		});

		it('forum  ' + name + ' should NOT be deleted deleteForum()', function () {
			expect(deletedForum2).to.be.false;
		});

		it('forum  ' + name + ' should be edited editForum()', function () {
			expect(editedForumid).to.be.equal(addForum);
		});

	});
}
