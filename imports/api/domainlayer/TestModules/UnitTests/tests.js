var authtests = require("./auth-tests");
var threadsTests = require("./threads-tests");
var forumtests = require("./forum-tests");
var subforumtests = require("./subforum-tests");
var messagingtests = require ("./messaging-tests")
var systemInitTests = require ("./systeminit-tests")

function runTests() {
	authtests.test();
	forumtests.test();
	subforumtests.test();
	threadsTests.test();
	messagingtests.test();
	systemInitTests.test();
}

runTests();