var systemSL = require('./../../SL/systemInit-services');
var systemDA = require('./../../DAL/SystemDA');
var systemBL = require('./../../BL/System/systemBL');

var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

module.exports = {test};

function test () {
	testSystemDA();
	testSystemBL();
}

function testSystemDA () {
	describe('System Init tests - DAL', function() {
		var username = "michaeltzach";
		var password = "pass1";

		var adminAccessTokenWithEmptyPass = systemDA.createAdmin(username, "");
		var adminAccessTokenWithEmptyUsername = systemDA.createAdmin("", password);

		var adminAccessToken1 = systemDA.createAdminSession (username, password);
		var createAdmin1res = systemDA.createAdmin(username, password);
		var adminAccessToken2 = systemDA.createAdminSession (username, password);
		var createAdmin2res = systemDA.createAdmin(username, password);
		var isAdminWithFalseCred = systemDA.isAdmin(adminAccessToken2 + "2");
		var isAdminWithAccessToke = systemDA.isAdmin(adminAccessToken2);


		it('shouldn\'t be able to get create admin with empty password', function () {
			expect(adminAccessTokenWithEmptyPass).to.not.be.ok;
		});

		it('shouldn\'t be able to get create admin with empty username', function () {
			expect(adminAccessTokenWithEmptyUsername).to.not.be.ok;
		});

		it('shouldn\'t be able to get access token for admin before admin is created', function () {
			expect(adminAccessToken1).to.not.be.ok;
		});

		it('should be able to create first admin with createAdmin()', function () {
			expect(createAdmin1res).to.be.true;
		});

		it('should be able to get access token with createAdminSession()', function () {
			expect(adminAccessToken2).to.be.ok;
		});

		it('should not be able to create another admin with createAdmin()', function () {
			expect(createAdmin2res).to.be.false;
		});

		it('should get false for bad access token with isAdmin()', function () {
			expect(isAdminWithFalseCred).to.be.false;
		});

		it('should get true for good access token with isAdmin()', function () {
			expect(isAdminWithAccessToke).to.be.true;
		});
	});
}

function testSystemBL () {
	describe('System Init tests - BL', function() {
		var username = "michaeltzach";
		var password = "pass1";

		var adminAccessToken1 = systemBL.loginAsSystemAdmin (username, password);
		var adminAccessToken2 = systemBL.loginAsSystemAdmin (username + "2", password);
		
		it('should get access token for good credentials with loginAsSystemAdmin()', function () {
			expect(adminAccessToken1).to.be.ok;
		});

		it('should not get access token for bad credentials with loginAsSystemAdmin()', function () {
			expect(adminAccessToken2).to.not.be.ok;
		});
	});
}