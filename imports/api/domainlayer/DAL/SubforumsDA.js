//Import
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

//Databasee
export const subForumsMongoDB = new Mongo.Collection('subforums');

//Returns forum object or null if unsuccessful
export function addSubForum(parentForumId, name, description, managerId) {
	check(name, String);
	check(description, String);
	var newSubForum = {
		forumId: parentForumId,
		subForumName: name,
		subForumDescription: description,
		createdAt: new Date(),
		subForumManagersId: [{
			managerId : managerId,
			experationDate : null
		}],
	}

	return subForumsMongoDB.insert(newSubForum);
}

export function getSubforumCursorForSubforumId(subforumId) {
	return subForumsMongoDB.find({ _id: subforumId });
}

export function subForumArrayForSubforumId(subforumId) {
    var subForum = subForumsMongoDB.findOne({ _id : subforumId });

    if (subForum) {
    	//Do maintanance on subforum

    	//Remove expired managers
    	var subForumManagersId = subForum.subForumManagersId;
	    var filteredValidSubForumManagersId = subForumManagersId.filter(function (managerObject) {
	    	var experationDate = managerObject.experationDate;
	    	if (!experationDate) return true;
	    	return experationDate > new Date();
    	});
    	if (subForumManagersId.length !== filteredValidSubForumManagersId.length) {
    		subForumsMongoDB.update({ _id : subforumId },
    								{ $set : { subForumManagersId : filteredValidSubForumManagersId}});
    	}
    }

	return subForumsMongoDB.find({ _id : subforumId});
}

export function getSubForumsForForumId (forumId) {
	return subForumsMongoDB.find({'forumId' : forumId});
}

// Returns a WriteResult object that contains the status of the operation.
export function deleteSubForum(subForumId) {
	check(subForumId, String);
	return subForumsMongoDB.remove( { '_id' : subForumId } );
}

// Returns a WriteResult object that contains the status of the operation.
export function editSubForum(subForumId, newName, desc) {
    
	check(newName, String);
	check(desc, String);

	return subForumsMongoDB.update({'_id':subForumId},{$set:{'subForumName':newName,'subForumDescription':desc}});
}

//					Subforum moderators

export function addSubForumManager(subforumId, newManagerId, experationDate) {
	var managerObject = {
		managerId : newManagerId,
		experationDate : experationDate,
	};
	
	removeSubForumManager(subforumId, newManagerId);
	return subForumsMongoDB.update({ '_id' : subforumId },{ $push : {'subForumManagersId' : managerObject }});
}

export function removeSubForumManager(subforumId, managerId) {
	return 	subForumsMongoDB.update({ '_id' : subforumId },{ $pull : {'subForumManagersId' : { managerId : managerId }}});
}

export function editSubForumManagerExperationDate(subforumId, managerId, experationDate) {
	return 	addSubForumManager(subforumId, managerId, experationDate);
}

export function getNumOfMessages(forumId) {
	var result = 0;
	forums =  subForumsMongoDB.find({ 'forumId': forumId });
	forums.forEach( function(item) { result = result + item.posts.count(); } );
	return result;
}

export function getNumOfMessagesForUser(forumId,user) {
	var result = 0;
	forums =  subForumsMongoDB.find({ $and: [ { 'forumId': forumId }, { 'forumId':  forumId }]});
	forums.forEach( function(item) { result = result + item.posts.count(); } );
	return result;
}
    
export function getForumIdBySubforum(subforumId) {
    var subforum = subForumsMongoDB.find({ _id: subforumId }).fetch()[0];
	return subforum && subforum.forumId;
}    

export function isNameFree(name,forumId) {
	check(name, String);
	check(forumId, String);

	return (subForumsMongoDB.find({ $and: [ { 'subForumName': name }, { 'forumId':  forumId }]}).count() == 0);
}

export function subForumExists(subForumId) {
	return (subForumsMongoDB.find({ '_id': subForumId }).count() == 1);
}

export function isManagerInSubForum(subForumId, managerId){
	check(subForumId, String);
    check(managerId, String);
    
    var subForum = subForumsMongoDB.findOne({ _id : subForumId });
    if (!subForum) {
    	return false;
    }

    var managerIds = subForum.subForumManagersId.filter(function (managerObject) {
    	var experationDate = managerObject.experationDate;
    	if (!experationDate) return true;
    	return experationDate > new Date();
    });

    managerIds = managerIds.filter(function(managerObject) {
    	return managerObject.managerId === managerId;
    });

 	return managerIds.length > 0;
}

//returns all subforums in the forumId in the database
export function getSubforumsForForumId(forumId) {
	return subForumsMongoDB.find( { 'forumId': forumId } );
}

export function getAllManagers() {
	return subForumsMongoDB.find( { }, {subForumManagers:1, _id:0} );
}

//Return Bool
export function checkIfUserIsModeratorInOneOfTheSubforumsOfAForum (userId, forumId) {
	let allSubForumsInForum = subForumsMongoDB.find({ forumId: forumId }).fetch();

	var found = {value: false};

	_.each(allSubForumsInForum, function(subforum) {
		if (isManagerInSubForum(subforum._id, userId)) {
			found.value = true;
		}
	});
	
	return found.value;
}