//Import
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

//Databasee
const notificationsForUser = new Mongo.Collection('notificationsForUser');
const usersRegisteredForNewThreadsInSubForumDB = new Mongo.Collection('usersRegisteredForNewThreadsInSubForumDB');

//forumId, subforumId, threadId - are optional
//Return boolean
export function createNotificationForUser(userId, notificationMessage, forumId, subforumId, threadId) {
	check(userId, String);
	check(notificationMessage, String);

	var notificationObject = {
		userId : userId,
		notificationMessage : notificationMessage,
		relativeUrlPathComponents : {
			forumId : forumId,
			subforumId : subforumId,
			threadId : threadId,
		},
		createdAt : new Date(),
		isRead : false,
	}

	return notificationsForUser.insert(notificationObject) ? true : false;
}

export function markNotificationIsRead(notificationId) {
	return notificationsForUser.update(	{ _id : notificationId},
										{ isRead : false });
}

//Returns pointer to a serch for the user notifications. Search is limited to "count" items
export function getNotificationsForUser(userId, count) {
	return notificationsForUser.find(	{ userId : userId }, 
										{ sort : { createdAt : -1 }});
}

//Returns pointer to a serch for the user notifications. Search is limited to "count" items
export function getUnreadNotificationsForUser(userId, count) {
	return notificationsForUser.find(	{ userId : userId, isRead : false }, 
										{ sort : { createdAt : -1 }});
}

//Boolean - success
export function registerUserForNotificationsOnObject (userId, objectId) {
	check(userId, String);
	check(objectId, String);

	var objectRegistrationEntry = usersRegisteredForNewThreadsInSubForumDB.findOne({ objectId : objectId });

	if (objectRegistrationEntry) {
		unregisterUserForNotificationsOnObject(userId, objectId);
		return usersRegisteredForNewThreadsInSubForumDB.update( { objectId : objectId} ,
																{ $push : { registeredUsers : userId }}) ? true : false;
	} else {
		var newobjectRegistrationEntry = {
			objectId : objectId,
			registeredUsers : [userId],
		};
		return usersRegisteredForNewThreadsInSubForumDB.insert(newobjectRegistrationEntry) ? true : false;
	}
}

//Boolean - success
export function unregisterUserForNotificationsOnObject (userId, objectId) {
	check(userId, String);
	check(objectId, String);

	var objectRegistrationEntry = usersRegisteredForNewThreadsInSubForumDB.findOne({ objectId : objectId });
	if (!objectRegistrationEntry) return true;

	return usersRegisteredForNewThreadsInSubForumDB.update(	{ objectId : objectId },
															{ $pull : { registeredUsers : userId }}) ? true : false;
}

//Array of userIds
export function getUsersRegisteredForNotificationsOnObjectId (objectId) {
	check(objectId, String);

	var objectRegistrationEntry = usersRegisteredForNewThreadsInSubForumDB.findOne({ objectId : objectId });
 	
 	if (objectRegistrationEntry) {
 		return objectRegistrationEntry.registeredUsers;
 	} else {
 		return [];
 	}
}







