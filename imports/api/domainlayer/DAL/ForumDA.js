//Import
import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';
import {check} from 'meteor/check';

//Databasee
export const forumsMongoDB = new Mongo.Collection('forums');

import {
  userNotificationSettingsInForum,
  forumNotiificationsSettings,
  allowUserBeModeratorInMultiplePolicies,
  allowModeratorDeleteThreads
} from '../../systemConsts';

//Returns forum object or null if unsuccessful
export function addForum(name, description, userId) {
  check(name, String);
  check(description, String);
  check(userId, String);

  var newForum = {
    forumName: name,
    forumDescription: description,
    createdAt: new Date(),
    forumAdminId: userId,
    users: [],
    userNotificationSettings: {},
    settings: {
      notificationSettings: forumNotiificationsSettings.OFFLINE,
      allowUserBeModeratorInMultiplePolicies: allowUserBeModeratorInMultiplePolicies.ALLOWED,
      allowModeratorDeleteThreads: allowModeratorDeleteThreads.NOTALLOWED
    }
  }

  var result = forumsMongoDB.insert(newForum);
  return result;
}

export function setForumAllowModeratorDeleteThreads(forumId, newSetting) {
  let isValidValue = validateObjectHasProperty(allowModeratorDeleteThreads, newSetting);
  if (isValidValue) {
    var updateSuccess = forumsMongoDB.update({_id: forumId}, {$set: {"settings.allowModeratorDeleteThreads": newSetting}});
    return updateSuccess;
  } else {
    return false;
  }
}

export function getForumAllowModeratorDeleteThreads(forumId) {
  var forum = forumsMongoDB.findOne({_id: forumId});
  if (!forum) return;

  return forum.settings.allowModeratorDeleteThreads;
}

export function setForumAllowUserBeModeratorInMultipleSubforums(forumId, newSetting) {
  let isValidValue = validateObjectHasProperty(allowUserBeModeratorInMultiplePolicies, newSetting);
  if (isValidValue) {
    var updateSuccess = forumsMongoDB.update({_id: forumId}, {$set: {"settings.allowUserBeModeratorInMultiplePolicies": newSetting}});
    return updateSuccess;
  } else {
    return false;
  }
}

export function getForumAllowUserBeModeratorInMultipleSubforums(forumId) {
  var forum = forumsMongoDB.findOne({_id: forumId});
  if (!forum) return;

  return forum.settings.allowUserBeModeratorInMultiplePolicies;
}

export function setForumNotificationSettings(forumId, newSetting) {
  let isValidValue = validateObjectHasProperty(forumNotiificationsSettings, newSetting);

  if (isValidValue) {
    var updateSuccess = forumsMongoDB.update({_id: forumId}, {$set: {"settings.notificationSettings": newSetting}});
    return updateSuccess;
  } else {
    return false;
  }
}

export function getForumNameForForumId(forumId) {
  var forum = forumsMongoDB.findOne({_id: forumId});
  return forum ? forum.forumName : "";
}

export function getForum(forumId) {

  return forumsMongoDB.find({_id: forumId});
}

// Returns a WriteResult object that contains the status of the operation.
export function deleteForum(forumId) {
  check(forumId, String);

  return forumsMongoDB.remove({"_id": forumId})
}

// Returns a WriteResult object that contains the status of the operation.
export function editForum(forumId, newName, desc) {
  check(forumId, String);
  check(newName, String);
  check(desc, String);

  return forumsMongoDB.update({'_id': forumId}, {$set: {'forumName': newName, 'forumDescription': desc}});
}

export function isAdminInForum(forumId, userId) {
  check(forumId, String);
  check(userId, String);
  let forum = getForum(forumId).fetch()[0];

  return forum.forumAdminId === userId;
}

export function register(forumId, userId) {
  check(forumId, String);
  check(userId, String);

  updateUserNotificationSettingsInForum(forumId, userId, userNotificationSettingsInForum.NOTSET);

  return forumsMongoDB.update({'_id': forumId}, {
    $push: { users: userId }
    });
}

export function unregister(forumId, userId) {
  check(forumId, String);
  check(userId, String);

  updateUserNotificationSettingsInForum(forumId, userId, undefined);

  return forumsMongoDB.update({'_id': forumId},
    {$pull: {users: userId}});
}

export function getUserNotificationSettingsInForum(forumId, userId) {
  check(forumId, String);
  check(userId, String);

  let forum = forumsMongoDB.findOne({ _id: forumId });
  if (!forum) return userNotificationSettingsInForum.NOTSET;

  let userNotificationSettings = forum.userNotificationSettings;
  if (!userNotificationSettings) return userNotificationSettingsInForum.NOTSET;

  let userSettings = userNotificationSettings[userId];
  if (!userSettings) return userNotificationSettingsInForum.NOTSET;

  return userSettings;
}

export function updateUserNotificationSettingsInForum(forumId, userId, newSetting) {
  //This part verifies that the new value set is part of the enum forumNotiificationsSettings
  let isValidValue = validateObjectHasProperty(forumNotiificationsSettings, newSetting);

  if (!isValidValue) return;

  let forum = forumsMongoDB.findOne({ _id: forumId });
  if (!forum) return;

  var userNotificationSettings = forum.userNotificationSettings;
  if (!userNotificationSettings) {
    userNotificationSettings = {};
  }

  if (newSetting) {
    userNotificationSettings[userId] = newSetting;
  } else {
    delete userNotificationSettings[userId];
  }

  return forumsMongoDB.update({'_id': forumId}, {
    $set: {userNotificationSettings: userNotificationSettings}
  });
}

export function userInForum(forumId, userId) {
  var forum = forumsMongoDB.findOne({_id: forumId});
  if (!forum) {
    return false;
  }
  return forum.users.indexOf(userId) >= 0;
}

//returns all forums in the database
export function getAllForums() {
  return forumsMongoDB.find({});
}

//returns true if name is free, else false
export function isNameFree(name) {
  return (forumsMongoDB.find({"forumName": name}).count() == 0);
}

//returns true if name is free, else false
export function forumExists(forumId) {
  return (forumsMongoDB.find({"_id": forumId}).count() == 1);
}

//returns the number of forums overall in system
export function getNumOfForums() {
  return forumsMongoDB.find({}).count();
}

export function forumsTheUsersHasJoined(userId) {
  return forumsMongoDB.find({users: userId},
    {sort: {forumName: 1}});
}

export function publicForumListUserIsNotIn(userId) {
  var allForumsUserIsIn = forumsMongoDB.find({users: userId}).fetch();
  var allForumIdsUserIsIn = allForumsUserIsIn.map(function (object) {
    return object._id;
  });
  return forumsMongoDB.find({_id: {$nin: allForumIdsUserIsIn}},
    {sort: {forumName: 1}});
}

export function getForumNotificationSettingsForForumId(forumId) {
  var forum = forumsMongoDB.findOne({_id: forumId});
  if (!forum) return;

  return forum.settings.notificationSettings;
}

//Checks if an enum has a value equal to valueToCheck in one of the properties
function validateObjectHasProperty(object, valueToCheck) {
  var isValidValue = false;

  for (var property in object) {
    if (object.hasOwnProperty(property)) {
      if (valueToCheck === object[property]) {
        isValidValue = true;
      }
    }
  }
  return isValidValue;
}