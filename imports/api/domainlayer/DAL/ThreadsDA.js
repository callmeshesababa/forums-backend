//Import
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Email } from 'meteor/email';

//Databasee
export const threadsMongoDB = new Mongo.Collection('threads');

export function threadsForSubforumId(subforumId) {
  return threadsMongoDB.find({ subForumId : subforumId});
}

export function getThreadArrayById (threadId) {
  return threadsMongoDB.find({ _id : threadId });
}

//Returns forum object or null if unsuccessful
export function postThread(subForumIdToPostTo, postUserId, subject, text) {
  check(subForumIdToPostTo, String);
  check(postUserId, String);
  check(subject, String);
  check(text, String);

 	var newPost = 
        {
					subForumId: subForumIdToPostTo,  
					postingUserId: postUserId,
					text: text,
          subject: subject,
					comments: [],
					subscribers: [postUserId],
					createdAt: new Date()};
  var result = threadsMongoDB.insert(newPost);
  return result;
}

// Returns a WriteResult object that contains the status of the operation.
export function deletePost(postId) {
  post = threadsMongoDB.find( { '_id': postId });

  for (var user in post.subscribers){
    var reciepent = user.emails[0].address;
    Email.send({to:reciepent,from: 'forum@notifier.com',subject:'Deleted Post '+ postId, text: 'This post has been deleted'});
  }
  return threadsMongoDB.remove( { "_id" : postId } )
}

export function editThread(threadId, editingUserId, newSubject, newText) {
  check(threadId, String);
  check(editingUserId, String);
  check(newSubject, String);
  check(newText, String);

  return threadsMongoDB.update( { "_id" : threadId }, 
                                { $set: { subject: newSubject, text: newText }} );
}

// Returns a WriteResult object that contains the status of the operation.
export function addComment(postId, userCommenting, commentText) {
  check(commentText, String);

  var comment = {
    user: userCommenting, 
    text: commentText, 
    createdAt: new Date()
  };
  
  post = threadsMongoDB.find( { '_id': postId });
  // for (var user in post.subscribers){
  //   var reciepent = user.emails[0].address;
  //   Email.send({to:reciepent,from: 'forum@notifier.com',subject:'New Comment for Post '+ postId, text: commentText});
  // }
  // return threadsMongoDB.update({'_id':postId},{$push:[{'comments':comment}, {'subscribers':userCommenting}]});
  return threadsMongoDB.update({'_id':postId},{$push:{'comments':comment}});
}

// Returns a WriteResult object that contains the status of the operation.
export function deleteComment(postId,commentId) {
  check(commentId, String);

  post = threadsMongoDB.find( { '_id': postId });

  for (var user in post.subscribers){
    var reciepent = user.emails[0].address;
    Email.send({to:reciepent,from: 'forum@notifier.com',subject:'Deleted Comment for Post '+ postId, text: 'Comment '+ commentId + ' has been deleted'});
  }

  return threadsMongoDB.update({'_id':postId},{$pull:{'comments':{ $elemMatch: { cid: commentId } }}});
}


// Returns a WriteResult object that contains the status of the operation.
export function editCommment(postId, commentId, newText, userCommenting) {
  check(postId, String);
  check(newText, String);

  var n_id = GUID.guid();
  var comment = { cid: ncid, user: userCommenting, text: newText, createdAt: new Date()};

  threadsMongoDB.update({'_id':postId},{$pull:{'comments':{ $elemMatch: { cid: commentId } }}});
  threadsMongoDB.update({'_id':postId},{$push:[{'comments':comment}]});

  post = threadsMongoDB.find( { '_id': postId });

  for (var user in post.subscribers){
    var reciepent = user.emails[0].address;
    Email.send({to:reciepent,from: 'forum@notifier.com',subject:'Edited Comment for Post '+ postId, text: newText});
  }
  return comment;
}

//returns all threads in the database
export function getAllThreads() {
	return threadsMongoDB.find({});
}

export function getPost(postId) {
  var post = threadsMongoDB.find( { '_id': postId });
  return post;
}

export function getPostObject(postId) {
  return threadsMongoDB.findOne({ _id : postId});
}

export function userCanManipulatePost(postId, user) {
	 return (threadsMongoDB.find({ $and: [ { '_id': postId }, { 'user':  user }]}).count() == 1);
}

export function userCanManipulateComment(postId, commentID, user) {

  comment = threadsMongoDB.find({ $and: [ { '_id': postId }, { $elemMatch: { 'cid': commentId }}]})
  return (comment.user == user);
}

export function numOfPostsForUserInForum(user) {
   var posts =  threadsMongoDB.find({ 'user':  user })
   var comments = posts.comments.count();
   return posts.count() + comments;
}

export function numOfPostsForUserInSubForum(subForumId, user) {
   var posts =  threadsMongoDB.find({ $and: [ { 'subForumId': subForumId }, { 'user':  user }]});
   var comments = posts.comments.count();
   return posts.count() + comments;
}

//returns the all comments related to the given postId in a decrease order from.
export function getCommentsByPost(subforumId, postId){
  var post = threadsMongoDB.find({ '_id': postId});
  return post.comments;
}

export function getThreadSubjectForThreadId (threadId) {
  var thread = threadsMongoDB.findOne({ _id : threadId });
  return thread ? thread.subject : "";
}