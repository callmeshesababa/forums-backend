import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

var policyMongoDB = new Mongo.Collection('policy');	

export function getPolicy () {
	maintainDBState();
	return policyMongoDB.find({});
}

export function setPassExporationPeriod (period) {
	maintainDBState();
	var policy = policyMongoDB.findOne({});

	return policyMongoDB.update({ _id : policy._id },
								{ $set: { passExpirationPeriod: period }});
}

export function setAllowSecurityQuestion (isAllowed) {
	maintainDBState();
	var policy = policyMongoDB.findOne({});

	return policyMongoDB.update({ _id : policy._id },
								{ $set: { allowSecurityQuestion: isAllowed }});
}

export function maintainDBState() {
	var policy = policyMongoDB.findOne({});
	if (policy) return;

	var defaultPolicyObject = {
			passExpirationPeriod : 14,
			allowSecurityQuestion: false,
		};

	if (Meteor.isServer) {
		policyMongoDB.insert(defaultPolicyObject);
	}
}