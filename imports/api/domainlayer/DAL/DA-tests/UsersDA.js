//Import
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';


export function getAllUsers (forumId) {
  return Meteor.users.find({ });
}

export function getUsersWithName (seachQuery) {
	var regex = new RegExp(seachQuery, 'i');
	return Meteor.users.find({ $or : [ {username : regex},
										{"profile.name" : regex}]});
}

export function getUsersWithNameOutOfIdList (seachQuery, userIdList) {
	var regex = new RegExp(seachQuery, 'i');
	return Meteor.users.find({ $or : [ {username : regex},
										{"profile.name" : regex}],
								_id : { $in : userIdList }});
}

export function getUserWithId(userId) {
	return Meteor.users.find({ _id : userId });
}

export function getUserNameForUserId (userId) {
	var user = Meteor.users.findOne ( { _id : userId });

	if (user && user.username) {
		return user.username;
	} else if (user && user.profile && user.profile.name) {
		return user.profile.name;
	}
	return "";
}

export function isSuperAdminSet(){
	return Meteor.users.findOne({ superAdmin : true });
}

export function isSuperAdmin(userId) {
	return Meteor.users.findOne({ _id : userId, superAdmin : true });
}

export function setUserAsSuperAdmin(userId) {
	return Meteor.users.update({ _id : userId }, { $set : {superAdmin : true }});
}