import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const policyDA = require('./../PolicyDA.js');

if (Meteor.isServer) {
  describe('Unit Tests - Policy', () => {
      it('policy creation - success: getter returns a policy', () => {
          resetDatabase();
          var policy = policyDA.getPolicy().fetch()[0];
          assert.isNotNull(policy);
          resetDatabase();
      });
      
 	  it('policy initialization - success: policy initialized', () => {
          resetDatabase();
          var policy = policyDA.maintainDBState();
          assert.isNotNull(policy);
          resetDatabase();
      });
  });
}
          
