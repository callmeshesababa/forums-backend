import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const twoWayAuthDA = require('./../TwoWayAuthDA.js');

if (Meteor.isServer) {
  describe('Unit Tests - TwoWayAuth', () => {
      it('add connection - success: returns id', () => {
          resetDatabase();
          var connection = twoWayAuthDA.addConnection('connectionId', 'userId');
          assert.isNotNull(connection);
          resetDatabase();
      });
      
  });
}
          
