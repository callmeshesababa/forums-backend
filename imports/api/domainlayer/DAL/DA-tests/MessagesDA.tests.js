import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const messagesDA = require('./../MessagesDA.js');

if (Meteor.isServer) {
  describe('Unit Tests - Messages', () => {
      it('send message - success: num of messages', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var numOfMessages = messagesDA.getAllMessages().fetch().length;
          assert.equal(numOfMessages, 1);
          resetDatabase();
      });
      
      it('send message - success: message content', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var message = messagesDA.getAllMessages().fetch()[0];
          assert.equal(message.messageBody, 'text');
          resetDatabase();
      });
      
      it('read message - success: message content', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var messageSent = messagesDA.getAllMessages().fetch()[0];
          var messageRead = messagesDA.readMessages('receiverid').fetch()[0];
          assert.equal(messageSent.messageBody, messageRead.messageBody);
          resetDatabase();
      });
      
      it('read message - success: senderid', () => {
          resetDatabase();
          Meteor.call('messages.send', 'senderid', 'receiverid', 'text');
          var messageSent = messagesDA.getAllMessages().fetch()[0];
          var messageRead = messagesDA.readMessages('receiverid').fetch()[0];
          assert.equal(messageSent.sender, messageRead.sender);
          resetDatabase();
      });
      
      it('num of messages - success: forum initialization contains empty messages db', () => {
          resetDatabase();
          var messageNum = messagesDA.getAllMessages().fetch().length;
          assert.equal(messageNum, 0);
          resetDatabase();
      });
    });
}      
