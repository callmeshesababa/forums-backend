import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const forumDA = require('./../ForumDA.js');
const subforumsDA = require('./../SubforumsDA.js');

if (Meteor.isServer) {
  describe('Unit Tests - Subforums', () => {
      it('subforum creation - success: num of subforums', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          var numOfSubforums = subforumsDA.getSubforumsForForumId(forum._id).fetch().length;
          assert.equal(numOfSubforums, 1);
          resetDatabase();
      });
      
      it('subforum creation - success: subforum name', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          assert.equal(subforum.subForumName, 'Gals Subforum');
          resetDatabase();
      });
      
      it('subforum creation - success: subforum description', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          assert.equal(subforum.subForumDescription, 'this is gals subforum');
          resetDatabase();
      });

      it('subforum creation - success: subforum exists', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          assert.equal(subforumsDA.subForumExists(subforum._id), true);
          resetDatabase();
      });
      
      it('subforum creation - success: subforum does not exists after deletion', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          subforumsDA.deleteSubForum(subforum._id);
          assert.equal(subforumsDA.subForumExists(subforum._id), false);
          resetDatabase();
      });
      it('subforum deletion - success: subforum deleted', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          subforumsDA.deleteSubForum(subforum._id);
          var numOfSubforums = subforumsDA.getSubforumsForForumId(forum._id).fetch().length;
          assert.equal(numOfSubforums, 0);
          resetDatabase();
      });     

      it('subforum edit - success: subforum edited', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          subforumsDA.editSubForum(subforum._id, 'Elads Subforum', '');
          subforum = subforumsDA.getSubforumsForForumId(forum._id).fetch()[0];
          assert.equal(subforum.subForumName, 'Elads Subforum');
          resetDatabase();
      });   

      it('subforum manager - success: subforum creator is manager', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          var isManager = subforumsDA.isManagerInSubForum(subforum._id, 'userid1');
          assert.equal(isManager, true);
          resetDatabase();
      }); 

      it('subforum manager - success: new manager assigned', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          subforumsDA.addSubForumManager(subforum._id, 'userid2', null);
          var isManager = subforumsDA.isManagerInSubForum(subforum._id, 'userid2');
          assert.equal(isManager, true);
          resetDatabase();
      }); 

      it('subforum maintanance - success: manager removed by expiration date', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          var date = new Date() - 1;
          subforumsDA.addSubForumManager(subforum._id, 'userid2', date);
          subforumsDA.subForumArrayForSubforumId(subforum._id);
          var isManager = subforumsDA.isManagerInSubForum(subforum._id, 'userid2');
          assert.equal(isManager, false);
          resetDatabase();
      });

      it('subforum maintanance - success: manager isnt removed when future expiration date', () => {
          resetDatabase();
          var forum = createForum();
          var subforum = createSubforum(forum._id);
          var date = new Date() + 1;
          subforumsDA.addSubForumManager(subforum._id, 'userid2', date);
          subforumsDA.subForumArrayForSubforumId(subforum._id);
          var isManager = subforumsDA.isManagerInSubForum(subforum._id, 'userid2');
          assert.equal(isManager, false);
          resetDatabase();
      });  
    });
}

function createForum(){
    var name = 'Gals Forum';
    var desc = 'this is gals forum';
    var userId = 'userid1';
    forumDA.addForum(name, desc, userId);
    return forumDA.getAllForums().fetch()[0];
}

function createSubforum(forumId){
    var name = 'Gals Subforum';
    var desc = 'this is gals subforum';
    var userId = 'userid1';
    subforumsDA.addSubForum(forumId, name, desc, userId);
    return subforumsDA.getSubforumsForForumId(forumId).fetch()[0];
}
  