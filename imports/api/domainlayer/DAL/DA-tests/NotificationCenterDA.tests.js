import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const notificationCenterDA = require('./../NotificationCenterDA.js');

if (Meteor.isServer) {
  describe('Unit Tests - NotificationCenter', () => {
      it('notification creation - success: returns true', () => {
          resetDatabase();
          assert.equal(notificationCenterDA.createNotificationForUser('userId', 'notificationMessage', 'forumId', 'subforumId', 'threadId'), true);
          resetDatabase();
      });
      
      it('notification creation - success: get the notification from the db', () => {
          resetDatabase();
          notificationCenterDA.createNotificationForUser('userId', 'notificationMessage', 'forumId', 'subforumId', 'threadId');
          var notification = notificationCenterDA.getNotificationsForUser('userId', 1).fetch()[0];
          assert.equal(notification.isRead, false);
          resetDatabase();
      });

      it('notification read - success: notification marked as read', () => {
          resetDatabase();
          notificationCenterDA.createNotificationForUser('userId', 'notificationMessage', 'forumId', 'subforumId', 'threadId');
          var notification = notificationCenterDA.getNotificationsForUser('userId', 1).fetch()[0];
          notificationCenterDA.markNotificationIsRead(notification._id);
          assert.equal(notification.isRead, false);
          resetDatabase();
      });

  });
}
          
