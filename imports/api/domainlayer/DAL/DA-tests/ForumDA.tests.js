import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const forumDA = require('./../ForumDA.js');

if (Meteor.isServer) {
  describe('Unit Tests - Forums', () => {
      it('forum creation - success: forum exists', () => {
          resetDatabase();
          var forum = createForum();
          var forumExists = forumDA.forumExists(forum._id);
          assert.equal(forumExists, true);
          resetDatabase();
      });
      
      it('forum creation - success: num of forums', () => {
          resetDatabase();
          var forum = createForum();
          var numOfForums = forumDA.getNumOfForums();
          assert.equal(numOfForums, 1);
          resetDatabase();
      });
      
      it('forum creation - success: name taken', () => {
          resetDatabase();
          var forum = createForum();
          var isNameFree = forumDA.isNameFree(forum.forumName);
          assert.equal(isNameFree, false);
          resetDatabase();
      });
      
      it('forum creation - success: admin', () => {
          resetDatabase();
          var forum = createForum();
          var isAdmin = forumDA.isAdminInForum(forum._id, 'userid1');
          assert.equal(isAdmin, true);
          resetDatabase();
      });
      
      it('forum deletion - success: num of forums', () => {
          resetDatabase();
          var forum = createForum();
          forumDA.deleteForum(forum._id);
          var numOfForums = forumDA.getNumOfForums();
          assert.equal(numOfForums, 0);
          resetDatabase();
      });    

      it('forum deletion - success: unable to delete from an empty list', () => {
          resetDatabase();
          var forum = createForum();
          forumDA.deleteForum(forum._id);
          forumDA.deleteForum(forum._id);
          var numOfForums = forumDA.getNumOfForums();
          assert.equal(numOfForums, 0);
          resetDatabase();
      });    

      it('forum does not exists - success: no forum found', () => {
          resetDatabase();
          var forumName = forumDA.getForumNameForForumId(1);
          assert.equal(forumName, "");
          resetDatabase();
      });

      it('forum does exists - success: forum found', () => {
          resetDatabase();
          var forum = createForum();
          var forumName = forumDA.getForumNameForForumId(forum._id);
          assert.equal(forumName, "Gals Forum");
          resetDatabase();
      });

      it('forum edit - success: forum name changed', () => {
          resetDatabase();
          var forum = createForum();
          forumDA.editForum(forum._id, 'Elads Forum', '');
          var forumName = forumDA.getForumNameForForumId(forum._id);
          assert.equal(forumName, "Elads Forum");
          resetDatabase();
      });  

      it('forum admin - success: forum creator is the admin', () => {
          resetDatabase();
          var forum = createForum();
          var isAdmin = forumDA.isAdminInForum(forum._id, 'userid1');
          assert.equal(isAdmin, true);
          resetDatabase();
      });  

      it('forum registration - success: user registered to forum', () => {
          resetDatabase();
          var forum = createForum();
          forumDA.register(forum._id, 'userid2');
          var isInForum = forumDA.userInForum(forum._id, 'userid2');
          assert.equal(isInForum, true);
          resetDatabase();
      });  

      it('forum registration - success: user unregistered successfully', () => {
          resetDatabase();
          var forum = createForum();
          forumDA.register(forum._id, 'userid2');
          forumDA.unregister(forum._id, 'userid2');
          var isInForum = forumDA.userInForum(forum._id, 'userid2');
          assert.equal(isInForum, false);
          resetDatabase();
      });  
  });
}

function createForum(){
    var name = 'Gals Forum';
    var desc = 'this is gals forum';
    var userId = 'userid1';
    forumDA.addForum(name, desc, userId);
    return forumDA.getAllForums().fetch()[0];
}
          
