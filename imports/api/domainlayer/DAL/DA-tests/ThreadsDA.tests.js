import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { resetDatabase } from 'meteor/xolvio:cleaner';

const forumDA = require("./../ForumDA.js");
const subforumsDA = require("./../SubforumsDA.js");
const threadsDA = require("./../ThreadsDA.js");

if (Meteor.isServer) {
  describe('Unit Tests - Threads', () => {
      it('posting thread - success: number of threads in subforum', () => {
          resetDatabase();	
      	  var forum = createForum();
    	  var subForum = createSubForum(forum._id);
          threadsDA.postThread(subForum._id, 'userid1', 'test1', 'this is my test1');
          assert.equal(threadsDA.threadsForSubforumId(subForum._id).fetch().length, 1);
      });
 
      it('posting thread - success: according to post subject', () => {
          resetDatabase();	
      	  var forum = createForum();
    	  var subForum = createSubForum(forum._id);
          threadsDA.postThread(subForum._id, 'userid1', 'test1', 'this is my test1');
          var thread = threadsDA.threadsForSubforumId(subForum._id).fetch()[0];
          assert.equal(threadsDA.getThreadSubjectForThreadId(thread._id), 'test1');
      });
      
      it('post deletion - success: number of threads in subforum', () => {
          resetDatabase();	
      	  var forum = createForum();
    	  var subForum = createSubForum(forum._id);
          threadsDA.postThread(subForum._id, 'userid1', 'test1', 'this is my test1');
          var thread = threadsDA.threadsForSubforumId(subForum._id).fetch()[0];
          threadsDA.deletePost(thread._id);
          assert.equal(threadsDA.threadsForSubforumId(subForum._id).fetch().length, 0);
      });
      
      it('post deletion - success: getPost returns empty list', () => {
          resetDatabase();	
      	  var forum = createForum();
    	  var subForum = createSubForum(forum._id);
          threadsDA.postThread(subForum._id, 'userid1', 'test1', 'this is my test1');
          var thread = threadsDA.threadsForSubforumId(subForum._id).fetch()[0];
          threadsDA.deletePost(thread._id);
          assert.equal(threadsDA.getPost(thread._id).fetch().length, 0);
      });
  });
}

function createForum(){
    var name = 'Gals Forum';
    var desc = 'this is gals forum';
    var userId = 'userid1';
    forumDA.addForum(name, desc, userId);
    return forumDA.getAllForums().fetch()[0];
}

function createSubForum(forumId){
    var name = 'Gals Subforum';
    var desc = 'this is gals subforum';
    var userId = 'userid1';
    subforumsDA.addSubForum(forumId, name, desc, userId);
    return subforumsDA.getSubforumsForForumId(forumId).fetch()[0];
}
