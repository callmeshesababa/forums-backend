//Import
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

if (Meteor.isServer) {
	var twoWayAuthDB = new Mongo.Collection(null);	
	twoWayAuthDB._collection.name = "twoWayAuthDB";
} else {
	var twoWayAuthDB = new Mongo.Collection('twoWayAuthDB');	
}


export function addConnection (connectionId, userId) {
	check(connectionId, String);
	check(userId, String);

	var connectionForConnectionId = twoWayAuthDB.findOne({ connectionId : connectionId });
	if (connectionForConnectionId) { //Case - there is no need to create a new connetion object
		authorizeConnection(connectionId); //Try to authorize the connectionObject
		return;
	}

	if (!userId) return;

	var connectionForUser = twoWayAuthDB.findOne({ userId : userId });

	var connectionObject = {
		connectionId,
		userId,
		authorized : false,
		keyToAuthorize : null,
		authorizedClientShouldShowAuthorizationKey : false,
	};

	if (connectionForUser) { //Case - the user is already connected to another client
		connectionObject.authorized = false;
		connectionObject.keyToAuthorize = connectionForUser.keyToAuthorize;
	} else {
		connectionObject.authorized = true;
		connectionObject.keyToAuthorize = makeAuthorizationKey(4);
	}

	return twoWayAuthDB.insert(connectionObject);
}

//authorizationKey is optional
export function authorizeConnection(connectionId, authorizationKey) {
	check(connectionId, String);

	var connectionObject = twoWayAuthDB.findOne({ connectionId : connectionId });
	if (!connectionObject) {
		console.log("There is a problem in the server in authorizeConnection.");
		console.log("This shouldn't happen. Code 105");
		return;
	}

	//There is at least one connection for the user
	var userId = connectionObject.userId;

	//Check if the user has only one connection
	var numberOfConnectionsForUser = twoWayAuthDB.find({ userId : userId }).fetch().length;
	if (numberOfConnectionsForUser == 1) {
		twoWayAuthDB.update({ userId: userId },
							{ $set : { 	authorized : true, 
										authorizedClientShouldShowAuthorizationKey : false }},
							{ multi : true });
		return true;
	}

	//Check if authorizationKey matches and authorize if so
	if (connectionObject.keyToAuthorize === authorizationKey) {
		twoWayAuthDB.update({ connectionId : connectionId },
							{ $set : { authorized : true }});

		twoWayAuthDB.update({ userId : userId },
							{ $set : { authorizedClientShouldShowAuthorizationKey : false }},
							{ multi : true });
		return true;
	}

	//Keys did not match
	return false;
}

export function removeConnection (connectionId) {
	check(connectionId, String);

	var connectionObject = twoWayAuthDB.findOne({ connectionId : connectionId });
	//No such connection. can return
	if (!connectionObject) {
		return true;
	}

	var userId = connectionObject.userId;

	twoWayAuthDB.remove({ _id : connectionObject._id });

	//Check if the user has only one connection
	var numberOfConnectionsForUser = twoWayAuthDB.find({ userId : userId }).fetch().length;
	if (numberOfConnectionsForUser == 1) {
		twoWayAuthDB.update({ userId: userId },
							{ $set : { 	authorized : true,
										authorizedClientShouldShowAuthorizationKey : false }});
	}

	return true;
}

export function isCurrentSessionAuthorized(connectionId) {
	check(connectionId, String);

	var connectionObject = twoWayAuthDB.findOne({ connectionId : connectionId });

	//if there is no connection, the connectionId is not authorized
	if (!connectionObject) {
		return false;
	}

	return connectionObject.authorized;
}

export function connectionsForConnectionId (connectionId) {
	return twoWayAuthDB.find({ connectionId : connectionId });
}

export function getAuthorizationKeyForConnectionId (connectionId) {
	check(connectionId, String);

	var connectionObject = twoWayAuthDB.findOne({ connectionId : connectionId });

	return connectionObject && connectionObject.keyToAuthorize;
}

//Should be called every time the server is initialized
export function initialize () {
	twoWayAuthDB.remove({});
}

function makeAuthorizationKey(keyLength) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < keyLength; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

export function clientShouldShowConnectionCode() {
	var connectionObject = twoWayAuthDB.findOne({});
	if (!connectionObject) return false;

	if (connectionObject.authorizedClientShouldShowAuthorizationKey && connectionObject.authorized) {
		return connectionObject.keyToAuthorize;
	}
	return false;
}

export function requestShowConnectionCodeInAuthorizedClients(userId) {
	twoWayAuthDB.update({ userId : userId, authorized : true},
						{ $set : { authorizedClientShouldShowAuthorizationKey : true }},
						{ multi : true });
}

export function stopShowingConnectionCodeInAuthorizedClient(connectionId) {
	twoWayAuthDB.update({ connectionId : connectionId }, 
						{ $set : { authorizedClientShouldShowAuthorizationKey : false }});
}

export function clientSideCurrentConnectionIsAuthorized () {
	var connectionObject = twoWayAuthDB.findOne({});
	if (!connectionObject) return true;
	return connectionObject.authorized;
}

export function clientSideCurrentConnectionIsAuthorizedDefaultIsNo () {
	var connectionObject = twoWayAuthDB.findOne({});
	if (!connectionObject) return false;
	return connectionObject.authorized;
}