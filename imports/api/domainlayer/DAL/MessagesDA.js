//Import
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

//Databasee
export const messagesMongoDB = new Mongo.Collection('messages');

//Returns forum object or null if unsuccessful
export function sendMessage(sender, reciever, text) {
	check(sender, String);
	check(reciever, String);
	check(text, String);

	var message =  {
					sender: sender,
					reciever: reciever,
					messageBody: text,
					createdAt: new Date(),
					unread: true
				};
  	message = messagesMongoDB.insert(message);
	return message;
}

//Returns forum object or null if unsuccessful
export function readMessages(userId) {
	var newMessages = messagesMongoDB.find({'reciever':userId },{sort: {createdAt: -1}});
	// for (message in newMessages){
	// 	messagesMongoDB.update({'_id': message._id },{$set:{'unread':false}});
	// }
  	return newMessages;
}

//returns all forums in the database
export function getAllMessages() {
	return messagesMongoDB.find({});
}