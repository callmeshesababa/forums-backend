import { Meteor } from 'meteor/meteor';
const usersBL = require("./domainlayer/BL/Users/usersBL");
const PolicyBL = require("./domainlayer/BL/Policy/PolicyBL");

import { logFail, logSuccess } from './domainlayer/BL/logger';

//IMPORTANT The commented lines are for future reference. please do not delete

let _MS_PER_DAY = 1000 * 60 * 60 * 24;

if (Meteor.isServer) {
	Meteor.publish("userData", userData);
	Meteor.publish("getAllUsers", getAllUsers);
	Meteor.publish("superAdminUsers", function() {
		return Meteor.users.find({ superAdmin : true });
	});

}

Meteor.methods({
  	createUserWithoutLogin(user) {
	  	if (Meteor.isServer) {
				try {
					var newUserId = Accounts.createUser(user);
					logSuccess("User creation success. New user ID: " + newUserId + ". sending email");
					Meteor.users.update({ _id : newUserId }, {$set : { securityQuestions : user.securityQuestions}});
					return newUserId;
				} catch (error) {
					logFail("User creations failed because of: " + error.reason);
					throw error;
				}
	  	}
  	}, 
  sendVerificationLink(userId, email) {
  	if (Meteor.isServer) {
			if ( userId ) {
				return Accounts.sendEnrollmentEmail( userId, email );
			}
  	}
  },
  userForUserName(username) {
  	if (Meteor.isServer) {
		if ( username ) {
		  return Meteor.users.findOne({ username : username });
		}
  	}
  },
  isSuperAdminSet() {
	if (Meteor.isServer) {
		return isSuperAdminSet();	
	}
	return false;
	},
	getSecurityQuestionsForEmail(email) {
		if (Meteor.isServer) {
			var user = Accounts.findUserByEmail(email);

			if (user.superAdmin) {
				return "User is super admin";
			}

			return user && user.securityQuestions;
		}
	},
	sendEmailResetWithEmailAndSecurityAnswers(email, answer1, answer2) {
		if (Meteor.isServer) {
			var user = Accounts.findUserByEmail(email);
			if (!user) return false;

			if (user.securityQuestions && user.securityQuestions.answer1) {
				if (user.securityQuestions.answer1 !== answer1) {
					logFail("Reset password failed. User: " + email + ". entered a wrong security question");
					return "wrongAnswer";
				}
			}

			if (user.securityQuestions && user.securityQuestions.answer2) {
				if (user.securityQuestions.answer2 !== answer2) {
					logFail("Reset password failed. User: " + email + ". entered a wrong security question");
					return "wrongAnswer";
				}
			}

			var userId = user._id;
			Accounts.sendResetPasswordEmail(userId);
			logSuccess("Sending password reset email to " + email);
			return true;
		}
	},
	sendEmailResetWithEmail(email) {
		if (Meteor.isServer) {
			var user = Accounts.findUserByEmail(email);
			if (!user) return false;
			var userId = user._id;
			Accounts.sendResetPasswordEmail(userId);
			return true;
		}
	},
	setFirstUserAsSuperAdmin() {
		setUserAsSuperAdmin(this.userId);
	},
	checkCurrentUserPasswordIsValid() {
		if (Meteor.isServer) {
			var user = Meteor.user();

			//Case - user is not logged in or doesnt have a set date
			if (!user || !user.password_date) return true;

			var policyArray = PolicyBL.getPolicy().fetch();
			
			//Case - there is no policy set
			if (policyArray.length < 1) return true;

			var policy = policyArray[0];
			var passExpirationPeriod = policy.passExpirationPeriod;

			var now = new Date();

			var daysSincePasswordWasSet = Math.floor((now - user.password_date) / _MS_PER_DAY);

			return daysSincePasswordWasSet < passExpirationPeriod;
		}
		return true;
	},
	updatePasswordSetDateToNow() {
		if (Meteor.isServer) {
			var userId = this.userId;

			Meteor.users.update({ _id : userId},
								{ $set : { password_date : new Date() }});
			return true;
		}
	}
});

function userData() {
	if (this.userId) {
		return Meteor.users.find({_id: this.userId});
	} else {
		this.ready();
	}
}

function getAllUsers() {
	return usersBL.getAllUsers()
}

export function getUsersWithName (seachQuery) {
	return usersBL.getUsersWithName(seachQuery);
}

export function getUsersWithNameOutOfIdList (seachQuery, userIdList) {
	return usersBL.getUsersWithNameOutOfIdList(seachQuery, userIdList);
}

export function getUserWithId(userId) {
	return usersBL.getUserWithId(userId);
}

export function isSuperAdminSet(){
	return usersBL.isSuperAdminSet();
}

export function isSuperAdmin(userId) {
	return usersBL.isSuperAdmin(userId);
}

export function setUserAsSuperAdmin(userId) {
	return usersBL.setUserAsSuperAdmin(userId);
}