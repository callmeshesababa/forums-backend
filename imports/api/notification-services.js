import { Meteor } from 'meteor/meteor';
const notificationCenterBL = require("./domainlayer/BL/NotificationCenter/NotificationCenterBL");

if (Meteor.isServer) {
	Meteor.publish('unreadNotificationsForUserWithCount', getUnreadNotificationsForUser);
	Meteor.publish('unreadNotificationsForUser', function (userId) {
		return getUnreadNotificationsForUser(userId, 10); //Default notification count is 10
	});
}

Meteor.methods({
	'markNotificationIsRead' (notificationId) {
		notificationCenterBL.markNotificationIsRead(notificationId);
	},
});

export function getUnreadNotificationsForUser(userId, count) {
	return notificationCenterBL.getUnreadNotificationsForUser(userId, count);
}