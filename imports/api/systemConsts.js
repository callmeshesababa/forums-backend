export let allowUserBeModeratorInMultiplePolicies = {
  ALLOWED: "allowed",
  NOTALLOWED: "notallowed"
}

export let allowModeratorDeleteThreads = {
  ALLOWED: "allowed",
  NOTALLOWED: "notallowed"
}

export let forumNotiificationsSettings = {
  ONLINE: "online",
  OFFLINE: "offline",
  SELECTIVE: "selective"
};

export let userNotificationSettingsInForum = {
  ONLINE: "online",
  OFFLINE: "offline",
  NOTSET: "notset"
}

export let privilegesErrorName = "privileges";

export let allowStressTest = true;