import { Meteor } from 'meteor/meteor';
const threadHandler = require("./domainlayer/BL/Threads/threadsBL");

if (Meteor.isServer) {
  Meteor.publish(
    'threadsBySubForumId', function(subforumId) {
    var userId = this.userId;

    return threadHandler.threadsForSubforumId(subforumId, userId);
  });

  Meteor.publish('threadById', function(threadId) {
    var userId = this.userId;
    return threadHandler.getThreadArrayById(threadId, userId);
  });

  Meteor.publish('getPost', getPost);
};

Meteor.methods({
  'threads.postThread'(subForumIdToPostTo, postUserId, subject, text) {
    return threadHandler.postThread(subForumIdToPostTo, postUserId, subject, text);
  },
  'threads.deletePost'(subForumId, postId) {
    return threadHandler.deletePost(subForumId, postId, Meteor.user());
  },
  'threads.editThread'(threadId, editingUserId, newSubject, newText) {
    return threadHandler.editThread(threadId, editingUserId, newSubject, newText);
  },
  'threads.comment'(postId, text) {
    threadHandler.comment(postId, Meteor.user(), text);
  },
  'threads.deleteComment'(subforumId, postId, commentId) {
    threadHandler.deleteComment(subforumId, postId, commentId, Meteor.user());
  },
  'threads.editComment'(subforumId, postId, commentId, newText) {
    threadHandler.editComment(subforumId, postId, commentId, newText, Meteor.user());
  },
  'threads.num-of-posts-for-user-in-forum'(user, forumId) {
    threadHandler.numOfPostsForUserInForum(Meteor.user(), forumId, user);
  },
  'threads.num-of-posts-for-user-in-subforum'(user, subforumId) {
    threadHandler.numOfPostsForUserInSubForum(Meteor.user(), subforumId, user);
  },
});

export function threadsForSubforumIdWithUserId (subforumId, userId) {
    return threadHandler.threadsForSubforumId(subforumId, userId);
}

export function getCommentsByPost(subforumId, postId){
  return threadHandler.getCommentsByPost(subforumId, postId);
}

export function getThreadArrayById(threadId, userId) {
  return threadHandler.getThreadArrayById(threadId, userId);
}

export function getPost(postId) {
  return threadHandler.getPost(postId);
}
