import { Meteor } from 'meteor/meteor';
const forumHandler = require("./domainlayer/BL/Forum/forumBL");
const usersBL = require("./domainlayer/BL/Users/usersBL");

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('allForums', getAllForums);
  Meteor.publish('getForum', getForum);
}

Meteor.methods({
  'forums.insert'(name, description, userId) {
    return forumHandler.create(name, description, userId);
  },
  'forums.delete'(forumId, userId) {
	 forumHandler.deleteForum(forumId, userId);
  },
  'forums.edit'(forumId, newName, description, userId) {
	 forumHandler.editForum(forumId, newName, description, userId);
  },
  'forums.join'(forumId, userId) {
   forumHandler.register(forumId, userId);
  },
  'forums.leave'(forumId, userId) {
   forumHandler.unregister(forumId, userId);
  },
  'forums.user-can-delete-forum'(forumId, userId) {
    console.log("some");
    console.log(forumHandler.userCanDeleteForum(forumId, userId));

   return forumHandler.userCanDeleteForum(forumId, userId);
  },
  'forums.setForumNotificationSettings'(forumId, newSetting) {
    let userId = Meteor.userId();
    return forumHandler.setForumNotificationSettings(forumId, newSetting, userId);
  },
  'forums.setUserNotificationSettingsInForum'(forumId, newSetting) {
    let userId = Meteor.userId();
    return forumHandler.updateUserNotificationSettingsInForum(forumId, userId, newSetting, userId);
  },
  'forums.setForumAllowUserBeModeratorInMultipleSubforums'(forumId, newSetting) {
    let userId = Meteor.userId();
    return forumHandler.setForumAllowUserBeModeratorInMultipleSubforums(forumId, newSetting, userId);
  },
  'forums.setForumAllowModeratorDeleteThreads'(forumId, newSetting) {
    let userId = Meteor.userId();
    return forumHandler.setForumAllowModeratorDeleteThreads(forumId, newSetting, userId);
  },
});

//Private
export function getAllForums () {
	return forumHandler.getAllForums();
}

export function getForum(forumId) {
  return forumHandler.getForum(forumId);
}

export function forumsTheUsersHasJoined (userId) {
  return forumHandler.forumsTheUsersHasJoined(userId);
}

export function publicForumListUserIsNotIn (userId) {
  return forumHandler.publicForumListUserIsNotIn (userId);
}

export function getUserNotificationSettingsInForum(forumId, userId) {
  return forumHandler.getUserNotificationSettingsInForum(forumId, userId);
}

export function getForumAllowUserBeModeratorInMultipleSubforums(forumId) {
  return forumHandler.getForumAllowUserBeModeratorInMultipleSubforums(forumId);
}

export function getForumAllowModeratorDeleteThreads(forumId) {
  return forumHandler.getForumAllowModeratorDeleteThreads(forumId);
}