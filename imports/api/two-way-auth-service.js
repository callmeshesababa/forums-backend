import { Meteor } from 'meteor/meteor';
const twoWayAuthHandler = require("./domainlayer/BL/Authentication/two-way-auth-handler");

if (Meteor.isServer) {
	Meteor.publish("connectionStatus", function () {
		var connectionId = this._session.id;
		return twoWayAuthHandler.connectionsForConnectionId(connectionId);
	});
}

Meteor.methods({
	isCurrentSessionAuthorized() {		
		var connectionId = this.connection && this.connection.id;
		if (!connectionId) return false;
		return twoWayAuthHandler.isCurrentSessionAuthorized(connectionId);
	},
	authorizeCodeForCurrentConnection(authorizationCode) {
		var connectionId = this.connection && this.connection.id;
		if (!connectionId) return false;
		return twoWayAuthHandler.authorizeConnection(connectionId, authorizationCode);
	}, 
	requestShowConnectionCodeInAuthorizedClients() {
		if (Meteor.isServer) {
			var userId = this.userId;
			twoWayAuthHandler.requestShowConnectionCodeInAuthorizedClients(userId);
			return true;
		}
		return true;
	},
	stopShowingConnectionCodeInAuthorizedClient() {
		if (Meteor.isServer) {
			var connectionId = this.connection && this.connection.id;
			twoWayAuthHandler.stopShowingConnectionCodeInAuthorizedClient(connectionId);
			return true;
		}
		return true;
	},
});

//Should be called from the clien side only
//Will return false only if the connetionObject has beed downloaded and it authorized was false
export function clientSideCurrentConnectionIsAuthorized () {
	return twoWayAuthHandler.clientSideCurrentConnectionIsAuthorized();
}

//Should be called from the clien side only
//Will return true only if the connetionObject has beed downloaded and it authorized was true
export function clientSideCurrentConnectionIsAuthorizedDefaultIsNo () {
	return twoWayAuthHandler.clientSideCurrentConnectionIsAuthorizedDefaultIsNo();
}

//Should be called from the clien side only
//Allows the client to know if it should show the code it has
//Will return the code for true
export function clientShouldShowConnectionCode() {
	return twoWayAuthHandler.clientShouldShowConnectionCode();
}