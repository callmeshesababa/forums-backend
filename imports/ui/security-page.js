import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const policyServices = require('../api/policy-service.js');

import './security-page.html';
import './views/admin-forum-report-cell.js';

Template.securitypage.onCreated(function bodyOnCreated() {
  Meteor.subscribe('getPolicy');
});

Template.securitypage.events({
	'submit' (event, template) {
		event.preventDefault();

		var target = event.target;

		var allowedPeriod = target.period.value;
		var allowSecurutyQuestions = target.allowSec.checked;

		Meteor.call('policy.setAllowSecurityQuestionAndPassExporationDate', allowSecurutyQuestions, allowedPeriod, function(error, response) {
			if (error) {
				toastr.error("There was an error changing the settings");
				return;
			}

			toastr.success("Settings changed");
		});
	},
});

Template.securitypage.helpers({
	policy() {
		var policyObjects = policyServices.getPolicy().fetch();
		
		if (policyObjects.length) {
			var policyObject = policyObjects[0];
			return policyObject;
		}
	},
});
