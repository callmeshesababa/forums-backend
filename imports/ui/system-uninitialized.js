import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Accounts } from 'meteor/accounts-base'
import { ReactiveDict } from 'meteor/reactive-dict';

import './system-uninitialized.html';

Template.systemUninitialized.onCreated(function bodyOnCreated() {
	this.state = new ReactiveDict();
  	this.state.set('showSpinner', false);
});

Template.systemUninitialized.helpers({
	shouldShowSpinner() {
		const instance = Template.instance();
		return instance.state.get('showSpinner');
	}
});

Template.systemUninitialized.events({
	'submit form'(event, template) {
		event.preventDefault();

		var target = event.target;

		var username = target.username.value;
		var password = target.password.value;
		var email = target.email.value;
		
		if (!username.length || !password.length || !email.length) {
			toastr.error("You must enter a username, password and an email");
			return;
		}

		var userObject = {username, password, email};

		template.state.set('showSpinner', true);

		Accounts.createUser(userObject, function(error) {
			if (error) {
				template.state.set('showSpinner', false);

				toastr.error("There was an error creating your account");
				return;
			}

			Meteor.call('setFirstUserAsSuperAdmin', function(error, response) {
				toastr.success("System successfully initialized");

				template.state.set('showSpinner', false);

				var routeName = "forumlist";
				FlowRouter.go(routeName);
			});
		});
	},
})