import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './forumPersonalSettings.html';

import { getUserNotificationSettingsInForum } from '../api/forum-services';

import { userNotificationSettingsInForum } from '../api/systemConsts';

function selectedStringIfForumSettingIsEqualToSetting(setting) {
  let forumId = FlowRouter.getParam('forumId');

  let userSetting = getUserNotificationSettingsInForum(forumId, Meteor.userId());

  if (setting === userSetting) return "selected";

  //Default setting is offline
  if (userSetting === userNotificationSettingsInForum.NOTSET && setting === "offlineNotifications") return "selected";

  return "";
}

Template.forumPersonalSettings.onCreated(function bodyOnCreated() {
  var forumId = FlowRouter.getParam("forumId");
  Meteor.subscribe('getForum', forumId);
});

Template.forumPersonalSettings.helpers({
  "onlineIsSelected"() {
    return selectedStringIfForumSettingIsEqualToSetting(userNotificationSettingsInForum.ONLINE);
  },
  "offlineIsSelected"() {
    return selectedStringIfForumSettingIsEqualToSetting(userNotificationSettingsInForum.OFFLINE);
  }
});

Template.forumPersonalSettings.events({
  'submit' (event, template) {
    event.preventDefault();

    let target = event.target;

    let selectedIndex = target.forumNotificationSettings.selectedIndex;
    let options = target.forumNotificationSettings.options;
    let selectedOptionId = options[selectedIndex].id;

    var selectedOptionEnum;
    switch (selectedOptionId) {
      case "onlineNotifications":
        selectedOptionEnum = userNotificationSettingsInForum.ONLINE;
        break;
      case "offlineNotifications":
        selectedOptionEnum = userNotificationSettingsInForum.OFFLINE;
        break;
    }

    if (!selectedOptionEnum) return;

    let forumId = FlowRouter.getParam('forumId');

    Meteor.call("forums.setUserNotificationSettingsInForum", forumId, selectedOptionEnum, function (error) {
      if (error) {
        toastr.error("There was an error while changing the settings");
      } else {
        toastr.success("Settings chagned!");
      }
    });
  },
});