import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const forumServices = require('../api/forum-services.js');

import './admin-page.html';
import './views/admin-forum-report-cell.js';

Template.adminpage.onCreated(function bodyOnCreated() {
  Meteor.subscribe('allForums');
});

Template.adminpage.helpers({
  forumList() {
  	return forumServices.getAllForums();
  },
});
