import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import './stress-test.html';

let numberOfForums = 10;
let numberOfSubForumsInForum = 10;
let numberOfThreadsInSubforum = 10;

Template.stresstest.onCreated(function bodyOnCreated() {
  this.state = new ReactiveDict();
  this.state.set('isRunning', false);
  this.state.set('isFinished', false);
});

Template.stresstest.events({
  'click'(event, template) {
    // Prevent default browser form submit
    event.preventDefault();

    if (template.state.get('isRunning')) return;

    template.state.set('isRunning', true);

    var completionCounterObj = {completed: 0};

    let createThreadInSubforumFunction = createThreadInSubforumIdCPS(numberOfThreadsInSubforum, function(response) {
      completionCounterObj.completed++;

      if (completionCounterObj.completed === numberOfForums * numberOfSubForumsInForum *numberOfThreadsInSubforum) {
        template.state.set('isFinished', true);
      }
    });

    let createSubforumFunction = createSubforumInForumIdCPS(numberOfSubForumsInForum, createThreadInSubforumFunction);

    for( var i=0; i < numberOfForums; i++ ) {
      createSingleForum(createSubforumFunction);
    }
  },
});
 
Template.stresstest.helpers({
  'buttonText'() {
    const instance = Template.instance();

    if (instance.state.get('isFinished')) return "Finished";
    if (instance.state.get('isRunning')) return "Running";

    return "Run stress test";
  },
  'buttonClass'() {
    const instance = Template.instance();

    if (!instance.state.get('isRunning')) return "btn-danger";

    return "btn-info";
  },
  'finishedMessage'() {
    const instance = Template.instance();

    if (!instance.state.get('isFinished')) return;

    let numberOfSubforums = numberOfSubForumsInForum * numberOfForums;
    let numberOfThreads = numberOfForums * numberOfSubForumsInForum * numberOfThreadsInSubforum;

    return "Finished creating " + numberOfForums + " forums, " + numberOfSubforums + " subforums and " + numberOfThreads + " threads.";
  },
  'finishedMessageSecondLine'() {
    const instance = Template.instance();

    if (!instance.state.get('isFinished')) return;

    let numberOfSubforums = numberOfSubForumsInForum * numberOfForums;
    let numberOfThreads = numberOfForums * numberOfSubForumsInForum * numberOfThreadsInSubforum;
    let totalNumberOfEntitiesCreated = numberOfForums + numberOfSubforums + numberOfThreads;

    return "Created a total of " + totalNumberOfEntitiesCreated + " entities.";
  },
});

function randomStringWithSpaces(wordLength, numberOfWords) {
  var text = "";

  for( var i=0; i < numberOfWords; i++ ) {
    text += randomString(wordLength);
    text += " ";
  }

  return text;
}

function randomString(length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for( var i=0; i < length; i++ ) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

function createSingleForum(callback) {
  let forumName = randomString(8);
  let forumDescription = randomStringWithSpaces(5, 15);
  let userId = Meteor.userId();

  Meteor.call('forums.insert', forumName, forumDescription, userId, function(error, response) {
    if (error) {
      console.log("Error in creating forum");
      console.log(error);
    } else {
      callback(response);
    }
  });
}

function createSubforumInForumIdCPS(numberOfSubforums, callback) {
  let createSingleSubforum = function(forumId) {
    let subforumName = randomString(8);
    let subforumDescription = randomStringWithSpaces(5, 15);
    let userId = Meteor.userId();

    Meteor.call('subforums.insert', forumId, subforumName, subforumDescription, userId, function(error, response) {
      if (error) {
        console.log("Error in creating subforum");
        console.log(error);
      } else {
        callback(response);
      }
    });
  };

  return function(forumId) {
    for( var i=0; i < numberOfSubforums; i++ ) {
      createSingleSubforum(forumId);
    }
  }
}

function createThreadInSubforumIdCPS(numberOfThreads, callback) {
  let createSingleThread = function(subforumId) {
    let threadSubject = randomString(12);
    let threadText = randomStringWithSpaces(100, 7);
    let userId = Meteor.userId();
    Meteor.call('threads.postThread', subforumId, userId, threadSubject, threadText, function(error, response) {
      if (error) {
        console.log("Error in creating thread");
        console.log(error);
      } else {
        callback(response);
      }
    });
  };

  return function(subforumId) {
    for( var i=0; i < numberOfThreads; i++ ) {
      createSingleThread(subforumId);
    }
  }
}