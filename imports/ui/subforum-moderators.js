import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

const subforumServices = require('../api/subforum-service.js');
const forumServices = require('../api/forum-services.js');
const users = require('../api/user-services.js')

import './subforum-moderators.html';
import './views/subforum-moderator-cell.js';

Template.subforummoderators.onCreated(function bodyOnCreated() {
  var subForumId = FlowRouter.getParam("subForumId");
  var forumId = FlowRouter.getParam("forumId");
  Meteor.subscribe('subForumArrayForSubforumId', subForumId);

  Meteor.subscribe('getAllUsers');

  Meteor.subscribe('getForum', forumId);

  this.state = new ReactiveDict();
  this.state.set('newModeratorUserName', null);
  this.state.set('didSelectUser', null);
  this.state.set('showExperationDateChooser', false);
});

Template.subforummoderators.helpers({
  autoCompletionUsers() {
    const instance = Template.instance();
    var newModeratorUserName = instance.state.get('newModeratorUserName');

    var forumId = FlowRouter.getParam("forumId");
    var forum = forumServices.getForum(forumId).fetch()[0];
    var forumUserIdsArray = forum.users;

    return users.getUsersWithNameOutOfIdList(newModeratorUserName, forumUserIdsArray);
  },
  didSelectAutoCompletionUser() {
    const instance = Template.instance();
    var userSelected = instance.state.get('didSelectUser');
    return userSelected;
  },
  SelectedAutoCompletionUsername() {
    const instance = Template.instance();
    var userSelected = instance.state.get('didSelectUser');
    if (userSelected.username) {
      return userSelected.username;
    }
    if (userSelected.profile) {
      return userSelected.profile.name;
    }
    return "";
  },
  shouldShowCompletion() {
    const instance = Template.instance();
    var currentRecepientUserName = instance.state.get('newModeratorUserName');

    if (currentRecepientUserName && currentRecepientUserName.length) {
      return true;
    }
    return false;
    // var users.getUsersWithName
  },
  shouldShowDateChooser() {
    const instance = Template.instance();
    var showExperationDateChooser = instance.state.get('showExperationDateChooser');
    return showExperationDateChooser;
  },
});

Template.subforummoderators.events({
  'input .newModeratorUserName'(event, instance) {
    var newModeratorUserName = instance.find('.newModeratorUserName').value;
    instance.state.set('newModeratorUserName', newModeratorUserName);
  },
  'submit .addModerator'(event, instance) {
    event.preventDefault();

    const target = event.target;
    const moderatorUsername = target.newModeratorUserName.value;

    var expeditionDateChooserDateObject;
    if (target.datePicker) {
      var expeditionDateChooserDateString = target.datePicker.value;
      var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      expeditionDateChooserDateString = expeditionDateChooserDateString.replace(pattern,'$3-$2-$1');
      expeditionDateChooserDateObject = new Date(expeditionDateChooserDateString);
    }

    var newModerator = Meteor.users.findOne( { $or : [ { username : moderatorUsername },
														{ "profile.name" : moderatorUsername } ]});
    if (!newModerator) {
    	//TODO: write error message
    	target.newModeratorUserName.value = '';
  		instance.state.set('newModeratorUserName', null);
  		instance.state.set('didSelectUser', null);

      toastr.error("That is not a valid username");

      return;
    }
    var newModeratorId = newModerator._id;
    var userId = Meteor.userId();
  	var subForumId = FlowRouter.getParam("subForumId");

  	Meteor.call('subforums.addManager', subForumId, newModeratorId, userId, expeditionDateChooserDateObject, function (error, response) {
      if (error) {
        toastr.error(error.reason, "Failed adding a moderator to forum");
      } else if (response) {
        toastr.info("Added a moderator to the sub forum");
      }
    });

  	target.newModeratorUserName.value = '';
  	instance.state.set('newModeratorUserName', null);
  	instance.state.set('didSelectUser', null);
  },
  'change .showDateChooser input'(event, instance) {
    instance.state.set('showExperationDateChooser', event.target.checked);
  },
});

//Auto complete
Template.subforummoderatorsAutoCompletion.helpers({
  'userNameForUse'() {
    if (this.username) {
      return this.username;
    }
    if (this.profile) {
      return this.profile.name;
    }
    return "";
  }
});

Template.subforummoderatorsAutoCompletion.events({
  'click'(event, template) {
      var formState = template.view.parentView.parentView.parentView.parentView._templateInstance.state;
      formState.set('didSelectUser', null);
      formState.set('didSelectUser', this);
      formState.set('newModeratorUserName', null);
  }
});

//Wrapper
Template.subforummoderatorswrap.onCreated(function bodyOnCreated() {
  var subForumId = FlowRouter.getParam("subForumId");
  Meteor.subscribe('subForumArrayForSubforumId', subForumId);

  var forumId = FlowRouter.getParam("forumId");
  Meteor.subscribe('getForum', forumId);
});

Template.subforummoderatorswrap.helpers({
  'subforumModeratorsArray'(){
  	 var subForumId = FlowRouter.getParam("subForumId");
    return subforumServices.getSubforumsArrayForSubforumId(subForumId);
  }, 
  'userIsAdminInForum'() {
  	var userId = Meteor.userId();
  	var forumId = FlowRouter.getParam("forumId");
  	var forumArray = forumServices.getForum(forumId).fetch();
  	if (!forumArray || forumArray.length == 0) {
  		return false;
  	}
  	var forum = forumArray[0];
  	return forum.forumAdminId === userId;
  },
});