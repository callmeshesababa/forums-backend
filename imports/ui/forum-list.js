import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const forumServices = require('../api/forum-services.js');

import './forum-list.html';
import './views/forum-cell.js';

Template.forumlist.onCreated(function bodyOnCreated() {
  Meteor.subscribe('allForums');
  Meteor.subscribe("userData");
});

Template.forumlist.helpers({
  myForumList() {
  	var userId = Meteor.userId();
  	return forumServices.forumsTheUsersHasJoined(userId);
  },
  publicForumListUserIsNotIn() {
  	var userId = Meteor.userId();
  	return forumServices.publicForumListUserIsNotIn(userId);
  }
});
