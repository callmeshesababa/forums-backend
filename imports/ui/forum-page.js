import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

const forumServices = require('../api/forum-services.js');

import { forumNotiificationsSettings } from '../api/systemConsts';

import './forum-page.html';
import './views/subforum-list.js';
import './views/forum-reports-modal.js';

Template.forumpage.onCreated(function bodyOnCreated() {
    var forumId = FlowRouter.getParam("forumId");
	Meteor.subscribe('getForum', forumId);
});

Template.forumpage.helpers({
    'forumName'() {
        var forumId = FlowRouter.getParam("forumId");
        var forum = forumServices.getForum(forumId).fetch()[0];
        if (forum){
            return forum.forumName;
        }
        return;
    },
    'forumDescription'() {
        var forumId = FlowRouter.getParam("forumId");
        var forum = forumServices.getForum(forumId).fetch()[0];
        if (forum){
            return forum.forumDescription;
        }
        return;
    },
    'userIsAdminOfForum'() {
        var forumId = FlowRouter.getParam("forumId");
        var forum = forumServices.getForum(forumId).fetch()[0];
        if (forum){
            var forumAdminId = forum.forumAdminId;
            var userId = Meteor.userId();
            return forumAdminId === userId;
        }
        return false;
    },
    'forumSettingsURL'() {
      var forumId = FlowRouter.getParam("forumId");
      var params = { forumId };
      var routeName = "forumAdminPage";
      var path = FlowRouter.path(routeName, params);
      return path;
    },
    'forumPersonalSettingsURL'() {
      var forumId = FlowRouter.getParam("forumId");
      var params = { forumId };
      var routeName = "forumPersonalSettings";
      var path = FlowRouter.path(routeName, params);
      return path;
    },
    'shouldShowForumPersonalSettings'() {
      var forumId = FlowRouter.getParam("forumId");
      var forum = forumServices.getForum(forumId).fetch()[0];
      if (forum && forum.settings.notificationSettings === forumNotiificationsSettings.SELECTIVE) return true;
      
      return false;
    }
});

Template.forumCell.events({
  'click .report'(event) {
    // Prevent default browser form submit
    event.preventDefault();
    instance.state.set('isReport', true);  
  },
});


