import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Accounts } from 'meteor/accounts-base'
import { ReactiveDict } from 'meteor/reactive-dict';

import './updateOldPassword.html';

Template.updatePasswordView.onCreated(function bodyOnCreated() {
  	this.state = new ReactiveDict();
  	this.state.set('showSpinner', false);
});

Template.updatePasswordView.events({
	'submit'(event, template) {
		event.preventDefault();

		var target = event.target;

		var oldPassword = target.oldPassword.value;
		var newPassword = target.newPassword.value;

		if (!oldPassword.length || !newPassword.length) {
			toastr.error("You must enter the old password and the new one");
		}

		template.state.set('showSpinner', true);

		Accounts.changePassword(oldPassword, newPassword, function(error) {
			if (error) {
				template.state.set('showSpinner', false);
				toastr.error(error.reason);
				return;
			}
			Meteor.call("updatePasswordSetDateToNow", function(error, response) {
				template.state.set('showSpinner', false);
				toastr.success("Password changed");

				var routeName = "forumlist";
				FlowRouter.go(routeName);
			});
		});
	}
});

Template.updatePasswordView.helpers({
	forgotURL () {
		var routeName = "forgotRouteName";
		return FlowRouter.path(routeName);
	},
	shouldShowSpinner() {
		const instance = Template.instance();
		return instance.state.get('showSpinner');
	}
});