import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const threadServices = require('../api/threads-service.js');
const subForumServices = require('../api/subforum-service.js');

import './thread-page.html';

Template.threadpagewrap.onCreated(function bodyOnCreated() {
  var threadId = FlowRouter.getParam("threadId");
  Meteor.subscribe('threadById', threadId);
});

Template.threadpagewrap.helpers({
  'threads'(){
    var userId = Meteor.userId();
    var threadId = FlowRouter.getParam("threadId");
    return threadServices.getThreadArrayById(threadId, userId);
  }
})

Template.threadpage.onCreated(function bodyOnCreated() {
  var subForumId = FlowRouter.getParam("subForumId");
  var threadId = FlowRouter.getParam("threadId");
  var forumId = FlowRouter.getParam("forumId");
  Meteor.subscribe('getPost', threadId);
  Meteor.subscribe('subForumArrayForSubforumId',subForumId);
  Meteor.subscribe("getAllUsers");


});

Template.threadpage.events({
  'submit .new-comment'(event) {
    // Prevent default browser form submit
    event.preventDefault();
 
    // Get value from form element
    const target = event.target;
    const text = target.text.value;
 
    var subForumId = FlowRouter.getParam("subForumId");
    var threadId = FlowRouter.getParam("threadId");
    // Insert a comment into the collection of this post
    Meteor.call('threads.comment', threadId, text);
 
    // Clear form
    target.text.value = '';
  },
});

Template.threadpage.helpers({
  forumId() {
    return FlowRouter.getParam("forumId");
  },
  subForumId() {
    return FlowRouter.getParam("subForumId");
  },
  threadId() {
    return FlowRouter.getParam("threadId");
  },
  commentList() {
    return this.comments.reverse();
  },
  subForumName() {
    var subForum = subForumServices.getSubforumsArrayForSubforumId(this.subForumId);
    if(subForum.fetch()[0]){
      return subForum.fetch()[0].subForumName;
    }
    return "";
  },
  createdAtString() {
    return moment(this.createdAt).format('Do MMMM YYYY'); 
  },
  userName() {
    var user = Meteor.users.findOne({ '_id': this.postingUserId });
    if (user.profile) {
      return user.profile.name;
    }
    return user.username;
  },
  profileImg() {
    var user = Meteor.users.findOne(this.postingUserId);
    if (user.profile && user.services){
      return "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
    } 
    return "/face.png";
  },
});

Template.commentcell.helpers({
  createdAtString() {
    return moment(this.createdAt).format('Do MMMM YYYY'); 
  },
  userName() {
    if (this.user.profile) {
      return this.user.profile.name;
    }
    return this.user.username;
  },
   profileImg() {
    if (this.user.profile){
      return "http://graph.facebook.com/" + this.user.services.facebook.id + "/picture/?type=large";
    } 
    return "/face.png";
  },
});