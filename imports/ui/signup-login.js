import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

const policyServices = require('../api/policy-service.js');

import './signup-login.html';

//**************LOGIN**************//
Template.loginView.onCreated(function bodyOnCreated() {
  	this.state = new ReactiveDict();
  	this.state.set('isCreatingUser', false);

  	Meteor.subscribe('getPolicy');
});

Template.loginView.helpers({
	forgotURL () {
		var routeName = "forgotRouteName";
		return FlowRouter.path(routeName);
	},
	isCreatingUser() {
		const instance = Template.instance();
		return instance.state.get('isCreatingUser');
	}
});

Template.loginView.events({
	'submit form'(event, template) {
		event.preventDefault();

		var target = event.target;

		var username = target.username.value;
		var password = target.password.value;
		if (!username || !username.length || !password || !password.length) {
			toastr.error("You must enter a username and a password");
			return;
		}

		Meteor.loginWithPassword(username, password, function (error, response) {
			if (error) {
				var reason = error.reason;
				if (error.reason === "User has no password set") {
					reason += ". Please confirm you're email. Click here to resend";
				}
				
				var divArray = toastr.error(reason);
				
				if (error.reason === "User has no password set") {
					var div = divArray[0];
					div.addEventListener('click', function (event) {
						template.state.set('isCreatingUser', true);

						Meteor.call('userForUserName', username, function(error, createdUser) {
							if (error) {
								template.state.set('isCreatingUser', false);
								toastr.error("Error resending verification email. No user found");
								return;
							}

							var createdUserId = createdUser._id
							var email = createdUser.emails[0].address;

							Meteor.call( 'sendVerificationLink', createdUserId, email, function(error) {
								template.state.set('isCreatingUser', false);

								if ( error ) {
									toastr.error("Error sending verification email to " + email);
								} else {
									toastr.info("Verification mail was sent to " + email);

									var routeName = "forumlist";
									FlowRouter.go(routeName);
								}
							});
						});
					});
				} 
				return;
			}

			var routeName = "forumlist";
			FlowRouter.go(routeName);
		});
	}
});

//**************SIGNUP**************//
Template.signupView.onCreated(function bodyOnCreated() {
  	this.state = new ReactiveDict();
  	this.state.set('isCreatingUser', false);

  	Meteor.subscribe('getPolicy');
});

Template.signupView.helpers({
	isCreatingUser() {
		const instance = Template.instance();
		return instance.state.get('isCreatingUser');
	}, 
	shouldProvideSecurityQuestions() {
		return shouldProvideSecurityQuestionsMethod();
	}
});

function shouldProvideSecurityQuestionsMethod() {
	var policyArray = policyServices.getPolicy().fetch();
	if (!policyArray || !policyArray.length) return false;

	var policy = policyArray[0];

	return policy.allowSecurityQuestion;
}

Template.signupView.events({
	'submit form'(event, template) {
		event.preventDefault();

		var target = event.target;

		var username = target.username.value;
		var email = target.email.value;

		if (!username || !username.length || !email || !email.length) {
			toastr.error("You must enter a username and an email");
			return;
		}

		var user = {username, email};


		if (shouldProvideSecurityQuestionsMethod()) {
			var question1 = target.question1.value;
			var answer1 = target.answer1.value;
			var question2 = target.question2.value;
			var answer2 = target.answer2.value;

			if (!question1 || !question1.length ||
				!question2 || !question2.length ||
				!answer1 || !answer1.length ||
				!answer2 || !answer2.length ) {
				toastr.error("You must enter two security questions");
				return;
			}
			user.securityQuestions = {question1, question2, answer1, answer2};
		}


		template.state.set('isCreatingUser', true);

		Meteor.call("createUserWithoutLogin", user, function (error, createdUserId) {
			if ( error ) {
				template.state.set('isCreatingUser', false);
				toastr.error(error.reason);
			} else {
				Meteor.call( 'sendVerificationLink', createdUserId, email, function(error) {
					template.state.set('isCreatingUser', false);

					if ( error ) {
						console.log(error);
						toastr.error("Error sending verification email to " + email);
					} else {
						toastr.info("Verification mail was sent to " + email);

						var routeName = "forumlist";
						FlowRouter.go(routeName);
					}
				});
			}
		});
	}
});

//**************FORGOT**************//
Template.forgotView.onCreated(function bodyOnCreated() {
  	this.state = new ReactiveDict();
  	this.state.set('didEnterValidEmail', false);
  	this.state.set('question1', "");
  	this.state.set('question2', "");
  	this.state.set('isSendingResetEmail', false);

  	Meteor.subscribe('getPolicy');
});

Template.forgotView.helpers({
	didNotEnterValidEmail() {
		const instance = Template.instance();
		return !instance.state.get('didEnterValidEmail');
	},
	shouldProvideSecurityQuestions() {
		return shouldProvideSecurityQuestionsMethod();
	},
	q1() {
		const instance = Template.instance();
		return instance.state.get('question1');
	},
	q2() {
		const instance = Template.instance();
		return instance.state.get('question2');
	},
	isSendingResetEmail() {
		const instance = Template.instance();
		return instance.state.get('isSendingResetEmail');
	},
});

Template.forgotView.events({
	'click .validateEmail'(event, instance) {
		event.preventDefault();

		var email = instance.find(".emailTextField").value;

		if (!email || !email.length) {
			toastr.error("You must enter an email");
			return;
		}

		Meteor.call("getSecurityQuestionsForEmail", email, function(error, response) {
			if (error || !response) {
				toastr.error("No such email");
			} else {
				if (response === "User is super admin") {
					Meteor.call("sendEmailResetWithEmail", email, function(error, response) {
						toastr.success("Reset email was sent");

						var routeName = "forumlist";
						FlowRouter.go(routeName);
					});
					return;
				}

				instance.state.set('didEnterValidEmail', true);
				instance.state.set('question1', response.question1);
				instance.state.set('question2', response.question2);
			}
		});
	},
	'click .restoreWithQuestions'(event, instance) {
		event.preventDefault();
		
		var email = instance.find(".emailTextField").value;
		var answer1 = instance.find(".answer1TextField").value; 
		var answer2 = instance.find(".answer2TextField").value; 

		instance.state.set('isSendingResetEmail', true);
		Meteor.call("sendEmailResetWithEmailAndSecurityAnswers", email, answer1, answer2, function(error, response) {
			instance.state.set('isSendingResetEmail', false);
			
			if (error || !response) {
				toastr.error("There was an issue with reseting you password");
				return;
			}
			if (response === "wrongAnswer") {
				toastr.error("Wrong answers!");
				return;
			}
			toastr.success("Reset email was sent");

			var routeName = "forumlist";
			FlowRouter.go(routeName);
		});
	},
	'click .restoreWithoutQuestions'(event, instance) {
		event.preventDefault();
		
		var email = instance.find(".emailTextField").value;

		instance.state.set('isSendingResetEmail', true);
		Meteor.call("sendEmailResetWithEmail", email, function(error, response) {
			instance.state.set('isSendingResetEmail', false);
			
			if (error || !response) {
				toastr.error("There was an issue with reseting you password");
				return;
			}
			toastr.success("Reset email was sent");

			var routeName = "forumlist";
			FlowRouter.go(routeName);
		});
	}
});









