import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import './message-page.html';
import './views/message-cell.js';

const service = require('../api/messaging-service.js')
const users = require('../api/user-services.js')

Template.messagePage.onCreated(function bodyOnCreated() {
  Meteor.subscribe('getUserMessages');
  Meteor.subscribe('getAllUsers');

  this.state = new ReactiveDict();
  this.state.set('recepientUserName', null);
  this.state.set('didSelectUser', null);
});

Template.messagePage.helpers({
  get_messages() {
  	var result = service.read(Meteor.userId());
  	return result;
  },
  shouldShowCompletion() {
    const instance = Template.instance();
    var currentRecepientUserName = instance.state.get('recepientUserName');

    if (currentRecepientUserName && currentRecepientUserName.length) {
      return true;
    }
    return false;
    // var users.getUsersWithName
  },
  autoCompletionUsers() {
    const instance = Template.instance();
    var currentRecepientUserName = instance.state.get('recepientUserName');
    return users.getUsersWithName(currentRecepientUserName);
  },
  didSelectAutoCompletionUser() {
    const instance = Template.instance();
    var userSelected = instance.state.get('didSelectUser');
    return userSelected;
  },
  SelectedAutoCompletionUsername() {
    const instance = Template.instance();
    var userSelected = instance.state.get('didSelectUser');
    if (userSelected.username) {
      return userSelected.username;
    }
    if (userSelected.profile) {
      return userSelected.profile.name;
    }
    return "";
    },
});


Template.messagePage.events({
    'submit'(event) {
    // Prevent default browser form submit
    event.preventDefault();
    // Get value from form element

    const target = event.target;

    const username = target.username_to.value;
    const text = target.message_text.value;

    // // Insert a task into the collection

    var userToSendTo = Meteor.users.findOne( { $or : [ { username : username },
                            { "profile.name" : username } ]});
    if (!userToSendTo) {
      target.username_to.value = '';
      target.message_text.value = '';
      return;
    }

    userId = userToSendTo._id;
    Meteor.call('messages.send', Meteor.userId(), userId, text);
    
    // // Clear form
	target.username_to.value = '';
	target.message_text.value = '';
  },
  'input .recepientUserName'(event, instance) {
    var recepientUserName = instance.find('.recepientUserName').value;
    instance.state.set('recepientUserName', recepientUserName);
  },
});

Template.messagePageAutoCompletion.helpers({
  'userNameForUse'() {
    if (this.username) {
      return this.username;
    }
    if (this.profile) {
      return this.profile.name;
    }
    return "";
  }
});

Template.messagePageAutoCompletion.events({
  'click'(event, template) {
      var formState = template.view.parentView.parentView.parentView.parentView._templateInstance.state;
      formState.set('didSelectUser', null);
      formState.set('didSelectUser', this);
      formState.set('recepientUserName', null);
  }
});