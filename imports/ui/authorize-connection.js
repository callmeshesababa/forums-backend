import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import './authorize-connection.html';
const twoWayAuthService = require('../api/two-way-auth-service');

Template.authorizeConnectionView.onCreated(function bodyOnCreated() {
  Meteor.subscribe('connectionStatus');

    this.state = new ReactiveDict();
  	this.state.set('showSpinner', false);
});

Template.authorizeConnectionView.helpers({
  currentConnectionIsAuthorized() {
    return twoWayAuthService.clientSideCurrentConnectionIsAuthorizedDefaultIsNo();
  },
  redirectToMainPageAfterBeingAuthorized() {
    var routeName = "forumlist";
    FlowRouter.go(routeName);
  },
  shouldShowSpinner() {
	const instance = Template.instance();
	return instance.state.get("showSpinner");
  }
});

Template.authorizeConnectionView.events({
	'click .requestCodeFromClien'(event, template) {
		event.preventDefault();
		Meteor.call("requestShowConnectionCodeInAuthorizedClients", function(error, response) {
			toastr.success("Your code will now show in you other client");
		});
	},
	'submit'(event, template) {
		event.preventDefault();

		var target = event.target;
		var code = target.connectionCode.value;

		if (code.length !== 4) {
			toastr.error("The code needs to be 4 characters long!");
			return;
		}

		template.state.set("showSpinner", true);
		Meteor.call("authorizeCodeForCurrentConnection", code, function(error, response) {
			template.state.set("showSpinner", false);

			if (error) {
				toastr.error("There was an error connecting to the server");
				return;
			}
			if (response) {
    			var routeName = "forumlist";
    			FlowRouter.go(routeName);
			} else {
				toastr.error("You entered the wrong code");
				return;
			}
		})
	}
})