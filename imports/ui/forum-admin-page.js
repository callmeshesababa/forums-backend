import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './forum-admin-page.html';
import { getForum } from '../api/forum-services';

import {
  forumNotiificationsSettings,
  allowUserBeModeratorInMultiplePolicies,
  allowModeratorDeleteThreads
} from '../api/systemConsts';

Template.forumAdminPage.onCreated(function bodyOnCreated() {
  var forumId = FlowRouter.getParam("forumId");
  Meteor.subscribe('getForum', forumId);
});

Template.forumAdminPage.helpers({
  "selectiveIsSelected"() {
    return selectedStringIfForumSettingIsEqualToSetting(forumNotiificationsSettings.SELECTIVE);
  },
  "onlineIsSelected"() {
    return selectedStringIfForumSettingIsEqualToSetting(forumNotiificationsSettings.ONLINE);
  },
  "offlineIsSelected"() {
    return selectedStringIfForumSettingIsEqualToSetting(forumNotiificationsSettings.OFFLINE);
  },
  "allowModerateMultipleSelected"() {
    return selectedStringIfForumAllowMultipleModerationSettingIsEqualToSetting(allowUserBeModeratorInMultiplePolicies.ALLOWED);
  },
  "notAllowModerateMultipleSelected"() {
    return selectedStringIfForumAllowMultipleModerationSettingIsEqualToSetting(allowUserBeModeratorInMultiplePolicies.NOTALLOWED);
  },
  "allowModeratorDeleteThreadSelected"() {
    return selectedStringIfForumAllowModeratorDeleteThreads(allowModeratorDeleteThreads.ALLOWED);
  },
  "notAllowModeratorDeleteThreadSelected"() {
    return selectedStringIfForumAllowModeratorDeleteThreads(allowModeratorDeleteThreads.NOTALLOWED);
  }
});

Template.forumAdminPage.events({
  'submit' (event, template) {
    event.preventDefault();

    let target = event.target;

    let selectedNotificationSettingsOptionId = selectedOptionIdFromSelectElement(target.forumNotificationSettings);
    let selectedNotificationSettingsOptionEnum = selectedForumNotificationSettingsEnumFromSelectedId(selectedNotificationSettingsOptionId);

    let selectedAllowMultipleModerationOptionId= selectedOptionIdFromSelectElement(target.forumAllowUserBeModeratorInMultipleSubforums);
    let selecteAllowMultipleModerationOptionEnum = selectedMultipleModerationEnumFromSelectedId(selectedAllowMultipleModerationOptionId);

    let selectedAllowModeratorDeleteThreadOptionId= selectedOptionIdFromSelectElement(target.forumAllowModeratorDeleteThreads);
    let selecteAllowModeratorDeleteThreadOptionEnum = selectedModeratorDeleteThreadEnumFromSelectedId(selectedAllowModeratorDeleteThreadOptionId);

    if (!selectedNotificationSettingsOptionEnum || !selecteAllowMultipleModerationOptionEnum || !selecteAllowModeratorDeleteThreadOptionEnum) return;

    let forumId = FlowRouter.getParam('forumId');

    Meteor.call("forums.setForumNotificationSettings", forumId, selectedNotificationSettingsOptionEnum, function (error, response) {
      if (error) {
        toastr.error("There was an error while changing the settings");
      } else if (response) {
        toastr.success("Notification settings changed!");
      }
    });
    Meteor.call("forums.setForumAllowUserBeModeratorInMultipleSubforums", forumId, selecteAllowMultipleModerationOptionEnum, function (error, response) {
      if (error) {
        toastr.error("There was an error while changing the settings");
      } else if (response) {
        toastr.success("Moderate multiple setting changed!");
      }
    });
    Meteor.call("forums.setForumAllowModeratorDeleteThreads", forumId, selecteAllowModeratorDeleteThreadOptionEnum, function (error, response) {
      if (error) {
        toastr.error("There was an error while changing the settings");
      } else if (response) {
        toastr.success("Allow moderator to delete threads setting changed!");
      }
    });
  },
});

function selectedOptionIdFromSelectElement(element) {
  let selectedIndex = element.selectedIndex;
  let options = element.options;
  return options[selectedIndex].id;
}

function selectedForumNotificationSettingsEnumFromSelectedId(selectedId) {
  var selectedOptionEnum;
  switch (selectedId) {
    case "onlineNotifications":
      selectedOptionEnum = forumNotiificationsSettings.ONLINE;
      break;
    case "offlineNotifications":
      selectedOptionEnum = forumNotiificationsSettings.OFFLINE;
      break;
    case "selectiveNotifications":
      selectedOptionEnum = forumNotiificationsSettings.SELECTIVE;
      break;
  }
  return selectedOptionEnum;
}

function selectedMultipleModerationEnumFromSelectedId(selectedId) {
  var selectedOptionEnum;
  switch (selectedId) {
    case "allow":
      selectedOptionEnum = allowUserBeModeratorInMultiplePolicies.ALLOWED;
      break;
    case "notallow":
      selectedOptionEnum = allowUserBeModeratorInMultiplePolicies.NOTALLOWED;
      break;
  }
  return selectedOptionEnum;
}

function selectedModeratorDeleteThreadEnumFromSelectedId(selectedId) {
  var selectedOptionEnum;
  switch (selectedId) {
    case "allow":
      selectedOptionEnum = allowModeratorDeleteThreads.ALLOWED;
      break;
    case "notallow":
      selectedOptionEnum = allowModeratorDeleteThreads.NOTALLOWED;
      break;
  }
  return selectedOptionEnum;
}

function selectedStringIfForumSettingIsEqualToSetting(setting) {
  let forumId = FlowRouter.getParam('forumId');
  let forum = getForum(forumId).fetch()[0];

  return forum && forum.settings && forum.settings.notificationSettings && forum.settings.notificationSettings === setting && "selected" || "";
}

function selectedStringIfForumAllowMultipleModerationSettingIsEqualToSetting(setting) {
  let forumId = FlowRouter.getParam('forumId');
  let forum = getForum(forumId).fetch()[0];

  return forum && forum.settings && forum.settings.allowUserBeModeratorInMultiplePolicies && forum.settings.allowUserBeModeratorInMultiplePolicies === setting && "selected" || "";
}

function selectedStringIfForumAllowModeratorDeleteThreads(setting) {
  let forumId = FlowRouter.getParam('forumId');
  let forum = getForum(forumId).fetch()[0];

  return forum && forum.settings && forum.settings.allowModeratorDeleteThreads && forum.settings.allowModeratorDeleteThreads === setting && "selected" || "";
}