import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './compose-thread.html';

Template.composeThread.onCreated(function bodyOnCreated() {
	var subForumId = FlowRouter.getParam("subForumId");
  	Meteor.subscribe('threadsBySubForumId', subForumId);
});

Template.composeThread.events({
    'submit'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    
    const threadSubject = target.threadSubject.value;
    const threadText = target.threadText.value;
    var forumId = FlowRouter.getParam("forumId");
    var subForumId = FlowRouter.getParam("subForumId");
    var userId = Meteor.userId();

    // // Insert a task into the collection
    Meteor.call('threads.postThread', subForumId, userId, threadSubject, threadText, function (error, newThreadId) {
        if (!error) {
            var threadId = newThreadId;
            var routeParams = { forumId, subForumId, threadId };
            var routeName = "threadview";
            FlowRouter.go(routeName, routeParams);
        }
    });
  },
});