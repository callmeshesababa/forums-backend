import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import './thread-cell.html';
import { getForumAllowModeratorDeleteThreads } from '../../api/forum-services.js';
import '../../api/threads-service.js';
import {
  getSubforumCursorForSubforumId,
  userIsModeratorInSubforum
} from '../../api/subforum-service';

import { allowModeratorDeleteThreads } from '../../api/systemConsts';

Template.subforumpage.onCreated(function bodyOnCreated() {
  var subforumId = FlowRouter.getParam("subForumId");
  var forumId = FlowRouter.getParam("forumId");

  Meteor.subscribe('subforumCursorForSubforumId', subforumId);
  Meteor.subscribe('getForum', forumId);
});

Template.threadCell.helpers({
  'canEditThread'() {
    return currentUserIsThreadCreator(this);
  },
  'canDeleteThread'() {
    if (currentUserIsThreadCreator(this)) return true;

    return currentUserIsModeratorInSubforum() && forumAllowsModeratorsToDeleteThreads();
  },
  'threadShortDescription'() {
    var text = this.text;
    text = text.substring(0, 50);
    text = text.split("\n")[0];

    return text;
  },
  'threadEditURL'() {
    var forumId = FlowRouter.getParam("forumId");
    var subForumId = FlowRouter.getParam("subForumId");
    var threadId = this._id;
    var params = { forumId, subForumId, threadId };
    var routeName = "threadeditview";

    var path = FlowRouter.path(routeName, params);

    return path;
  },
  'threadURL'() {
    var forumId = FlowRouter.getParam("forumId");
    var subForumId = FlowRouter.getParam("subForumId");
    var threadId = this._id;
    var params = { forumId, subForumId, threadId };
    var routeName = "threadview";

    var path = FlowRouter.path(routeName, params);

    return path;
  },
});

Template.threadCell.events({
    'click .deleteThread'(event) {
    // Prevent default browser form submit
    event.preventDefault();
    var threadId = this._id;
    var subForumId = FlowRouter.getParam("subForumId");

    Meteor.call('threads.deletePost', subForumId, threadId);
  },
});

function currentUserIsThreadCreator(thread) {
  let postingUserId = thread.postingUserId;
  let currentUserId = Meteor.userId();
  return postingUserId === currentUserId;
}

function currentUserIsModeratorInSubforum() {
  let currentUserId = Meteor.userId();
  var subforumId = FlowRouter.getParam("subForumId");

  let userIsModerator = userIsModeratorInSubforum(currentUserId, subforumId);

  return userIsModerator;
}

function forumAllowsModeratorsToDeleteThreads() {
  var forumId = FlowRouter.getParam("forumId");

  let forumAllowModeratorDeleteThreadSetting = getForumAllowModeratorDeleteThreads(forumId);

  return allowModeratorDeleteThreads.ALLOWED === forumAllowModeratorDeleteThreadSetting;
}