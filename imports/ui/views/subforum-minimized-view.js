import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { subForumsMongoDB } from '../api/domainlayer/DAL/SubforumsDA.js';

import './subforum-minimized-view.html';
import './subforum-cell.js';

Template.subforums.onCreated(function bodyOnCreated() {
  Meteor.subscribe('allSubForums');
});

Template.subforums.helpers({
  subforums() {
    return subForumsMongoDB.find({});
  },
});

Template.subforums.events({
    'submit .new-subforum'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const forumName = target.forumName.value;
    const subforumName = target.subforumName.value;
    const manager = target.managerId.value;
    
    // Insert a task into the collection
    Meteor.call('subforums.insert', forumName, subforumName, "desc: " + forumName, manager);
    
    // Clear form
    target.forumName.value = '';
    target.subforumName.value = '';
    target.managerId.value = '';
  },
    'submit .delete-subforum'(event) {
    // Prevent default browser form submit
    event.preventDefault();
    
    // Get value from form element
    const target = event.target;
    const name = target.subforumName.value;
    
    // Insert a task into the collection
    Meteor.call('subforums.delete', name);
    
    // Clear form
    target.subforumName.value = '';
  },
});
