import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

const forumServices = require('../../api/forum-services.js');

import './subforum-cell.html';

Template.subforumCell.onCreated(function bodyOnCreated() {
  	this.state = new ReactiveDict();
  	this.state.set('isEditing', false);
    
    var forumId = FlowRouter.getParam("forumId");
	Meteor.subscribe('getForum', forumId);
});

Template.subforumCell.helpers({
	'userIsManagerInSubforum'() {
		var subForumManagersId = this.subForumManagersId;
		var userId = Meteor.userId();
        return (subForumManagersId.indexOf(userId) > -1);
	},
    'userIsAdminOfForum'() {
        var forumId = FlowRouter.getParam("forumId");
		var forum = forumServices.getForum(forumId).fetch()[0];
		var userId = Meteor.userId();
		return forum.forumAdminId === userId;
	},
	'isInEditMode'() {
		const instance = Template.instance();
		return instance.state.get('isEditing');
	}
});


Template.subforumCell.events({
    'click .deleteSubforum'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    var subforumId = this._id;
	var userId = Meteor.userId();

    Meteor.call('subforums.delete', subforumId, userId);
  },

  'click .editSubforum'(event, instance) {
    instance.state.set('isEditing', true);
  },
  'click .submitEditForum'(event, instance) {
    instance.state.set('isEditing', false);

    var subforumId = this._id;
	var userId = Meteor.userId();
    var newSubforumDesc = instance.find('.subforumDesc').value;
    var newSubforumName = instance.find('.subforumName').value;
      
    Meteor.call('subforums.edit', subforumId, newSubforumName, newSubforumDesc, userId);
  },
    
  'click .cancelForumEdit'(event, instance) {
    instance.state.set('isEditing', false);
  },
    
});