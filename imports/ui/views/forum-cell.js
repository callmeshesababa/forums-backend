import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import './forum-cell.html';
import '../../api/forum-services.js';

Template.forumCell.onCreated(function bodyOnCreated() {
	let forumId = this.data._id;
  	Meteor.subscribe('getForum', forumId);

  	this.state = new ReactiveDict();
  	this.state.set('isEditing', false);
});

Template.forumCell.helpers({
	'userIsAdminOfForum'() {
		let forumAdminId = this.forumAdminId;
		let userId = Meteor.userId();
		return forumAdminId === userId;
	},
	'showJoinButton'() {
		let forumUsers = this.users;
		let userId = Meteor.userId();

		return !forumUsers.includes(userId) && Meteor.user();
	},
  'showLeaveButton'() {
    let forumUsers = this.users;
    let userId = Meteor.userId();

    return forumUsers.includes(userId) && Meteor.user();
  },
  'allowNavigationToSubforum'() {
    let forumUsers = this.users;
    let userId = Meteor.userId();

    return forumUsers.includes(userId) && Meteor.user();
  },
	'isInEditMode'() {
		const instance = Template.instance();
		return instance.state.get('isEditing');
	}
});


Template.forumCell.events({
    'click .deleteForum'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    let forumId = this._id;
	let userId = Meteor.userId();

    Meteor.call('forums.delete', forumId, userId);
  },

  'click .editForum'(event, instance) {
    instance.state.set('isEditing', true);
  },
  'click .submitEditForum'(event, instance) {
    instance.state.set('isEditing', false);

    let forumId = this._id;
	let userId = Meteor.userId();
    var newForumDesc = instance.find('.forumDesc').value;
    var newForumName = instance.find('.forumName').value;

    Meteor.call('forums.edit', forumId, newForumName, newForumDesc, userId);
  },
  'click .cancelForumEdit'(event, instance) {
    instance.state.set('isEditing', false);
  },
  'click .joinForum'(event) {
    let forumId = this._id;
	let userId = Meteor.userId();

	Meteor.call('forums.join', forumId, userId);
  },
  'click .leaveForum'(event) {
    let forumId = this._id;
	let userId = Meteor.userId();

	Meteor.call('forums.leave', forumId, userId);
  },
});
