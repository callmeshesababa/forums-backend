import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './create-forum-modal-form.html';

Template.createforumform.events({
    'submit'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const forumName = target.forumName.value;
    const forumDesc = target.forumDesc.value;

    // // Insert a task into the collection
    Meteor.call('forums.insert', forumName, forumDesc, Meteor.userId());

    $("[data-dismiss=modal]").trigger({ type: "click" });

    // // Clear form
	target.forumName.value = '';
	target.forumDesc.value = '';
  },
});
