import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

const subforumService = require('../../api/subforum-service.js');

import './forum-reports-modal.html';
import './subforum-report.js';

Template.forumreportsform.onCreated(function bodyOnCreated() {
    var forumId = FlowRouter.getParam("forumId");
	Meteor.subscribe('subforumsForForumId', forumId);
});

Template.forumreportsform.helpers({
    'subForumsInForum'() {
        var forumId = FlowRouter.getParam("forumId");
        return subforumService.getSubforumsForForumId(forumId);
    },
});
