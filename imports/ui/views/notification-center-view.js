import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

const notificationCenter = require('../../api/notification-services.js');

import './notification-center-view.html';

Template.notificationCenterView.onCreated(function bodyOnCreated() {
	Meteor.subscribe('unreadNotificationsForUser', Meteor.userId());
});

Template.notificationCenterView.helpers({
	'notifications'() {
		var cursor = notificationCenter.getUnreadNotificationsForUser(Meteor.userId(), 10);
		cursor.observe({
			added (notification) {
				var divArray = toastr.info(notification.notificationMessage);
				var div = divArray[0];

				div.addEventListener('click', function (event) {
					var forumId = notification.relativeUrlPathComponents.forumId;
					var subForumId = notification.relativeUrlPathComponents.subforumId;
					var threadId = notification.relativeUrlPathComponents.threadId;

					if (forumId && subForumId && threadId) {
						var routeParams = { forumId, subForumId, threadId };
						var routeName = "threadview";
						FlowRouter.go(routeName, routeParams);
					} else if (forumId && subForumId) {
						var routeParams = { forumId, subForumId };
						var routeName = "subforumpage";
						FlowRouter.go(routeName, routeParams);
					} else if (forumId) {
						var routeParams = { forumId };
						var routeName = "forumpage";
						FlowRouter.go(routeName, routeParams);
					}
				});

				Meteor.call("markNotificationIsRead", notification._id);
			},
		});
	},

});