import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

const users = require('../../api/user-services.js');

import './subforum-moderator-cell.html';

Template.subforummoderatorscell.onCreated(function bodyOnCreated() {
    this.state = new ReactiveDict();
    this.state.set('isEditingExpirationDate', false);
});

Template.subforummoderatorscell.helpers({
  'moderator'() {
    var moderatorId = this.managerId;
    var experationDate = this.experationDate;
    var moderatorUserObject = users.getUserWithId(moderatorId).fetch()[0];
    return moderatorUserObject;
  },
  'createdAtString'() {
    return moment(this.experationDate).format('Do MMMM YYYY'); 
  },
  'moderatorUsername'() {
    var moderatorId = this.managerId;
    var experationDate = this.experationDate;
    var moderatorUserObject = users.getUserWithId(moderatorId).fetch()[0];
    if (!moderatorUserObject) {
      return "";
    }
    if (moderatorUserObject.username) {
      return moderatorUserObject.username;
    }
    if (moderatorUserObject.profile) {
      return moderatorUserObject.profile.name;
    }
    return "";
  },
  'isNotInExperationDateEditMode'() {
    const instance = Template.instance();
    return !instance.state.get('isEditingExpirationDate');
  },
  'showEditinButtons'() {
    var moderatorId = this.managerId;
    return moderatorId !== Meteor.userId();
  }
});

Template.subforummoderatorscell.events({
  'click .removeModerator'(event, instance) {
    event.preventDefault();

    var subForumId = FlowRouter.getParam("subForumId");
    var userId = Meteor.userId();
    var moderatorId = this.managerId;

    Meteor.call('subforums.removeManager', subForumId, userId, moderatorId);
  },
  'click .editModeratorExpirationDate'(event, instance) {
    event.preventDefault();
    instance.state.set('isEditingExpirationDate', true);
  },
  'click .cancelModeratorExperationDateEdit'(event, instance) {
    event.preventDefault();
    instance.state.set('isEditingExpirationDate', false);
  },
  'click .submitModeratorExperationDateEdit'(event, instance) {
    event.preventDefault();
    instance.state.set('isEditingExpirationDate', false);

    const target = event.target;

    var expeditionDateChooserDateString = instance.find('.datePickerText').value;
    var expeditionDateChooserDateObject = null;
    if (expeditionDateChooserDateString) {
      var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
      expeditionDateChooserDateString = expeditionDateChooserDateString.replace(pattern,'$3-$2-$1');
      expeditionDateChooserDateObject = new Date(expeditionDateChooserDateString);
    }

    var subForumId = FlowRouter.getParam("subForumId");
    var userId = Meteor.userId();
    var moderatorId = this.managerId;

    Meteor.call('subforums.editManagerExpidationDate', subForumId, moderatorId, userId, expeditionDateChooserDateObject);

  },
});






