import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { forumsMongoDB } from '../../api/domainlayer/DAL/ForumDA.js';

import './forum-minimized-view.html';
import '../../api/forum-services.js';
import './forum-cell.js';


Template.forums.onCreated(function bodyOnCreated() {
  Meteor.subscribe('allForums');
});

Template.forums.helpers({
  forums() {
    console.log(forumsMongoDB.find({}));
  	return forumsMongoDB.find({});
  },
});

Template.forums.events({
  'submit .new-forum'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    // Insert a task into the collection
    Meteor.call('forums.insert', text, "desc: " + text);

    // Clear form
    target.text.value = '';
  },
  'submit .delete-forum'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;

    // Insert a task into the collection
    Meteor.call('forums.delete', text);

    // Clear form
    target.text.value = '';
  },
    'submit .edit-forum'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const oldName = target.oldName.value;
    const newName = target.newName.value;

    // Insert a task into the collection
    Meteor.call('forums.edit', oldName, newName, "desc: " + newName);

    // Clear form
    target.oldName.value = '';
    target.newName.value = '';
  },
});
