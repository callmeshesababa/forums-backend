import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './create-subforum-modal-form.html';

Template.createsubforumform.events({
    'submit'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const subforumName = target.subforumName.value;
    const subforumDesc = target.subforumDesc.value;
    const forumId = FlowRouter.getParam("forumId");

    // // Insert a task into the collection
    Meteor.call('subforums.insert', forumId, subforumName, subforumDesc, Meteor.userId());
    
    $("[data-dismiss=modal]").trigger({ type: "click" });

    // // Clear form
	target.subforumName.value = '';
	target.subforumDesc.value = '';
  },
});
