import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './message-cell.html';

Template.messageCell.helpers({
  get_username() {
  	var userId = this.sender;

  	var sender = Meteor.users.findOne({ '_id': userId });

  	if (sender && sender.username) {
		return sender.username;
  	} else if (sender && sender.profile && sender.profile.name){
  		return sender.profile.name;
  	} else {
  		return "";
  	}
  },
});
