import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

const subforumService = require('../../api/subforum-service.js');

import './admin-forum-report-cell.html';
import '../../api/forum-services.js';

Template.adminforumreportcell.onCreated(function bodyOnCreated() {
	let forumId = this.data._id;
	Meteor.subscribe('subforumsForForumId', forumId);
});

Template.adminforumreportcell.helpers({
	'numOfSubforums'() {
        let forumId = this._id;
        return subforumService.getSubforumsForForumId(forumId).fetch().length;
	},
});

