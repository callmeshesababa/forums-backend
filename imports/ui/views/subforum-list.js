import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const subforumService = require('../../api/subforum-service.js');

import './subforum-list.html';
import './subforum-cell.js';

Template.subforumlist.onCreated(function bodyOnCreated() {
    var forumId = FlowRouter.getParam("forumId");
	Meteor.subscribe('subforumsForForumId', forumId);
});

Template.subforumlist.helpers({  
    subforumsForForumId() {
        var forumId = FlowRouter.getParam("forumId");
        return subforumService.getSubforumsForForumId(forumId);
    },
});

