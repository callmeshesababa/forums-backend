import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

const forumServices = require('../../api/forum-services.js');
const twoWayAuthService = require("../../api/two-way-auth-service");

import './top-nav-bar.html';
import './create-forum-modal-form.js';
import './create-subforum-modal-form.js';
import './notification-center-view.js';

Template.topnavbar.onCreated(function bodyOnCreated() {
    var forumId = FlowRouter.getParam("forumId");
    if (forumId) {
      Meteor.subscribe('getForum', forumId);
    }

    Meteor.subscribe('connectionStatus');
});

Template.topnavbar.helpers({
  currentUserUsername() {
  	if (Meteor.user().username) {
  		return Meteor.user().username;
  	}
    return Meteor.user().profile.name;
  },
  shouldShowCreateNewForum() {
  	return FlowRouter.getRouteName() === "forumlist" && Meteor.user();
  },
  shouldShowCreateNewSubForum() {
    var forumId = FlowRouter.getParam("forumId");
    var forum;
    if (forumId) {
      forum = forumServices.getForum(forumId).fetch();
      if (forum.length == 0) return false;
      forum = forum[0]; 
    }
  	return FlowRouter.getRouteName() === "forumview" && Meteor.user() && forum.forumAdminId === Meteor.userId();
  },
  shouldShowComposeThread() {
    return FlowRouter.getRouteName() === "subforumview" && Meteor.user();
  },
  composeThreadURL() {
    var forumId = FlowRouter.getParam("forumId");
    var subForumId = FlowRouter.getParam("subForumId");
    var params = { forumId, subForumId };
    var routeName = "composeThread";
    var path = FlowRouter.path(routeName, params);
    return path;
  },
  loginURL() {
    var routeName = "loginRouteName";
    return FlowRouter.path(routeName);
  },
  signupURL() {
    var routeName = "signupRouteName";
    return FlowRouter.path(routeName);
  },
  shouldShowConnectionCode() {
    return twoWayAuthService.clientShouldShowConnectionCode();
  },
  shouldShowSecurityPolicyButton() {
    return Meteor.user().superAdmin;
  },
  shouldShowAdminPanel() {
    return Meteor.user().superAdmin;
  },
});

Template.topnavbar.events({
	'click .logout'(event) {
		Meteor.logout();
	},
  'click .closeCodeAlert'(event) {
    Meteor.call("stopShowingConnectionCodeInAuthorizedClient");
  },
});