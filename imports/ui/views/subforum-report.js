import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

const threadsService = require('../../api/threads-service.js');

import './subforum-report.html';

Template.subforumreport.onCreated(function bodyOnCreated() {
    var subForumId = this.subForumId;
	Meteor.subscribe('threadsBySubForumId', subForumId);
});

Template.subforumreport.helpers({
  numOfPostsForSubforum() {
    var subForumId = this.subForumId;
  	return threadsService.threadsForSubforumIdWithUserId(subForumId).fetch().length;
  },
});
