import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const threadServices = require('../api/threads-service.js');
const forumsServices = require('../api/forum-services.js');

import './subforum-page.html';
import './views/thread-cell.js';

Template.subforumpage.onCreated(function bodyOnCreated() {
  var subforumId = FlowRouter.getParam("subForumId");
  var forumId = FlowRouter.getParam("forumId");

  Meteor.subscribe('threadsBySubForumId', subforumId);
  Meteor.subscribe('getForum', forumId);
});

Template.subforumpage.helpers({
  'subForumId'() {
    return FlowRouter.getParam("subForumId");
  },
  'threadsForSubForum'() {
  	var userId = Meteor.userId();
  	var subforumId = FlowRouter.getParam("subForumId");

  	return threadServices.threadsForSubforumIdWithUserId(subforumId, userId);
  },
  'moderatorsURL'() {
    var forumId = FlowRouter.getParam("forumId");
    var subForumId = FlowRouter.getParam("subForumId");
    var params = { forumId, subForumId };
    var routeName = "subforummoderators";
    var path = FlowRouter.path(routeName, params);
    return path;
  },
  'isAdminInForum'() {
    var forumId = FlowRouter.getParam("forumId");
    var forumArray = forumsServices.getForum(forumId).fetch();
    if (forumArray.length == 0) return false;
    var forum = forumArray[0];
    return forum.forumAdminId === Meteor.userId();
  }
});