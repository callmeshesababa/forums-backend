import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
const threadServices = require('../api/threads-service.js');

import './thread-edit-page.html';

Template.threadeditpagewrap.onCreated(function bodyOnCreated() {
  var threadId = FlowRouter.getParam("threadId");
  Meteor.subscribe('threadById', threadId);
});

Template.threadeditpagewrap.helpers({
  'threads'(){
    var userId = Meteor.userId();
    var threadId = FlowRouter.getParam("threadId");
    return threadServices.getThreadArrayById(threadId, userId);
  }, 
});

Template.threadeditpagewrap.events({
    'submit'(event) {
    // Prevent default browser form submit
    event.preventDefault();

    var subForumId = FlowRouter.getParam("subForumId");
    var threadId = this._id;

    // Get value from form element
    const target = event.target;
    
    const threadSubject = target.threadSubject.value;
    const threadText = target.threadText.value;

    var userId = Meteor.userId();

    // // Insert a task into the collection
    Meteor.call('threads.editThread', threadId, userId, threadSubject, threadText);

    var forumId = FlowRouter.getParam("forumId");
    var subForumId = FlowRouter.getParam("subForumId");
    var threadId = this._id;
    var routeName = "threadview";
    var routeParams = { forumId, subForumId, threadId };

    FlowRouter.go(routeName, routeParams);
  },
});