import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import '../imports/startup/accounts-config.js';

import '../imports/ui/forum-list.js';
import '../imports/ui/test-body.js';
import '../imports/ui/forum-page.js';
import '../imports/ui/subforum-page.js';
import '../imports/ui/views/top-nav-bar.js';
import '../imports/ui/thread-page.js';
import '../imports/ui/message-page.js';
import '../imports/ui/compose-thread.js';
import '../imports/ui/admin-page.js';
import '../imports/ui/thread-edit-page.js';
import '../imports/ui/subforum-moderators.js';
import '../imports/ui/signup-login.js';
import '../imports/ui/system-uninitialized.js';
import '../imports/ui/security-page.js';
import '../imports/ui/authorize-connection.js';
import '../imports/ui/updateOldPassword.js';
import '../imports/ui/forum-admin-page.js';
import '../imports/ui/forumPersonalSettings';
import '../imports/ui/stess-test';

const usersService = require("../imports/api/user-services");
const twoWayAuthService = require("../imports/api/two-way-auth-service");

import { allowStressTest } from '../imports/api/systemConsts';

Meteor.startup(function() {

});

FlowRouter.route('/', {
  triggersEnter: [renderUninitializedSystem, checkServerHealth],
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "forumlist"});
    },
    name : "forumlist",
});

FlowRouter.route('/uninitialized', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "systemUninitialized"});
    },
    name : "systemUninitializedName",
});


FlowRouter.route('/admin', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "adminpage"});
    },
    name : "adminpage",
});

FlowRouter.route('/security_policy', {
  action: function(params, queryParams) {
      BlazeLayout.render("mainLayout", {content: "securitypage"});
  },
  name : "securitypage",
});

FlowRouter.route('/forum/:forumId', {
    action: function(params, queryParams) {
      BlazeLayout.render("mainLayout", {content: "forumpage"});
    },
    name : "forumview",
});

FlowRouter.route('/forum/:forumId/admin', {
  action: function(params, queryParams) {
    BlazeLayout.render("mainLayout", {content: "forumAdminPage"});
  },
  name : "forumAdminPage",
});

FlowRouter.route('/forum/:forumId/settings', {
  action: function(params, queryParams) {
    BlazeLayout.render("mainLayout", {content: "forumPersonalSettings"});
  },
  name : "forumPersonalSettings",
});

FlowRouter.route('/forum/:forumId/subforum/:subForumId', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "subforumpage"});
    },
    name : "subforumview",
});

FlowRouter.route('/forum/:forumId/subforum/:subForumId/moderators', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "subforummoderatorswrap"});
    },
    name : "subforummoderators",
});

FlowRouter.route('/forum/:forumId/subforum/:subForumId/compose', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "composeThread"});
    },
    name : "composeThread",
});

FlowRouter.route('/forum/:forumId/subforum/:subForumId/threads/:threadId', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "threadpagewrap"});
    },
    name : "threadview",
});

FlowRouter.route('/forum/:forumId/subforum/:subForumId/threads/:threadId/Edit', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "threadeditpagewrap"});
    },
    name : "threadeditview",
});

FlowRouter.route('/test', {
    action: function(params, queryParams) {
      BlazeLayout.render("mainLayout", {content: "testbody"});
    },
    name : "test",
});

FlowRouter.route('/messages', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "messagePage"});
    },
    name : "messaging",
});

FlowRouter.route('/login', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "loginView"});
    },
    name : "loginRouteName",
});

FlowRouter.route('/signup', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "signupView"});
    },
    name : "signupRouteName",
});

FlowRouter.route('/forgot', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "forgotView"});
    },
    name : "forgotRouteName",
});

FlowRouter.route('/authorizeConnection', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "authorizeConnectionView"});
    },
    name : "authorizeConnectionRouteName",
});

FlowRouter.route('/updatePassword', {
    action: function(params, queryParams) {
        BlazeLayout.render("mainLayout", {content: "updatePasswordView"});
    },
    name : "updatePasswordRouteName",
});

if (allowStressTest) {
  FlowRouter.route('/stress', {
    action: function(params, queryParams) {
      BlazeLayout.render("mainLayout", {content: "stresstest"});
    },
    name : "stresstest",
  });
}


Template.mainLayout.onCreated(function bodyCreated () {
  Meteor.subscribe('superAdminUsers');
})
Template.mainNavLayout.onCreated(function bodyCreated () {
  Meteor.subscribe('superAdminUsers');
})


function renderUninitializedSystem(context, redirect) {
  Meteor.call("isSuperAdminSet", function (error, response) {
    if (!response)
      FlowRouter.go('systemUninitializedName');
  });
} 

var connectionChecker = {
  status: false
}

function checkServerHealth(context, redirect) {
  Meteor.setTimeout(function() {
    if (!connectionChecker.status) {
      toastr.error("There is a problem with the server. Functionality is limited");
    }
  }, 1500);

  Meteor.call("checkServerHealth", function (error, response) {
    connectionChecker.status = true;
  });  
  
} 

Template.mainNavLayout.helpers({
  isInitialized(){
    return usersService.isSuperAdminSet();
  }
});

Template.mainLayout.onCreated(function bodyOnCreated() {
  Meteor.subscribe('connectionStatus');
});

Template.mainLayout.helpers({
  currentConnectionIsAuthorized() {
    return twoWayAuthService.clientSideCurrentConnectionIsAuthorized();
  },
  redirectToConnectionAuthAndRequestTheServerToShowCodeOnAuthorizedClients() {
    Meteor.call("requestShowConnectionCodeInAuthorizedClients");
    var routeName = "authorizeConnectionRouteName";
    FlowRouter.go(routeName);
  },
  checkCurrentUserPasswordIsValid() {
    Meteor.call("checkCurrentUserPasswordIsValid", function(error, isValid) {
      if (!error && !isValid) {
        var routeName = "updatePasswordRouteName";
        FlowRouter.go(routeName);
      }
    });
  }
}) 

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "showDuration": "3000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
